const config = require('config');

const Vault = {

  STRIPE_PUBLIC_KEY: config.get('Stripe.PublicKey'),
  STRIPE_PRIVATE_KEY: config.get('Stripe.PrivateKey'),
  EXPRESS_SESSION_SECRET: config.get('ExpressSession.Secret'),
  API_KEYS: config.get('Glibr.ApiKeys'),
  PICPURIFY_KEY: config.get('Picpurify.ApiKey'),
  PICPURIFY_URL: config.get('Picpurify.URL'),

};

module.exports = Vault;
