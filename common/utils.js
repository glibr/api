/* eslint-disable no-unused-vars */
const Globals = require('../common/globals.js');
const Helper = require('../common/helpers');

module.exports = {


  // form validation methods
  Validation: {

    validHandle: (handle) => {
      return /^[A-Za-z0-9][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*(?:\.[A-Za-z0-9]+)*(?:\-[A-Za-z0-9]+)*$/.test(handle);
    },
    validHandleLength: (handle) => {
      return (3 < handle.length && handle.length <= 24);
    },
    validEmail: (email) => {
      return /[a-zA-Z0-9\+\.\_\%\-\+]{1,256}\@[a-zA-Z0-9][a-zA-Z0-9\-]{0,64}(\.[a-zA-Z0-9][a-zA-Z0-9\-]{0,25})/.test(email);
    },
    validPhone: (phone) => {
      return /(\+)?(\d{1,3})?(\s)?(\()?(\d{0,3})?(\)|\-)?(\s)?\d{3,3}(\-)?(\s)?\d{4,4}/.test(phone);
    },
    isGlibLessThanTextLimit: (glitText) => {
      return (glibText.length <= Globals.CONSTANTS.GLIB_POST_MAX_LENGTH);
    },
    // text must be alpha-numeric and - or _ or space only
    isAlphaNumericOnly: (checkThisText) => {
      return /^[ \w-]+$/.test(checkThisText);
    },
    // text can be only 55 characters long
    isTextLessThan55Characters: (checkThisText) => {
      return (checkThisText.length <= 55);
    },
    // a text has to be 150 characters or less
    isTextLessThan150Characters: (checkThisText) => {
      return (checkThisText.length <= 150);
    },
    isTextLessThan255Characters: (checkThisText) => {
      return (checkThisText.length <= 255);
    },
    isTextLessThan1000Characters: (checkThisText) => {
      return (checkThisText.length <= 1000);
    },
    isFirstName: (firstName) => {
      return (firstName.length <= 50);
    },
    isLastName: (lastName) => {
      return (lastName.length <= 50);
    },
    validStateLength: (state)=>{
      return (1< state.length && state.length<Globals.CONSTANTS.PROFILE_STATE_LENGTH );
    },
    noBadWords: (checkThisText) => {
      // eslint-disable-next-line no-invalid-this
      return (checkThisText === Helper.cleanText(checkThisText));
    },
    weakPassword: (pw) => {
      // private methods
      const isWeakPassword = (password) => {
        if (password.length < 8) {
          return true;
        }
        let charTypes = 0;
        if (hasAtleastOneLowercaseLetter(password)) {
          charTypes += 1;
        } // at least one lowercase
        if (hasAtleastOneUppercaseLetter(password)) {
          charTypes += 1;
        } // at least one uppercase
        if (hasAtleastOneNumeric(password)) {
          charTypes += 1;
        } // at least one numeric
        if (hasAtleastOneNonAlphaNumeric(password)) {
          charTypes += 1;
        } // at least one non-alphanumeric
        return charTypes < 4;
      };
      const hasAtleastOneLowercaseLetter = (string) => {
        return /.*[a-z].*/.test(string);
      };
      const hasAtleastOneUppercaseLetter = (string) => {
        return /.*[A-Z].*/.test(string);
      };
      const hasAtleastOneNumeric = (string) => {
        return /.*[0-9].*/.test(string);
      };
      const hasAtleastOneNonAlphaNumeric = (string) => {
        return !/^[a-zA-Z0-9]*$/.test(string);
      };
      // const isValidPhoneNumber = (phoneNumber) => {
      //   return /^(\([0-9]{3}\)|[0-9]{3}-?)[0-9]{3}-?[0-9]{4}$/.test(phoneNumber);
      // };


      return isWeakPassword(pw);
    },
  },


};
