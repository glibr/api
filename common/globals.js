const config = require('config');

module.exports = {
  SITE_NAME: 'glibr',
  ENVIRONMENT: process.env.NODE_ENV,
  ENV_VARS: {
    SITE_BASE_URL: config.get('Glibr.BaseUrl'),
    MONGO_CONN_STRING: config.get('Mongo.ConnectionString', {
      useFindAndModify: false,
    }),
    AWS_S3_SECRETACCESSKEY: config.get('AWS-S3.secretAccessKey'),
    AWS_S3_ACCESSKEYID: config.get('AWS-S3.accessKeyId'),
  },
  LOGGING: {
    PATH: config.get('Logging.Path'),
    LEVEL: config.get('Logging.Level'),
    PERIOD: config.get('Logging.Period'),
    RETENTION: config.get('Logging.Retention'),
  },
  CONSTANTS: {
    GLIB_POST_MAX_LENGTH: 150,
    GLIB_MAX_PENNIES: 250,
    GLIB_MIN_PENNIES: 2,
    GLIB_PROVISIONAL_PENNY_LIMIT: 60,
    GLIB_BILLING_THRESHOLD: 300,
    IMAGE_UPLOAD_SIZE_LIMIT: 2500000,
    IMAGE_UPLOAD_DESTINATION:
      'https://glibr-public.s3-us-west-2.amazonaws.com/uploads/',
    SALT_FACTOR: 10,
    ACCEPTED_IMAGE_FILE_TYPES: ['.jpg', '.jpeg', '.png', '.gif'],
    STRIPE_FAILURE_STATUS_CODE: 'failed',
    PROFILE_STATE_LENGTH: 55,
    PROFILE_FIRST_NAME_LENGTH: 55,
    PROFILE_LAST_NAME_LENGTH: 55,
  },

  /* eslint-disable max-len */
  STRINGS: {
    // Create Acccount Strings
    ACCOUNT_ACTIVATION_EMAIL_ERROR: 'Failed to send account activation email.',
    ACCOUNT_NO_EMAIL_ADDRESS_FOUND:
      'The given email address could not be found. ',
    ACCOUNT_ACTIVATION_EMAIL_SUCCESS:
      'Account activation email sent successfully.',
    ACCOUNT_ACTIVATION_EMAIL_BAD_EMAIL:
      'That does not appear to be a valid email address.',
    ACCOUNT_ACTIVATION_BAD_HANDLE:
      'That\'s not a good handle.  Your handle can only contain letters, numbers, and the characters  .  -  _ ',
    ACCOUNT_ACTIVATION_WEAK_PASSWORD:
      'Whoops, lets make a stronger password. Try including at least one uppercase letter, one special character, and one number.  Also more than 8 characters long.',
    ACCOUNT_AUTHENTICATION_ERROR: 'An error occurred during authentication.',
    ACCOUNT_AUTHENTICATION_SUCCESS: 'User was successfully authenticated.',
    ACCOUNT_DEAUTHENTICATION_SUCCESS: 'User successfully logged out.',
    ACCOUNT_REGISTRATION_STEP_ONE_COMPLETE:
      'Basic account information created.',
    ACCOUNT_REGISTRATION_STEP_TWO_COMPLETE:
      'Charity successfully chosen for this account.',
    ACCOUNT_REGISTRATION_STEP_THREE_COMPLETE:
      'Payment system  successfully signed up for this account.',
    ACCOUNT_REGISTRATION_PROVISIONAL_SETUP_SUCCESS:
      'You have successfully been signed up with a provisional account.',
    ACCOUNT_REGISTRATION_PROVISIONAL_SETUP_FAIL:
      'Sorry we were not able to sign you up.  Please contact glibr.help@gmail.com.',

    ACCOUNT_REGISTRATION_ERROR:
      'Hmm it seems that something went wrong on our end, please try hitting that button again. If you keep seeing this, it means we\'re aware of the issue, and are working to fix it as we speak.',
    ACCOUNT_STRIPE_NO_CUSTOMER_ID_TOKEN_RETURNED:
      'Sorry there was a problem with our request from STRIPE and no customer id was sent back from them.  If you need help please reach out to glibr.help@gmail.com',
    ACCOUNT_ACTIVATION_FAILED:
      'Sorry we were unable to authenticate this user.  If you think we are wrong about this please contact glibr.help@gmail.com',
    ACCOUNT_ACTIVATION_FAILED_NO_USER:
      'Hey we can&apos; find that user anywhere.  Sorry.',
    ACCOUNT_ACTIVATION_FAILED_NO_SAVE:
      'Hey there, we encountered issues on our side trying to activate you.  Please reach out to our admins: glibr.help@gmail.com',
    ACCOUNT_STRIPE_TOKEN_CHECK_FAILED: 'We could not lookup the token.',
    ACCOUNT_STRIPE_TOKEN_NOT_UNIQUE:
      'That Credit Card has been used for another customer signup.  Please use a different one.',
    ACCOUNT_STRIPE_TOKEN_UNIQUE: 'Houston, we have a new card.',
    ACCOUNT_STRIPE_REGISTRATION_FAILED:
      'Failed to register new customer with Stripe.',
    ACCOUNT_STRIPE_REGISTRATION_SUCCEEDED:
      'We succeeded in registering the user with Stripe.',

    // glib related strings
    GLIB_CREATION_ERROR:
      'Error attempting to create a new glib. Please see log.',
    GLIB_CREATION_SUCCESS: 'New glib created successfully.',
    GLIB_EMPTY_GLIB_POST:
      'Seriously dude, you gotta post something, you can\'t just leave it blank',
    GLIB_EMPTY_AFTER_TAG_CLEAN:
      'Now now, no HTML tags please.  Post some plain text.',
    GLIB_PENNIES_TOO_LITTLE:
      'Hey, you need to spend juuuust a bit more to get that glib posted.',
    GLIB_PENNIES_TOO_MUCH:
      'So, we have an upper limit on pennies per glib and you (in your enthusiasm) have gone over it a bit, keep it below - ', // add penny upper limit
    GLIB_LIKE_ADDED: 'Glib like added.',
    GLIB_LIKE_ADD_FAILED: 'Failed to add the like for this glib.  sad..',
    GLIB_LIKE_REMOVE_FAILED: 'We failed to remove the like.',
    GLIB_UNLIKE_ADD_FAILED: 'We failed to add the UN-like',
    GLIB_UNLIKE_REMOVE_FAILED: 'We failed to remove the UN-like.',
    DELETE_GLIB_BY_USER: 'glib deleted by user.',
    DELETE_GLIB_BY_ADMIN: 'glib deleted by administrator.',
    DELETE_GLIB_BY_GROUP_ADMIN: 'glib deleted by group admin ',
    DELETE_FAILED_TO_FIND_PAID_FLAG:
      'Unable to find whether the glib has been paid out.',
    DELETE_GLIB_FAILED: 'We experienced an error trying to delete that glib.',

    // search
    SEARCH_FAILURE: 'The search failed.',

    GET_PAYMENTS_LIST_ERROR: 'Error retrieving user payment list.',
    GET_USER_GLIBS_ERROR: 'Error retrieving the current user\'s glibs.',
    GET_GLIBFEED_MULTI_HANDLE_FEED_FAIL:
      'Error in retrieving the list of glibs for a set of handles.',
    GET_GLIBFEED_FAIL: 'Error in retrieving the glibfeed',
    GET_GLIBFEED_SUCCESS: 'We have a succseful glibFeed.  Woot!',

    // picture upload and management strings
    PROFILE_PICTURE_TOO_LARGE:
      'Profile picture is too large, maximum size is: ',
    PROFILE_FAILED_TO_UPLOAD_PICTURE:
      'Sorry we failed to upload the picture together, let\'s try harder next time.',
    PICTURE_TOO_LARGE: 'Your  picture is too large, maximum size is: ', // add the size constant
    PROFILE_UPDATE_INVALID_PHONE:
      'Oops, we just tried calling you, but we couldn\'t get through. Totally kidding, but seriously, please enter a valid phone number. :)',
    PROFILE_UPDATE_SUCCESS: 'User profile updated successfully.',
    PROFILE_UPDATE_INVALID_STATE:
      'Hey, something is wrong with the state you entered, try again.',

    FILE_SAVE_ERROR: 'File save process failed, please try again.',
    FILE_UPLOAD_ERROR: 'There was an error during the upload process.',
    FILE_UPLOAD_SUCCESS: 'That file has been successfully uploaded.  Nice!',
    UNSUPPORTED_IMAGE_FILE_TYPE:
      'This file type is not supported. Accepted file types are: .jpg, .jpeg, png, .gif',
    FILE_DELETE_SUCCESS: 'The file has been successfully deleted.',
    fILE_DELETE_FAILURE: 'The file was not deleted.',
    FILE_REJECTED_NSFW:
      'Your image has been rejected as containing nudity.  Sorry, try something more PG.',
    FAILED_NSFW_CHECK:
      'There was a failed response from the NSFW call: error Code: ',
    FILE_DOES_NOT_EXIST: 'unable to find the file:',
    FILE_NOT_DEFINED: 'Filename was not specified in request.',
    FAILED_WRITING_FILENAME_TO_DB: 'Failed to save the filename to the db',
    FILE_PROBLEM_LOADING_FROM_S3: 'Failed to load file from S3.',

    // payment / stript / charge user strings
    CHARGE_FAILED_TO_FIND_USER: 'failed to find that user: ',
    CHARGE_UNABLE_TO_UPDATE_PENNY_LINES:
      'unable to update penny lines in glibpennies collection.',
    CHARGE_NO_PENNIES_ACCRUED: 'No pennies accrued: ',
    CHARGE_PENNIES_ACCRUED_BELOW_BILLING_THRESHOLD:
      'Accrued pennies do not meet billing threshold.',
    CHARGE_FAILED_TO_CHARGE_STRIPE: 'failed to charge stripe: ',
    CHARGE_SUCCESS_FULL: 'success, we have charged and updated a customer',
    CHARGE_SUCCESS_PARTIAL:
      'Stripe successfully communicated with but failed to save to the glibr payment collection.',
    CHARGE_SUCCESS_NO_PENNY_LINE_UPDATE:
      'success, we have charged and updated a customer - however we were unable to update penny lines.',
    CHARGE_FAILURE_AND_FAILED_TO_UPDATE_PENNY_LINES:
      'faied to charge customer and failed to update penny lines.',

    FIELD_ALREADY_TAKEN:
      'One of the fields you entered has already been used by a different user.',
    TEXT_CLEAN_FAILURE:
      'Something went wrong when we tried to clean the text you wrote.',

    HANDLE_ALREADY_TAKEN:
      'Aw, darn it all to "h" "e" "double hockeysticks", that handle has already been taken. Please try something else :(',
    IMAGE_UPLOAD_SUCCESS: 'Image uploaded successfully.',
    INCORRECT_CREDENTIALS: 'Username or password is incorrect 1.',
    INVALID_API_KEY: 'The api key you provided is not valid.',
    INVOICE_CREATION_FAILURE: 'Failure to create the invoice for this payment.',
    INVOICE_CREATION_SUCCESS: 'Invoice created successfully',
    MISSING_FIELDS_DURING_REGISTRATION:
      'Bad Request. Handle, Password, and Email Address are required fields.',
    NOT_AUTHORIZED: 'You are not authorized to view this page.',

    RESET_PW_EMAIL_ERROR: 'Sending reset password email failed.',
    RESET_PW_EMAIL_SUCCESS: 'Reset password email sent successfully.',
    RESET_PW_ERROR: 'An error occurred trying to reset your password.',
    RESET_PW_ERROR_INCORRECT_PW: 'The original password you entered is incorrect.',
    RESET_PW_LINK_EXPIRED:
      'The reset password link has expired. Please send another forgot password request.',
    RESET_PW_SUCCESS: 'User password was reset successfully.',
    SERVER_ISSUES: 'We are experiencing server issues, please try again later.',
    USER_ALREADY_EXISTS:
      'Oops, looks like we have already got you in our system, try logging in with that same email.',
    USER_DOES_NOT_EXIST:
      'Whoops, I wish we did, but we don\'t know you yet. Please create a new account with that email.',

    USERLIST_NO_ID_LIST: 'There is not list of ids passed to the method.',
    USERLIST_FAILED_TO_GET_USERLIST: 'Failed to retrieve userlist',

    SET_CHARITY_IN_USER_ERROR:
      'We weren\'t able to save the charity in your profile. sorry.',
    USER_WITH_GIVEN_HANDLE_NOT_FOUND:
      'Sorry, we couldn\'t find a user by that handle.',
    USER_UNABLE_TO_FIND_BY_ID: 'Sorry, we were unable to get handle by that id',
    USER_SIGN_IN_CURRENT_USER_FAIL:
      'Sorry, we couldn\'t find a user for that handle.',

    USERLINK_SAVE_ERROR: 'Sorry we couldn\'t save that user follow link.',
    USERLINK_REMOVE_ERROR: 'Unable to remove link',
    USERLINK_SAME_USER:
      'Sorry, but those are the same handle for link user and current user.',
    UERLINK_COULDNT_RETRIEVE_LINKUSER:
      'We were unable to retrieve the link user from the database',
    USERLINK_COULDNT_RETRIEVE_CURRENT_USER:
      'We were unable to retireve the current user from the database.',
    USERLINK_THAT_PAIR_ALREADY_EXISTS:
      'That pair of users are already linked in this way.',
    USERLINK_FAILED_TO_DO_SAVE: 'We failed to save the link',
    USERLINK_FAILED_TO_UPDATE_USER: 'We failed to update the user',
    USERLINK_UNABLE_TO_DELETE_LINK:
      'Unable to delete the userlink between users.',
    FOLLOW_USER_SUCCESS: 'This user has been added to your follow list.',
    USERLINK_FAILURE_ON_LINK_LOOKUP:
      'We had a problem looking up the link between uses.',

    // groups
    GROUPS_CREATE_FAIL: 'Sorry, there was a problem creating a group.',
    GROUPS_CREATE_SUCCESS: 'Your group was succesfully created.',
    GROUPS_UPDATE_FAIL: 'Sorry, we were not able to update your group.',
    GROUPS_UPDATE_SUCCESS: 'Your group was updated successfully.',
    GROUPS_CREATE_USER_GROUP_LINK_FAIL:
      'Sorry we were unable to link your handle to a group.',
    GROUPS_CREATE_USER_GROUP_FAIL_WRONG_USERTYPE:
      'That is not a valid usertype to assign a user to..',
    GROUPS_GET_USER_ID_FAIL:
      'Sorry we couldn\'t get the user id for that handle',
    GROUPS_GET_USER_IN_GROUP_SUCCESS:
      'Great! we found this user in that group.',
    GROUPS_GET_USER_IN_GROUP_FAIL:
      'Sorry that user does not belong to that group.',
    GROUPS_CHANGE_USERTYPE_FAIL:
      'Sorry we were not able to change the Group User Type for this user',
    GROUPS_CHANGE_USERTYPE_SUCCESS:
      'The user type for this group has been changed to: ',
    GROUPS_USER_LINK_SUCCESS: 'You hve succesfully joined the group',
    GROUPS_RETRIEVE_GROUP_LINKS_FAIL:
      'An error occured when we tried to retrieve your groups',
    GROUPS_RETRIEVE_GROUP_BY_ID_FAIL:
      'An error occured when trying to retrieve a group by its id.',
    GROUPS_RETRIEVE_BY_ID_SUCCESS: 'We have a group.',
    GROUPS_USER_LINK_INSERT_DUPE_FAIL:
      'This user already has a group link for this group.',
    GROUPS_USER_LINK_INSERT_DUPE_NO_DUPE:
      'There is no entry for this user - group link yet.',
    GROUPS_USER_LINK_NO_GROUP_MATCH:
      'There was no match for that user and that group',
    GROUPS_USER_LINK_DELETE_FAIL: 'Failed to delete the user from the group',
    GROUPS_USER_LINK_DELETE_SUCCESS:
      'We successfully deleted the link between the user and group.',
    GROUP_NO_DUPE_FOUND:
      'We did not find a duplicate in the system for this group, it is unique.',
    GROUP_YES_DUPE_FOUND:
      'We found a duplicate group, you need to try a different name.',
    GROUP_NAME_TOO_LONG:
      'The name is too long, you need to keep it less than 55 characters.',
    GROUP_NAME_MUST_BE_ALPHANUMERIC:
      'Hey, the group name has to be just alpha-numeric and spaces, dashes and underscores.',
    GROUP_NAME_NO_BAD_WORDS: 'Hey you can\'t use bad words in your group name.',
    GROUP_DESCRIPTION_TOO_LONG:
      'The description is too long, you need to keep it less than 1000 characters.',
    GROUP_DESCRIPTION_NO_BAD_WORDS:
      'Hey you can\'t use bad words in your group description.',
    GROUP_CATEGORY_TOO_LONG:
      'The category  is too long, you need to keep it less than 55 characters.',
    GROUP_CATEGORY_NO_BAD_WORDS:
      'Hey you can\'t use bad words in your group category.',
    GROUP_CATEGORY_MUST_BE_ALPHANUMERIC:
      'Hey, the group category has to be just alpha-numeric and spaces, dashes and underscores.',
    GROUPS_NO_USER_FOUND_IN_GROUP:
      'Sorry, that user wasn\'t found in this group.',
    GROUPS_YES_USER_FOUND_IN_GROUP: 'Cool, we found that user in this group.',
    GROUPS_UNABLE_TO_GET_USER_LIST_FOR_GROUP:
      'We were unable to find any users for that group',
  },
};

/* eslint-disable max-len */
