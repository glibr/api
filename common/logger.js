// -----------------------------------------
const GLOBALS = require('./globals.js');
const fs = require('fs');
const Bunyan = require('bunyan');

// Create log directory.
fs.mkdir(GLOBALS.LOGGING.PATH, (err) => {/* no-op */});

// Instantiate the logger
const log = new Bunyan({
  name: GLOBALS.SITE_NAME,
  streams: [{
    stream: process.stdout,
    level: GLOBALS.LOGGING.LEVEL,
  },
  {
    type: 'rotating-file',
    path: './logs/glibr.log',
    level: GLOBALS.LOGGING.LEVEL,
    period: GLOBALS.LOGGING.PERIOD, // When to roll the files.
    count: GLOBALS.LOGGING.RETENTION, // Log files to retain.
  },
  ],
  serializers: Bunyan.stdSerializers,
});

log.info('Logger initialized.');

// Already a singleton if we export an object.
module.exports = log;

