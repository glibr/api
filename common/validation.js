const Globals = require('../common/globals.js');
const Helper = require('../common/helpers');

class Validation {
  static validHandle(handle) {
    return /^[A-Za-z0-9][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*(?:\.[A-Za-z0-9]+)*(?:\-[A-Za-z0-9]+)*$/.test(handle);
  }
  static validHandleLength(handle) {
    return (3 < handle.length && handle.length <= 24);
  }
  static validEmail(email) {
    return /[a-zA-Z0-9\+\.\_\%\-\+]{1,256}\@[a-zA-Z0-9][a-zA-Z0-9\-]{0,64}(\.[a-zA-Z0-9][a-zA-Z0-9\-]{0,25})/.test(email);
  }
  static validPhone(phone) {
    return /(\+)?(\d{1,3})?(\s)?(\()?(\d{0,3})?(\)|\-)?(\s)?\d{3,3}(\-)?(\s)?\d{4,4}/.test(phone);
  }
  static isGlibLessThanTextLimit(glitText) {
    return (glibText.length <= Globals.CONSTANTS.GLIB_POST_MAX_LENGTH);
  }
  // text must be alpha-numeric and - or _ or space only
  static isAlphaNumericOnly(checkThisText) {
    return /^[ \w-]+$/.test(checkThisText);
  }
  // text can be only 55 characters long
  static isTextLessThan55Characters(checkThisText) {
    return (checkThisText.length <= 55);
  }
  // a text has to be 150 characters or less
  static isTextLessThan150Characters(checkThisText) {
    return (checkThisText.length <= 150);
  }
  static isTextLessThan255Characters(checkThisText) {
    return (checkThisText.length <= 255);
  }
  static isTextLessThan1000Characters(checkThisText) {
    return (checkThisText.length <= 1000);
  }
  static isFirstName(firstName) {
    return (firstName.length <= 50);
  }
  static isLastName(lastName) {
    return (lastName.length <= 50);
  }
  static validStateLength(state) {
    return (1< state.length && state.length<Globals.CONSTANTS.PROFILE_STATE_LENGTH );
  }
  static noBadWords(checkThisText) {
    // eslint-disable-next-line no-invalid-this
    return (checkThisText === Helper.cleanText(checkThisText));
  }

  static isStrongPassword(pw) {
    return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!\^\@#\$%&\?]).{8,}/.test(pw);
  }
}

module.exports = Validation;
