
const Filter = require('bad-words');
const logger = require('./logger');
const Audit = require('../models/audit');

class Helper {
  static isEmptyOrWhitespace(str) {
    const regEx = /\s/;
    const number = 'number';
    return (typeof str === number && isNaN(str)) || str === null || str === undefined || str === ''
      || regEx.test(typeof str !== number ? str.charAt(0) : String.fromCharCode(str));
  }

  static getMonthByAbbreviatedName(month) {
    switch (month) {
      case 0: return 'Jan';
      case 1: return 'Feb';
      case 2: return 'Mar';
      case 3: return 'Apr';
      case 4: return 'May';
      case 5: return 'Jun';
      case 6: return 'Jul';
      case 7: return 'Aug';
      case 8: return 'Sep';
      case 9: return 'Oct';
      case 10: return 'Nov';
      case 11: return 'Dec';
    }
  }

  /** cleanText
       * returns both the fully cleaned text AND a boolean indicating that cleaning occured.
       *  @param {string} textToClean what to clean
       * @returns {object} boolean for cleaning and the cleaned text as string.
       */
  static cleanText(textToClean) {
    try {
      let newText = this.cleanHTMLTagsFromText(textToClean);
      newText = this.cleanBadWordsFromText(newText);
      return newText;
    } catch (err) {
      logger.error(err);
      return null;
    }
  }

  static cleanTextAndCompare(textToClean) {
    try {
      let newText = this.cleanHTMLTagsFromText(textToClean);
      newText = this.cleanBadWordsFromText(newText);

      const wasTextCleaned = (textToClean !== newText);

      return {
        success: true,
        textWasCleaned: wasTextCleaned,
        cleanedText: newText,
      };
    } catch (err) {
      logger.error(err);
      return {
        success: false,
        textWasCleaned: false,
        cleanedText: textToClean,
      };
    }
  }

  static cleanHTMLTagsFromText(textToClean) {
    logger.info(`in text cleaning: ${textToClean}`);
    logger.info('in text cleaning: ' + textToClean);
    try {
      // text parsing to remove HTML tags from post.
      const tagBody = '(?:[^"\'>]|"[^"]*"|\'[^\']*\')*';

      const tagOrComment = new RegExp(
          '<(?:'
                  // Comment body.
                  +
                  '!--(?:(?:-*[^->])*--+|-?)'
                  // Special "raw text" elements whose content should be elided.
                  +
                  '|script\\b' + tagBody + '>[\\s\\S]*?</script\\s*' +
                  '|style\\b' + tagBody + '>[\\s\\S]*?</style\\s*'
                  // Regular name
                  +
                  '|/?[a-z]' +
                  tagBody +
                  ')>',
          'gi');


      // remove HTML tags from, post to prevent XSS
      let oldText;
      do {
        oldText = textToClean;
        textToClean = textToClean.replace(tagOrComment, '');
      } while (textToClean !== oldText);

      textToClean.replace(/</g, '&lt;');

      return textToClean;
    } catch (err) {
      logger.error(err);
      return null;
    }
  }


  static cleanBadWordsFromText(textToClean) {
    try {
      //  clean out dirty words
      const filter = new Filter();
      const newText = filter.clean(textToClean);

      return newText;
    } catch (err) {
      logger.error(err);
      return null;
    }
  }


  static insertAuditLine(aObj) {
    const auditObj = Audit({
      ...aObj,
    });

    try {
      auditObj.save();

      return ({
        success: true,
        auditLineId: auditObj._id,
      });
    } catch (err) {
      logger.error(err);
      return ({
        success: false,
        errMessage: err,
      });
    }
  }


  static parseHashtags(str) {
    const regex = /#(\w*[0-9a-zA-Z]+\w*[0-9a-zA-Z])/g;

    let m;

    while ((m = regex.exec(str)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }

      // The result can be accessed through the `m`-variable.
      m.forEach((match, groupIndex) => {
        logger.info(`Found match, group ${groupIndex}: ${match}`);
      });
    }
  }
};


module.exports = Helper;
