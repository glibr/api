const Patterns = require('./patterns');
const RequestStrip = require('request-strip');

const UserValidation = {
  update: {
    emailAddress: {type: String, regex: Patterns.EMAIL, required: true},
    handle: {type: String},
    firstName: {type: String},
    lastName: {type: String},
    tagline: {type: String},
    phone: {type: String, regex: Patterns.PHONE},
  },
  address: {
    address_line1: {type: String},
    address_line2: {type: String},
    city: {type: String},
    state: {type: String},
    zipcode: {type: String},
    country: {type: String},
  },
  register: {
    emailAddress: {type: String, regex: Patterns.EMAIL, required: true},
    handle: {type: String, required: true},
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    phone: {type: String, regex: Patterns.PHONE},
    password: {type: String, minLength: 8, required: true},
    userStatusID: {type: Number, default: 202},
    userStatusName: {type: String, default: 'pending'},
    userRoleID: {type: Number, default: 102},
    userRoleName: {type: String, default: 'user'},
  },

  /**
   * Farm logic to this function, grab validations via key
   * @param {number} name
   * @param {number} args
   * @return {Clean}
   */
  clean: function(name, args={}) {
    return RequestStrip.clean(args, this[name]);
  },
};

module.exports = UserValidation;
