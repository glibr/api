'use strict';

const Patterns = {
  PHONE: /(\+)?(\d{1,3})?(\s)?(\()?(\d{0,3})?(\)|\-)?(\s)?\d{3,3}(\-)?(\s)?\d{4,4}/,
  EMAIL: /[a-zA-Z0-9\+\.\_\%\-\+]{1,256}\@[a-zA-Z0-9][a-zA-Z0-9\-]{0,64}(\.[a-zA-Z0-9][a-zA-Z0-9\-]{0,25})/,
};

module.exports = Patterns;
