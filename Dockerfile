FROM node:10

WORKDIR /usr/src/app
#VOLUME [".", "/usr/src/app"]

COPY package*.json ./

RUN npm install
COPY . .

ENV NODE_ENV prod

EXPOSE 5000

CMD [ "npm", "start" ]
