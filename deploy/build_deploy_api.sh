#!/bin/bash
docker stop api
docker rm api
docker build -t glibr-api .
docker run -d --network=multi-host-network --network-alias api -p 49600:5000 --name api glibr-api
