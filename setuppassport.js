const passport = require('passport');
const User = require('./models/user');
const LocalStrategy = require('passport-local').Strategy;

passport.use('login', new LocalStrategy(
    function(username, password, done) {
      User.findOne({emailAddress: username}, function(err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(true, null, 'Invalid email address or password.');
        }

        user.checkPassword(password, function(err, isMatch) {
          if (err) {
            return done(err, user, 'Invalid email address or password.');
          }

          if (isMatch) {
            return done(false, user, 'User successfully logged in.');
          } else {
            return done(true, user, 'Invalid email address or password.');
          }
        });
      });
    }));

module.exports = function() {
  passport.serializeUser(function(user, done) {
    const sessionUser = {_id: user._id};
    done(null, sessionUser);
  });

  passport.deserializeUser(function(sessionUser, done) {
    User.findById(sessionUser, function(err, user) {
      done(err, user);
    });
  });
};
