const mongoose = require('mongoose');

/**
 * Audit Collection
 *
 *  Track transactional things that we want to ... er... track.
 */
const AuditSchema = new mongoose.Schema({

  userIdActor: {type: String, required: true},
  handle: {type: String, required: true},
  // chargeCustomer | deleteGlib | suspendUser | etc.
  action: {type: String, required: true},
  // message
  message: {type: String},

  // -- tracking ids optional, depends on action
  //  stripe charge Token  id
  stripeInvoiceChargeId: {type: String},
  // glibr payment line id
  glibrPaymentInvoiceId: {type: String},
  // glib
  glibId: {type: String},
  // charity
  charityId: {type: String},
  // user
  userIdActedOn: {type: String},

  // timestampsd
  createTimeStamp: {type: Date, required: true},
  updateTimeStamp: {type: Date, required: true},
});

// Immediately export created model, no need to name it.
module.exports = mongoose.model('Audit', AuditSchema);
