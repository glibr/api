const mongoose = require('mongoose');

/**
 * linking users
 */
const UserLinksSchema = new mongoose.Schema({
  primeId: {type: String, required: true},
  followId: {type: String},
  doubleId: {type: String},
  // timestampsd
  createTimeStamp: {type: Date, required: true},
});

// Immediately export created model, no need to name it.
module.exports = mongoose.model('UserLinks', UserLinksSchema);
