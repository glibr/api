const mongoose = require('mongoose');

/**
 * linking groups to users
 * searches can be done for each of the owner/admin/user ids and return a list
 */
const GroupUserLinkSchema = new mongoose.Schema({
  groupId: {type: String, required: true},
  groupName: {type: String, required: true},
  handle: {type: String, required: true},
  ownerId: {type: String},
  adminId: {type: String},
  userId: {type: String},
  pendingJoinId: {type: String},
  suspendedId: {type: String},
  bannedId: {type: String},
  // timestampsd
  createTimeStamp: {type: Date, required: true},
});

// Immediately export created model, no need to name it.
module.exports = mongoose.model('GroupUserLink', GroupUserLinkSchema);
