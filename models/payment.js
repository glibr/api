const mongoose = require('mongoose');

/**
 *
 */
const PaymentSchema = new mongoose.Schema({

  userId: {type: String, required: true},
  // Stripe Customer ID
  stripeCustomerId: {type: String, required: true},
  // card fingerprint id
  stripeCardTokenFingerprint: {type: String, required: true},
  //  stripe charge Token  id
  stripeInvoiceChargeId: {type: String},

  // new |  pending | paid |  denied | error
  stripePaymentStatus: {type: String, required: true},
  // new |  pending | paid |  denied | error
  stripePaymentPaid: {type: String, required: true},
  // Stripe error code
  stripeFailureCode: {type: String},
  // Stripe error message
  stripeFailureMessage: {type: String},

  handle: {type: String, required: true},
  charityId: {type: String, required: true},
  charityName: {type: String, required: true},

  // amount charged
  amount: {type: Number, required: true},

  // when was the payment for this glib submitted
  chargetSubmit_Timestamp: {type: Date},

  // timestampsd
  createTimeStamp: {type: Date, required: true},
  updateTimeStamp: {type: Date, required: true},
});

// Immediately export created model, no need to name it.
module.exports = mongoose.model('Payment', PaymentSchema);
