const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

/**
 * Groups:
 *  name: name for group
 *  type: private || public || protected || inherit
 *  description: description
 *  ownerHandle:  who owns this group
 *  adminHandles: who are admins for this group
 */
const GroupsSchema = new mongoose.Schema({
  name: {type: String, unique: true, required: true},
  nameLower: {type: String, unique: true, required: true},
  category: {type: String, required: true},
  description: {type: String, required: true},
  ownerHandle: {type: String, required: true},
  groupPicture: {type: String},
  website: {type: String},
  isPrivate: {type: Boolean},
  // timestamps
  createTimeStamp: {type: Date, required: true},
  updateTimeStamp: {type: Date, required: true},

});

// Apply the uniqueValidator plugin to userSchema.
GroupsSchema.plugin(uniqueValidator);

// Immediately export created model, no need to name it.
module.exports = mongoose.model('Groups', GroupsSchema);
