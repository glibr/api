const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const SALT_FACTOR = 10; // todo: research what "10" means for this in hashing passwords, increase/decrease this?
mongoose.set('useCreateIndex', true);
// Define "Users" collection Schema
const UserSchema = new mongoose.Schema({
  handle: {type: String, required: true, unique: true},
  handleLower: String,
  tagline: String,
  emailAddress: {type: String, required: true, unique: true},
  lastName: String,
  firstName: String,
  tagline: String,
  profilePicture: String,
  phone: {type: String},
  address_line1: String,
  address_line2: String,
  city: String,
  state: String,
  zipcode: String,
  country: String,
  timezone: String,

  /* eslint-disable no-tabs */
  // User state
  /**
   * user status
  active	201		can read/write on site
  active-provisional 	202		can read/write on site – until penny threshold
	payment-pending	203		can read/write on site

	registration-one-basic	241		can only access registration page 2
	registration-two-charity 	221		can only access registration page 3
	registration-three-payment	222
  registered-inactive	223		can only access activation notice page

  registered-provisional-inactive	231		can only access activation notice page
  registered-provisional-expired	233		over penny threshold

	suspended-payment-failure	241		goes to user problem page and payment widget
	suspended 	242		goes to user problem page with email form
	locked	243
	deactivated	244
	deleted	245

   *
   */
  /* eslint-disable no-tabs */

  userStatusID: Number,
  userStatusName: String,
  userRoleID: Number,
  userRoleName: String,

  // Payment Information
  stripeCustomerId: String,
  stripeCardTokenFingerprint: String,

  // Password
  password: {type: String, required: true},
  failedLoginCount: Number,
  passwordActivationToken: String,
  resetExpiration: Date,
  pwHistory: String,
  // Follows
  followCount: Number,
  followerCount: Number,
  doubleFollowCount: Number,

  // Glibs
  numberOfGlibs: Number,
  totalPennies: Number,
  // Charities
  userCharity: String,
  userCharityId: String,
  userCharityLogo: String,

  // timestamps
  createTimeStamp: {type: Date},
  updateTimeStamp: {type: Date},
});

// Pre-save to hash passwords
const noop = function() { };

UserSchema.pre('save', function(done) {
  const user = this;
  if (!user.isModified('password')) {
    return done();
  }

  bcrypt.genSalt(SALT_FACTOR, function(err, salt) {
    if (err) {
      return done(err);
    }
    bcrypt.hash(user.password, salt, noop, function(err, hashedPassword) {
      if (err) {
        return done(err);
      }
      user.password = hashedPassword;
      done();
    });
  });
});

// Check the user's password guess
UserSchema.methods.checkPassword = function(guess, done) {
  bcrypt.compare(guess, this.password, function(err, isMatch) {
    done(err, isMatch);
  });
};

// Methods on the "Users" Schema
// todo: double check if these methods are even required or needed
UserSchema.methods.name = function() {
  return this.firstName + ' ' + this.lastName;
};

UserSchema.methods.userHandle = function() {
  return this.handle;
};

module.exports = mongoose.model('Users', UserSchema);
