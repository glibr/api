const mongoose = require('mongoose');

/**
 * The Charities
 */
const CharitySchema = new mongoose.Schema({
  name: {type: String, required: true},
  description: {type: String, required: true},
  logo: {type: String, required: true},
  url: {type: String, required: true},
  glibCount: {type: Number, required: true},
  glibPennies: {type: Number, required: true},
  usersChosen: {type: Number, required: true},
});

// Immediately export created model, no need to name it.
module.exports = mongoose.model('Charity', CharitySchema);
