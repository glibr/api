const mongoose = require('mongoose');

/**
 * Depending on what service we use to make payments, and from what I can
 * see. We will need AT LEAST an externalId to associate with the payment.
 *
 * Internally we want to pin it to a specific user, and eventually we will
 * update their status if they are in default.
 *
 * UserID, handle, glibId, charityid, pennies, create_timestamp, status
 */
const GlibPennySchema = new mongoose.Schema({
  userId: {type: String, required: true},
  handle: {type: String, required: true},
  glibId: {type: String, required: true},
  charityId: {type: String, required: true},
  charityName: {type: String, required: true},
  pennies: {type: Number, required: true},
  createTimeStamp: {type: Date, required: true},
  updateTimeStamp: {type: Date, required: true},
  invoiceId: {type: String},
  // new |  pending | paid |  denied | error
  paymentStatus: {type: String, required: true},
  // when was the payment for this glib submitted
  chargetSubmit_Timestamp: {type: Date},
  // Token from Stripe
  invoiceChargeId: {type: String},
});

// Immediately export created model, no need to name it.
module.exports = mongoose.model('GlibPenny', GlibPennySchema);
