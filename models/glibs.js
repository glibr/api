const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
/**
 * The Glibs
 *
 * parentPost: This will contain the unqiue ID of another Glib. If this is not null,
 * then the Glib doc in question is a threaded reply to another Glib.
 *
 * quotedPost: This will contain the unique ID of another Glib. If this is not null,
 * then the Glib doc in question is a copy (share or "Re-Glib") of another Glib.
 *
 * millisecondTimestamp - hmm may not need it if the timeStamp is easily converted.
 *
 * glibrRanking: this is the number used to sort after the number of pennies are
 * taken into account.
 */
const GlibSchema = new mongoose.Schema({
  userId: {type: String, required: true}, // Glibr user unique ID
  glibPost: {type: String, required: true},
  glibDate: {type: String, required: true},
  glibFullDate: {type: String},
  parentPost: {type: Object}, // Glib unique ID
  quotedPost: {type: Object}, // Glib unique ID
  parsedMentions: {type: Array},
  parsedTopics: {type: Array},
  cost: {type: Number, required: true},
  handle: {type: String, required: true},
  cleanHandle: {type: String, required: true},
  author: {type: String, required: true},
  avatar: {type: String},
  timeStamp: {type: Date, required: true},
  millisecondTimeStamp: {type: Number, required: true},
  glibrRanking: {type: Number, required: true},
  glibPictures: {type: Array},
  // glib counters
  glibLikes: {type: Array},
  glibUnlikes: {type: Array},
  glibReglibCount: {type: Number},
  glibReplyCount: {type: Number},
  // Charity
  charityID: {type: String, required: true}, // Charity ID
  charityLogo: {type: String},

  groupId: {type: String},

  glibPrivate: {type: String},
  glibPrivateUserId: {type: String},
  glibPMToHandle: {type: String},
});

// Immediately export created model, no need to name it.
module.exports = mongoose.model('Glib', GlibSchema);
