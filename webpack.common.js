const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const TerserPlugin = require('terser-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const DIST_DIR = 'dist';

// Node module prep.
const nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function(x) {
      return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
      nodeModules[mod] = 'commonjs ' + mod;
    });


module.exports = {
  entry: './server.js',
  target: 'node',
  output: {
    path: path.join(__dirname, DIST_DIR),
    filename: 'glibr.bundle.js',
  },
  externals: nodeModules,
  plugins: [
    new CleanWebpackPlugin([DIST_DIR]),
    new webpack.IgnorePlugin(/\.(css|less)$/),
    new webpack.BannerPlugin({
      banner: 'require("source-map-support").install();',
      raw: true, entryOnly: false,
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(js|.es6)$/,
        exclude: '/node_modules/',
        use: ['babel-loader', 'eslint-loader'],
      },
    ],
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: true,
        sourceMap: true,
      }),
    ],
  },
  resolve: {
    extensions: ['.js', '.es6'],
  },
  devtool: 'sourcemap',
};
