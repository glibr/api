const express = require('express');
// Models
const Glib = require('../models/glibs');

// glibr modules
const Helper = require('../common/helpers');
const GLOBALS = require('../common/globals.js');
const VAULT = require('../common/vault.js');

const logger = require('../common/logger');

//  TEXT PARSING
// make  links into nice clickable links
const linkify = require('linkifyjs');
// #hashtag plugin
require('linkifyjs/plugins/hashtag')(linkify);
// @mention plugin
require('linkifyjs/plugins/mention')(linkify);


const searchRouter = express.Router();

searchRouter.use(function(req, res, next) {
  res.locals.currentUser = req.user;
  next();
});

// Check for a valid Api key
function checkApiKey(req, res, next) {
  if (VAULT.API_KEYS.includes(req.query.apikey) || VAULT.API_KEYS.includes(req.body.apikey)) {
    next();
  } else {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.INVALID_API_KEY,
    });
  }
}


async function returnPostsFromSearch(searchText) {
  // full text search index on glibpost field
  try {
    const searchResults = await Glib.find({$text: {$search: searchText}});
    // .skip(20)
    // .limit(10)
    // .exec(function(err, docs) { ... });
    logger.info('results: ' + searchResults);

    return {
      success: true,
      searchResults: searchResults,
    };
  } catch (err) {
    logger.error(`${GLOBALS.STRINGS.SEARCH_FAILURE} - ${err}`);
    return {
      success: false,
      message: (`${GLOBALS.STRINGS.SEARCH_FAILURE} - ${err}`),
    };
  }
}

// returns a list of glib ids
searchRouter.get('/search/:searchtext', checkApiKey, async (req, res) => {
  Helper.parseHashtags(req.params.searchtext);

  const response = await returnPostsFromSearch(req.params.searchtext);

  if (response.success) {
    res.status(200).send({
      success: true,
      searchResults: response.searchResults,
    });
  } else {
    res.status(400).send({
      success: false,
    });
  }
});

module.exports = searchRouter;
