const express = require('express');
const testRouter = express.Router();

testRouter.get('/timestamplist/:numberoftimestamps', function(req, res) {
  const numberOfTimeStamps = req.params.numberoftimestamps;
  let listOfTimeStamps = '';

  for (let i=0; i < numberOfTimeStamps; i++) {
    // add comma at the end as needed
    if (i>0) listOfTimeStamps += ',';
    // append the raw dateTimeStamp
    listOfTimeStamps += new Date().getTime();
    // pause a second
    sleep(999);
  }

  res.status(200).send({success: true, message: listOfTimeStamps});
});

/**
 * @param {number} miliseconds
 * @returns {object}
 */
function sleep(miliseconds) {
  const currentTime = new Date().getTime();
  while (currentTime + miliseconds >= new Date().getTime()) {
  }
}


module.exports = testRouter;
