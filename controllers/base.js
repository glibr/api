'use strict';

const express = require('express');

/**
 * Controller, a base class to be used for routing of a controller,
 * wraps the express.Router so any calls made to it can mimic those of
 * an express Router.
 * @param {*} model
 *  @returns {object}
 */
const Controller = function(model) {
  // construct a router
  const Router = express.Router();

  // add our model to it
  Router.Model = model;

  // return the newly created router
  return Router;
};

module.exports = Controller;
