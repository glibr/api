const express = require('express');
// Models
const Glib = require('../models/glibs');
const User = require('../models/user');
const GlibPenny = require('../models/glibpennies');

// glibr modules
const Helper = require('../common/helpers');
const GLOBALS = require('../common/globals.js');
const VAULT = require('../common/vault.js');

const logger = require('../common/logger');

//  TEXT PARSING
// make  links into nice clickable links
const linkify = require('linkifyjs');
const linkifyHtml = require('linkifyjs/html');
// #hashtag plugin
require('linkifyjs/plugins/hashtag')(linkify);
// @mention plugin
require('linkifyjs/plugins/mention')(linkify);

const glibRouter = express.Router();

glibRouter.use(function(req, res, next) {
  res.locals.currentUser = req.user;
  next();
});

// Check for a valid Api key
function checkApiKey(req, res, next) {
  if (
    VAULT.API_KEYS.includes(req.query.apikey) ||
    VAULT.API_KEYS.includes(req.body.apikey)
  ) {
    next();
  } else {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.INVALID_API_KEY,
    });
  }
}

// retun current Millisecond is used as the seed for the ranking.
function returnCurrentMillisecond() {
  const milliseconds = new Date().getTime();

  return milliseconds || -1;
}

// Compute glibr rank
// pass in the number of pennies on a post
// compute adjustment and return ranking adjustment.
function glibrSetRanking(pennies, rawTimestamp) {
  return rawTimestamp + getGlibrPennyAdjustment(pennies);
}

function computePenniesInBandValue(
    totalPennies,
    topOfValueBand,
    bottomOfValueBand,
    pennyValue,
) {
  // check to see if totalPennies is less than top
  // this means we are in the highest band somewhere
  if (totalPennies < topOfValueBand) {
    topOfValueBand = totalPennies;
  }

  // subtract top from bottom and multiply by value voila.
  return (topOfValueBand - bottomOfValueBand) * pennyValue;
}

/**
 *  getGlibrPennyAdjustment
 * @param {*} pennies
 * @returns {number}
 * Warning **
 * This function is written for performance
 * Therefore
 * there are magic numbers in the code.
 * if you edit these please note the values in the commented configuration
 * section at the top of the method
 */
//
function getGlibrPennyAdjustment(pennies) {
  // Ranking adjustment bands
  // 1. top tier - 1-25 pennies, rank per each penny value = 10:1
  // 2. 26-50 penny value 8:1
  // 3. 51-110 penny value 6
  // 4. 111-250 4
  // 5. 251-550 2
  // 6. 551-1100 1
  // 7. 1101-10500 .5
  // 8. more than $10.51 on the glib, ranking adjust per additional penny = .1

  let glibrRankAdjustment = 0;
  const setAdjustmentAtSecond = 1000;

  if (pennies <= 25) {
    // pass in ->  total pennies, top of band, bottom of band, penny value
    glibrRankAdjustment = computePenniesInBandValue(pennies, 25, 0, 10);

    // return accrued ranking.
    return glibrRankAdjustment * setAdjustmentAtSecond;
  } else if (pennies > 25 && pennies <= 50) {
    glibrRankAdjustment = computePenniesInBandValue(pennies, 25, 0, 10);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 50, 25, 8);

    // return accrued ranking.
    return glibrRankAdjustment * setAdjustmentAtSecond;
  } else if (pennies > 50 && pennies <= 110) {
    glibrRankAdjustment = computePenniesInBandValue(pennies, 25, 0, 10);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 50, 25, 8);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 110, 50, 6);

    // return accrued ranking.
    return glibrRankAdjustment * setAdjustmentAtSecond;
  } else if (pennies > 110 && pennies <= 250) {
    glibrRankAdjustment = computePenniesInBandValue(pennies, 25, 0, 10);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 50, 25, 8);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 110, 50, 6);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 250, 110, 4);

    // return accrued ranking.
    return glibrRankAdjustment * setAdjustmentAtSecond;
  } else if (pennies > 250 && pennies <= 550) {
    glibrRankAdjustment = computePenniesInBandValue(pennies, 25, 0, 10);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 50, 25, 8);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 110, 50, 6);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 250, 110, 4);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 550, 250, 2);

    // return accrued ranking.
    return glibrRankAdjustment * setAdjustmentAtSecond;
  } else if (pennies > 550 && pennies <= 1100) {
    glibrRankAdjustment = computePenniesInBandValue(pennies, 25, 0, 10);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 50, 25, 8);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 110, 50, 6);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 250, 110, 4);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 550, 250, 2);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 1100, 550, 1);

    // return accrued ranking.
    return glibrRankAdjustment * setAdjustmentAtSecond;
  } else if (pennies > 1100 && pennies <= 10500) {
    glibrRankAdjustment = computePenniesInBandValue(pennies, 25, 0, 10);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 50, 25, 8);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 110, 50, 6);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 250, 110, 4);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 550, 250, 2);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 1100, 550, 1);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 10500, 1100, 0.5);

    // return accrued ranking.
    return glibrRankAdjustment * setAdjustmentAtSecond;
  } else if (pennies > 10500) {
    glibrRankAdjustment = computePenniesInBandValue(pennies, 25, 0, 10);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 50, 25, 8);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 110, 50, 6);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 250, 110, 4);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 550, 250, 2);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 1100, 550, 1);
    glibrRankAdjustment += computePenniesInBandValue(pennies, 10500, 1100, 0.5);
    glibrRankAdjustment += computePenniesInBandValue(
        pennies,
        pennies,
        10500,
        0.1,
    );

    // return accrued ranking.
    return glibrRankAdjustment * setAdjustmentAtSecond;
  } else {
    glibrRankAdjustment = 0;

    // return ranking adjustment as zero.
    return glibrRankAdjustment;
  }
}

/* get user id by handle inner method
 *  we want to save the user id in the groupuserlinks collection.
 * get it
 */
async function getUserIdByHandle(userHandle) {
  let userObj = {
    success: false,
  };

  try {
    const user = await User.findOne({
      handle: userHandle,
    });

    userObj = {
      success: true,
      userId: user._id,
      handle: user.handle,
    };
    return userObj;
  } catch (err) {
    logger.error(`${GLOBALS.STRINGS.GROUPS_GET_USER_ID_FAIL} - ${userHandle}`);
    logger.error(err);
    userObj = {
      success: false,
      errMessage: GLOBALS.STRINGS.GROUPS_GET_USER_ID_FAIL,
    };
    return userObj;
  }
}

async function getUserHandleById(userId) {
  try {
    const userHandleObj = await User.findOne({_id: userId});

    return {
      success: true,
      userId: userHandleObj._id,
      handle: userHandleObj.handle,
    };
  } catch (err) {
    logger.error(`${GLOBALS.STRINGS.GROUPS_GET_USER_ID_FAIL} - ${userId}`);
    logger.error(err);
    return {
      success: false,
      message: GLOBALS.STRINGS.USER_UNABLE_TO_FIND_BY_ID,
    };
  }
}

/**
 * create a new glib
 */
glibRouter.post('/glibs/create', checkApiKey, async (req, res) => {
  // check penny levels and return if out of bounds.
  if (req.body.glibCost < GLOBALS.CONSTANTS.GLIB_MIN_PENNIES) {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.GLIB_PENNIES_TOO_LITTLE,
    });
    return;
  } else if (req.body.glibCost > GLOBALS.CONSTANTS.GLIB_MAX_PENNIES) {
    // protects against rogue API attack with huge penny amount too
    res.status(400).send({
      success: false,
      message:
        GLOBALS.STRINGS.GLIB_PENNIES_TOO_MUCH +
        GLOBALS.CONSTANTS.GLIB_MAX_PENNIES,
    });
    return;
  }

  // check for empty glib post
  if (
    typeof req.body.glibPost === 'undefined' ||
    req.body.glibPost.length <= 0 ||
    req.body.glibPost === null
  ) {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.GLIB_EMPTY_GLIB_POST,
    });
    return;
  }

  // set group id if this is a group post
  let _groupId = null;

  // check for group
  if (typeof req.body.groupId !== 'undefined' && req.body.groupId !== null) {
    _groupId = req.body.groupId;
  }
  // set private if this is a private message
  let _glibPrivate = false;
  // check for private flag
  if (
    typeof req.body.glibPrivate !== 'undefined' &&
    req.body.glibPrivate !== null
  ) {
    _glibPrivate = req.body.glibPrivate;
  }

  // set pm  user Id if this is a private message
  let _glibPrivateUserId = null;
  let _glibPMToHandle = null;
  // check for private pm
  if (
    typeof req.body.glibPrivateUserId !== 'undefined' &&
    req.body.glibPrivateUserId !== null
  ) {
    // set private message id and handle.
    _glibPrivateUserId = req.body.glibPrivateUserId;
    responseGetHandle = await getUserHandleById(req.body.glibPrivateUserId);
    if (responseGetHandle.success) {
      _glibPMToHandle = responseGetHandle.handle;
    }
  }

  // ----------------------------------------------------------
  // Parse Glib Post

  // remove HTML tags from, post to prevent XSS
  //  clean out dirty words along the way
  let glibPostCleaned = Helper.cleanText(req.body.glibPost);

  // recheck length now after cleaning out html tags from post
  // check for empty glib post
  if (glibPostCleaned.length <= 0) {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.GLIB_EMPTY_AFTER_TAG_CLEAN,
    });
    return;
  }

  // LinkifyJS
  //  https://soapbox.github.io/linkifyjs/docs/
  // add back in linked URLS
  glibPostCleaned = linkifyHtml(glibPostCleaned, {
    formatHref: {
      hashtag: (val) =>
        GLOBALS.ENV_VARS.SITE_BASE_URL + '/search/?q=' + val.substr(1),
      mention: (val) => GLOBALS.ENV_VARS.SITE_BASE_URL + val,
    },
    className: {
      mention: 'glibrMention',
      hashtag: 'glibrHashtag',
    },
  });

  const date = new Date();
  const fullDateString = date.toLocaleString('en-US');
  const month = Helper.getMonthByAbbreviatedName(date.getMonth());
  const millisecondTS = returnCurrentMillisecond();

  // this is the ranking adjustment on top of the raw milliseconds
  // this will be the default sorting method
  const glibrRankingNumber = glibrSetRanking(req.body.glibCost, millisecondTS);

  // we create a plain object
  const glib = {
    userId: res.locals.currentUser._id,
    glibPost: glibPostCleaned,
    glibDate: month + ' ' + date.getDate(),
    glibFullDate: fullDateString,
    parentPost: req.body.parentPost || null, // Glib unique ID
    quotedPost: req.body.quotedPost || null, // Glib unique ID
    parsedMentions: req.body.parsedMentions || [],
    parsedTopics: req.body.parsedTopics || [],
    cost: req.body.glibCost || 2,
    handle: '@' + res.locals.currentUser.handle,
    cleanHandle: res.locals.currentUser.handle,
    author:
      res.locals.currentUser.firstName + ' ' + res.locals.currentUser.lastName,
    avatar: res.locals.currentUser.profilePicture || null,
    timeStamp: date,
    millisecondTimeStamp: millisecondTS || 0,
    glibrRanking: glibrRankingNumber || 0,
    glibLikes: [],
    glibUnlikes: [],
    glibReglibCount: 0,
    glibReplyCount: 0,

    // add picture
    // // only one for now - 09/07/2018 - but array structure for future
    // glib.glibPictures.addToSet(req.body.glibPicture);
    glibPictures: [req.body.glibPicture],

    charityID: res.locals.currentUser.userCharityId,
    charityLogo: res.locals.currentUser.userCharityLogo,

    groupId: _groupId,

    glibPrivate: _glibPrivate,
    glibPrivateUserId: _glibPrivateUserId,
    glibPMToHandle: _glibPMToHandle,
  };

  const responseSaveGlib = await saveGlib(glib);

  if (!responseSaveGlib.success) {
    console.log(GLOBALS.STRINGS.GLIB_CREATION_ERROR);
    logger.error(GLOBALS.STRINGS.GLIB_CREATION_ERROR);
    logger.error(err);

    res.status(400).send({
      success: false,
      glibNewText: GLOBALS.STRINGS.GLIB_CREATION_ERROR,
    });
    return;
  }

  // success
  // add a glib counter to the user document
  const responseGlibCountIncrement = await incremementGlibCountForUser(
      res.locals.currentUser.handle,
      glib.cost,
  );

  if (responseGlibCountIncrement.success) {
    console.log(
        'glib count update: ' + responseGlibCountIncrement.numberOfGlibs,
    );
  } else {
    console.log('unable to update glib count');
  }

  // check if this is a reply and increment replies in parent glib
  if (req.body.parentPost !== null) {
    // fire and forget
    await incrementReplyCountInParent(req.body.parentPost);
  }

  if (req.body.quotedPost !== null) {
    // fire and forget
    await incrementReglibCountInParent(req.body.quotedPost);
  }

  // check if this is a reglib and increment reglibs in parent glib

  // check for provisional over threshold.
  // if user is provisional
  // if total pennies for user are over threshold
  // change to provisional end state: registered-provisional-expired
  // navigate to userproblem page
  let responseProvisionalExpired = false;
  if (
    res.locals.currentUser.userStatusName === 'active-provisional' &&
    responseGlibCountIncrement.totalPennies >
      GLOBALS.CONSTANTS.GLIB_PROVISIONAL_PENNY_LIMIT
  ) {
    responseProvisionalExpired = await updateUserProvisionalExpired(
        res.locals.currentUser.handle,
    );
  }

  const glibPennyLine = {
    userId: res.locals.currentUser._id,
    handle: res.locals.currentUser.handle,
    glibId: responseSaveGlib.glib._id,
    charityId: responseSaveGlib.glib.charityID,
    charityName: res.locals.currentUser.userCharity,
    pennies: responseSaveGlib.glib.cost,
    createTimeStamp: millisecondTS,
    updateTimeStamp: millisecondTS,
    paymentStatus: 'new',
  };

  const responseSaveGlibPennyLine = await saveGlibPennyLine(glibPennyLine);

  if (!responseSaveGlibPennyLine.success) {
    console.log(GLOBALS.STRINGS.GLIB_CREATION_ERROR);
    logger.error(GLOBALS.STRINGS.GLIB_CREATION_ERROR);
    logger.error(err);

    res.status(400).send({
      success: false,
      glibNewText: GLOBALS.STRINGS.GLIB_CREATION_ERROR,
    });
    return;
  }

  res.status(200).send({
    success: true,
    glibId: glib._id,
    provisionalExpired: responseProvisionalExpired.success,
  });
  return;
});

async function saveGlib(glib) {
  const glibToSave = Glib({
    ...glib,
  });
  try {
    await glibToSave.save({});

    if (glibToSave) {
      return {
        success: true,
        glib: glibToSave,
      };
    } else {
      return {
        success: false,
        message: GLOBALS.STRINGS.GLIB_CREATION_ERROR,
      };
    }
  } catch (err) {
    logger.error(GLOBALS.STRINGS.GLIB_CREATION_ERROR);
    logger.error(err);

    return {
      success: false,
      message: GLOBALS.STRINGS.GLIB_CREATION_ERROR,
    };
  }
}

async function saveGlibPennyLine(glibPennyLine) {
  const gilbPennyLineToSave = GlibPenny({
    ...glibPennyLine,
  });

  try {
    await gilbPennyLineToSave.save({});

    return {
      success: true,
    };
  } catch (err) {
    logger.error(GLOBALS.STRINGS.GLIB_CREATION_ERROR);
    logger.error(err);

    return {
      success: false,
      message: GLOBALS.STRINGS.GLIB_CREATION_ERROR,
    };
  }
}

async function incrementReplyCountInParent(parentGlibId) {
  try {
    await Glib.findOneAndUpdate(
        {
          _id: parentGlibId,
        },
        {
          $inc: {
            glibReplyCount: 1,
          },
        },
        {
          new: true,
          useFindAndModify: false,
        },
    );

    return {
      success: true,
    };
  } catch (err) {
    logger.error(`Failed to incrememt reply in: ${parentGlibId} - ${err}`);
    return false;
  }
}

async function incrementReglibCountInParent(parentGlibId) {
  try {
    await Glib.findOneAndUpdate(
        {
          _id: parentGlibId,
        },
        {
          $inc: {
            glibReglibCount: 1,
          },
        },
        {
          new: true,
          useFindAndModify: false,
        },
    );

    return {
      success: true,
    };
  } catch (err) {
    logger.error(`Failed to incrememt reglib in: ${parentGlibId} - ${err}`);
  }
}

async function incremementGlibCountForUser(handle, pennies) {
  try {
    const resUserObj = await User.findOneAndUpdate(
        {
          handle: handle,
        },
        {
          $inc: {
            numberOfGlibs: 1,
            totalPennies: pennies,
          },
        },
        {
          new: true,
          useFindAndModify: false,
        },
    );

    return {
      success: true,
      numberOfGlibs: resUserObj.numberOfGlibs,
      totalPennies: resUserObj.totalPennies,
    };
  } catch (err) {
    logger.error(err);
    return {
      success: false,
      numberOfGlibs: 0,
    };
  }
}

async function updateUserProvisionalExpired(handle) {
  try {
    await User.findOneAndUpdate(
        {
          handle: handle,
        },
        {
          $set: {
            userStatusName: 'provisional-expired',
            userStatusID: '233',
          },
        },
        {
          new: true,
          useFindAndModify: false,
        },
    );

    return {
      success: true,
    };
  } catch (err) {
    logger.error(err);
    return {
      success: false,
    };
  }
}

/**
 * Delete Glib
 *
 *  remove a glib.
 */
// var feedQuery = `/glibs/glibdelete/${this.props.glibId}/${deleteType}/${this.props.user.currentUserHandle}`;
glibRouter.get(
    '/glibs/glibdelete/:glibid/:deletetype/:handle',
    checkApiKey,
    async (req, res) => {
    // api.glibr:5000/glibs/glibaddlike/5b89728c13099e2eb00c1a3d/glibrsmith?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      try {
        let canHardDelete = true;
        let glibDeleted = false;
        let glibDeleteText = `Delete attempt by ${req.params.handle} - delete type ${req.params.deletetype}`;

        const userResponse = await getUserIdByHandle(req.params.handle);
        // userResponse.userId

        // 1. check to see if the pennies have been paid yet?
        const isGlibPaidOut = await hasGlibBeenPaid(req.params.glibid);
        // if so then we can't hard delete

        if (isGlibPaidOut.glibPaid) {
        // glibPaid is false
          logger.info(
              `glib delete: glib has been paid out: ${isGlibPaidOut.glibPaid}`,
          );
          canHardDelete = false;
          glibDeleteText = `${GLOBALS.STRINGS.DELETE_GLIB_BY_USER} - ${req.params.handle}`;
        }

        //  2. Check to see if there are any posts with a parentPost ID or quotedPost ID
        // if so then we do not want to do a hard delete.
        const isGlibAParentGlib = await glibHasChildPosts(req.params.glibid);

        if (isGlibAParentGlib.isChildGlib) {
          logger.info(
              `glib delete: glib has child glib: ${isGlibAParentGlib.isChildGlib}`,
          );

          canHardDelete = false;
          glibDeleteText = `${GLOBALS.STRINGS.DELETE_GLIB_BY_USER} - ${req.params.handle}`;
        }
        // 3. reset text for admin
        if (req.params.deletetype === 'admin') {
          glibDeleteText = `${GLOBALS.STRINGS.DELETE_GLIB_BY_ADMIN} - ${req.params.handle}`;
        }
        // 4. reset text for group  admin - most relevant if present
        if (req.params.deletetype === 'group') {
          glibDeleteText = `${GLOBALS.STRINGS.DELETE_GLIB_BY_GROUP_ADMIN} - ${req.params.handle}`;
        }

        logger.info(`can we hard delete this glib?: ${canHardDelete}`);

        // 5. delete glib
        if (canHardDelete) {
        // do a hard delete
          const glibDeleteResponse = await glibHardDelete(req.params.glibid);
          glibDeleted = glibDeleteResponse.success;
        } else {
        // do a soft delete
          const glibDeleteResponse = await glibSoftDelete(
              req.params.glibid,
              glibDeleteText,
          );
          glibDeleted = glibDeleteResponse.success;
        }

        // 6. write Audit line for succesful delete only
        if (glibDeleted) {
          const auditObj = {
            userIdActor: userResponse.userId,
            handle: req.params.handle,
            action: 'delete glib',
            message: glibDeleteText,
            glibId: req.params.glibid,
            userIdActedOn: null,
            createTimeStamp: new Date(),
            updateTimeStamp: new Date(),
          };
          // insert audit line
          Helper.insertAuditLine(auditObj);
        }

        logger.info(glibDeleteText);

        res.status(200).send({
          success: glibDeleted,
          glibId: req.params.glibid,
          message: glibDeleteText,
        });
      } catch (err) {
        logger.error(`${GLOBALS.STRINGS.DELETE_GLIB_FAILED} - ${err}`);
        res.status(400).send({
          success: false,
          glibId: req.params.glibid,
          message: `${GLOBALS.STRINGS.DELETE_GLIB_FAILED} - ${err}`,
        });
      }
    },
);

async function glibSoftDelete(glibId, glibText) {
  logger.info(`in soft delete: ${glibId}`);

  try {
    const glibRedacted = await Glib.findOneAndUpdate(
        {
          _id: glibId,
        },
        {
          $set: {
            glibPost: glibText,
            glibPictures: null,
          },
        },
        {
          new: true,
          useFindAndModify: false,
        },
    );

    if (glibRedacted === null) {
      logger.info('failed to delete: no error');
      return {
        success: false,
      };
    } else {
      logger.info('deleted');
      return {
        success: true,
      };
    }
  } catch (err) {
    logger.error('failed to delete: ' + err);
    return {
      success: false,
    };
  }
}

//  Remove whole glib.  HARD DELETE
async function glibHardDelete(glibId) {
  try {
    logger.info(`in hard delete: ${glibId}`);

    // AND payment entry
    const glibRemoved = await Glib.findOneAndDelete({_id: glibId});

    if (glibRemoved === null) {
      logger.info('failed to delete: no error');
      return {
        success: false,
      };
    } else {
      logger.info('deleted');
      return {
        success: true,
      };
    }
  } catch (err) {
    logger.error('failed to delete: ' + err);
    return {
      success: false,
    };
  }
}

async function glibHasChildPosts(gId) {
  let glibHasChild = null;

  logger.info(`glibHasChild: ${gId}`);

  try {
    // 1. check to see if the pennies have been paid yet?
    glibHasChild = await Glib.find({
      $or: [
        {
          parentPost: gId,
        },
        {
          quotedPost: gId,
        },
      ],
    });

    logger.info(`glibparent: ${glibHasChild.length}`);
    if (glibHasChild.length > 0) {
      return {
        success: true,
        isChildGlib: true,
      };
    } else {
      return {
        success: true,
        isChildGlib: false,
      };
    }
  } catch (err) {
    logger.error(`${GLOBALS.STRINGS.DELETE_FAILED_TO_FIND_PAID_FLAG} - ${err}`);
    return {
      success: false,
      isChildGlib: false,
    };
  }
}

async function hasGlibBeenPaid(gId) {
  let fGlibPaid = null;
  try {
    // 1. check to see if the pennies have been paid yet?
    fGlibPaid = await GlibPenny.findOne({
      $and: [
        {
          glibId: gId,
        },
        {
          paymentStatus: {
            $ne: 'new',
          },
        },
      ],
    });

    if (fGlibPaid === null) {
      return {
        success: true,
        glibPaid: false,
      };
    } else {
      return {
        success: true,
        glibPaid: true,
      };
    }
  } catch (err) {
    logger.error(`${GLOBALS.STRINGS.DELETE_FAILED_TO_FIND_PAID_FLAG} - ${err}`);
    return {
      success: false,
      glibPaid: false,
    };
  }
}

/**
 * Returns integer of number of glibs and number of follows for currently logged in user
 *
 * @returns two separate integers
 */
glibRouter.get(
    '/glibs/numberglibsandfollows',
    checkApiKey,
    (req, res, next) => {
      Glib.count({
        cleanHandle: res.locals.currentUser.handle,
      })
          .exec()
          .then((result) => {
            res.status(200).send({
              success: true,
              glibs: result,
              numberFollows: res.locals.currentUser.followCount,
            });
            return;
          })
          .catch((err) => {
            console.log('can\'t find user by handle: ' + err);
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.GET_USER_GLIBS_ERROR,
            });
          });
      return;
    },
);

/**
 * Returns integer of number of glibs by user handle
 *
 * @returns one integer
 */
glibRouter.get(
    '/glibs/numberglibsbyhandle/:handle',
    checkApiKey,
    (req, res, next) => {
    //   http://api.glibr:5000/glibs/numberglibsbyhandle/gonzo/?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      Glib.count({
        cleanHandle: req.params.handle,
      })
          .count()
          .then((result) => {
            res.status(200).send({
              success: true,
              glibs: result,
            });
            return;
          })
          .catch((err) => {
            console.log('can\'t find user by handle: ' + err);
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.GET_USER_GLIBS_ERROR,
            });
          });
      return;
    },
);

/** ---------------------------------------------------------------------------------
 *
 * Glib Feed Methods
 *
 * ----------------------------------------------------------------------------------*/

/** getGlibFeed()
 * @param {*} glibQuery
 * @param {*} glibSort
 * @param {*} glibCount
 * @param {*} pageCountSkip
 * @returns {object}
 *
 *  Internal method to get a glibFeed from passed in parameters.
 */
async function getGlibFeed(glibQuery, glibSort, glibCount, pageCountSkip) {
  try {
    const glibSkip = pageCountSkip || 0;

    const glibFeed = await Glib.find(glibQuery)
        .sort(glibSort)
        .limit(glibCount)
        .skip(glibSkip)
        .exec();

    if (glibFeed.length > 0) {
      return {
        success: true,
        message: GLOBALS.STRINGS.GET_GLIBFEED_SUCCESS,
        glibCount: glibFeed.length || 0,
        glibFeed: glibFeed,
      };
    } else {
      return {
        success: false,
        message: GLOBALS.STRINGS.GET_GLIBFEED_FAIL,
        glibCount: 0,
        glibFeed: null,
      };
    }
  } catch (err) {
    logger.error(GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_LINKS_FAIL);
    logger.error(err);

    return {
      success: false,
      message: GLOBALS.STRINGS.GET_GLIBFEED_FAIL,
      glibFeed: null,
      glibCount: 0,
    };
  }
}

/**
 * GLIBS get methods
 * Primary list of glibs
 * --> exludes replies with the  parameter  {parentPost: null}
 *  So we don't get replies included.
 */

glibRouter.get(
    '/glibs/glibs/:perPage/:pageNumber/:sort',
    checkApiKey,
    async (req, res) => {
      perPage = parseInt(req.params.perPage) || 10;
      pageNumber = parseInt(req.params.pageNumber) || 1;
      glibSort = {glibrRanking: -1};

      const pagination = {
        limit: perPage,
        skip: perPage * (pageNumber - 1),
      };

      const glibQuery = {
        parentPost: null,
        glibPrivate: 'false',
        groupId: null,
      };

      const glibFeedResult = await getGlibFeed(
          glibQuery,
          glibSort,
          pagination.limit,
          pagination.skip,
      );
      // users = await User.find({<CONDITION>}).limit(pagination.limit).skip(pagination.skip).exec()

      if (glibFeedResult.success) {
        res.status(200).send({
          success: true,
          message: glibFeedResult.message,
          glibCount: glibFeedResult.glibCount,
          glibs: glibFeedResult.glibFeed,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: glibFeedResult.message,
          glibCount: 0,
          glibs: null,
        });
        return;
      }
    },
);

glibRouter.get(
    '/glibs/getFullRawFeed/:numglibs',
    checkApiKey,
    async (req, res) => {
    // mongodb query
    // -> no parent post present
    // - -> order by ranking
    // -> limit it to numGlibs

      const glibQuery = {
        parentPost: null,
        glibPrivate: 'false',
        groupId: null,
      };

      const glibSort = {glibrRanking: -1};

      const numberOfGlibs = parseInt(req.params.numglibs) || 0;

      const skip = 0;

      const glibFeedResult = await getGlibFeed(
          glibQuery,
          glibSort,
          numberOfGlibs,
          skip,
      );

      if (glibFeedResult.success) {
        res.status(200).send({
          success: true,
          message: glibFeedResult.message,
          glibs: glibFeedResult.glibFeed,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: glibFeedResult.message,
          glibs: null,
        });
        return;
      }
    },
);

/**
 * List top 5 most expensive glibs of all time
 *
 * @returns a list of glibs
 */
glibRouter.get(
    '/glibs/getmostexpensiveglibs/:numglibs',
    checkApiKey,
    async (req, res) => {
    // we currently will show replies too, if we want to suppress replies then we will put in {parentPost: null} in the find selector

      const glibQuery = {
        glibPrivate: 'false',
        groupId: null,
      };

      const glibSort = {cost: -1};

      const numberOfGlibs = parseInt(req.params.numglibs) || 0;

      const skip = 0;

      const glibFeedResult = await getGlibFeed(
          glibQuery,
          glibSort,
          numberOfGlibs,
          skip,
      );

      if (glibFeedResult.success) {
        res.status(200).send({
          success: true,
          message: glibFeedResult.message,
          glibs: glibFeedResult.glibFeed,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: glibFeedResult.message,
          glibs: null,
        });
        return;
      }
    },
);

/**
 * List top 5 most expensive glibs of today
 *
 * @returns a list of glibs
 */
glibRouter.get(
    '/glibs/getmostexpensiveglibstoday/:numglibs',
    checkApiKey,
    async (req, res) => {
      const date = new Date();
      date.setHours(0);
      date.setMinutes(0);
      date.setSeconds(0);

      const glibQuery = {
        glibPrivate: 'false',
        groupId: null,
        timeStamp: {
          $gte: date,
        },
      };

      const glibSort = {cost: -1};

      const numberOfGlibs = parseInt(req.params.numglibs) || 0;

      const skip = 0;

      const glibFeedResult = await getGlibFeed(
          glibQuery,
          glibSort,
          numberOfGlibs,
          skip,
      );

      if (glibFeedResult.success) {
        res.status(200).send({
          success: true,
          message: glibFeedResult.message,
          glibs: glibFeedResult.glibFeed,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: glibFeedResult.message,
          glibs: null,
        });
        return;
      }
    },
);

/**
 * List 50 most recent glibs
 *
 * @returns a list of glibs
 */
glibRouter.get('/glibs/getfiftyrecentglibs', checkApiKey, async (req, res) => {
  const glibQuery = {
    glibPrivate: 'false',
    groupId: null,
  };

  const glibSort = {timeStamp: -1};

  const numberOfGlibs = 50;

  const skip = 0;

  const glibFeedResult = await getGlibFeed(
      glibQuery,
      glibSort,
      numberOfGlibs,
      skip,
  );

  if (glibFeedResult.success) {
    res.status(200).send({
      success: true,
      message: glibFeedResult.message,
      glibs: glibFeedResult.glibFeed,
    });
    return;
  } else {
    res.status(400).send({
      success: false,
      message: glibFeedResult.message,
      glibs: null,
    });
    return;
  }
});

/**
 * List replies to current glib
 *
 * @returns a list of glibs
 */
glibRouter.get(
    '/glibs/getrepliestothisglib/:glibId/:perPage/:pageNumber',
    checkApiKey,
    async (req, res) => {
    // getGlibFeed(glibQuery, glibSort, glibCount, pageCountSkip)
      perPage = parseInt(req.params.perPage) || 10;
      pageNumber = parseInt(req.params.pageNumber) || 1;
      const glibSort = {timeStamp: -1};

      const pagination = {
        limit: perPage,
        skip: perPage * (pageNumber - 1),
      };

      const glibQuery = {
        glibPrivate: 'false',
        groupId: null,
        parentPost: req.params.glibId,
      };

      const glibFeedResult = await getGlibFeed(
          glibQuery,
          glibSort,
          pagination.limit,
          pagination.skip,
      );

      if (glibFeedResult.success) {
        res.status(200).send({
          success: true,
          message: glibFeedResult.message,
          glibs: glibFeedResult.glibFeed,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: glibFeedResult.message,
          glibs: null,
        });
        return;
      }
    },
);

/**
 * List all glibs made by a specific user, queried via user handle
 *
 * @returns a list of glibs
 */
glibRouter.get(
    '/glibs/getuserglibs/:handle/:perPage/:pageNumber',
    checkApiKey,
    async (req, res) => {
    // getGlibFeed(glibQuery, glibSort, glibCount, pageCountSkip)
      perPage = parseInt(req.params.perPage) || 10;
      pageNumber = parseInt(req.params.pageNumber) || 1;
      const glibSort = {glibrRanking: -1};

      const pagination = {
        limit: perPage,
        skip: perPage * (pageNumber - 1),
      };

      const glibQuery = {
        glibPrivate: 'false',
        groupId: null,
        cleanHandle: req.params.handle,
      };

      const glibFeedResult = await getGlibFeed(
          glibQuery,
          glibSort,
          pagination.limit,
          pagination.skip,
      );

      if (glibFeedResult.success) {
        res.status(200).send({
          success: true,
          message: glibFeedResult.message,
          glibs: glibFeedResult.glibFeed,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: glibFeedResult.message,
          glibs: null,
        });
        return;
      }
    },
);

/**
 * List all glibs made by group. queried via groupId
 *
 * @returns a list of glibs
 */
glibRouter.get(
    '/glibs/getgroupglibs/:groupId/:numglibs',
    checkApiKey,
    async (req, res) => {
      const glibQuery = {
        glibPrivate: 'false',
        groupId: req.params.groupId,
      };

      const glibSort = {
        glibrRanking: -1,
      };

      const numberOfGlibs = parseInt(req.params.numglibs) || 0;

      const skip = 0;

      const glibFeedResult = await getGlibFeed(
          glibQuery,
          glibSort,
          numberOfGlibs,
          skip,
      );

      if (glibFeedResult.success) {
        res.status(200).send({
          success: true,
          message: glibFeedResult.message,
          glibs: glibFeedResult.glibFeed,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: glibFeedResult.message,
          glibs: null,
        });
        return;
      }
    },
);

// `/glibs/getPrivateMessages/${userFeedHandle}/${storeState.userState.currentUserHandle}/${numGlibs}`;
/**
 * List all glibs made by a specific user, queried via user handle
 *
 * @returns a list of glibs
 */
glibRouter.get(
    '/glibs/getprivatemessages/:primeHandle/:otherhandle/:numglibs',
    checkApiKey,
    async (req, res) => {
      const resUserObj1 = await getUserIdByHandle(req.params.primeHandle);
      const resUserObj2 = await getUserIdByHandle(req.params.otherhandle);

      let glibQuery = {
        glibPrivate: 'true',
        groupId: null,
        $or: [
          {
            cleanHandle: req.params.otherhandle,
            glibPrivateUserId: resUserObj1.userId,
          },
          {
            cleanHandle: req.params.primeHandle,
            glibPrivateUserId: resUserObj2.userId,
          },
        ],
      };

      // if the current user == the primeHandle
      // -> get all glibs that have primehandle as the private message id
      if (
        req.params.primeHandle === req.params.otherhandle &&
      req.params.primeHandle === res.locals.currentUser.handle
      ) {
        glibQuery = {
          glibPrivate: 'true',
          groupId: null,
          $or: [
            {glibPrivateUserId: resUserObj1.userId},
            {
              cleanHandle: req.params.primeHandle,
              glibPrivateUserId: {$ne: null},
            },
          ],
        };
      }

      const glibSort = {
        glibrRanking: -1,
      };
      const numberOfGlibs = parseInt(req.params.numglibs) || 0;

      const skip = 0;

      const glibFeedResult = await getGlibFeed(
          glibQuery,
          glibSort,
          numberOfGlibs,
          skip,
      );

      if (glibFeedResult.success) {
        res.status(200).send({
          success: true,
          message: glibFeedResult.message,
          glibs: glibFeedResult.glibFeed,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: glibFeedResult.message,
          glibs: null,
        });
        return;
      }
    },
);

/**
 * List all glibs made by the currently logged in user, queried via userId
 *
 * @returns a list of glibs
 */
glibRouter.get('/glibs/getcurrentuserglibs', checkApiKey, async (req, res) => {
  const glibQuery = {
    glibPrivate: 'false',
    groupId: null,
    userId: res.locals.currentUser._id,
  };

  const glibSort = {
    glibrRanking: -1,
  };

  const numberOfGlibs = parseInt(req.params.numglibs) || 50;

  const skip = 0;

  const glibFeedResult = await getGlibFeed(
      glibQuery,
      glibSort,
      numberOfGlibs,
      skip,
  );

  if (glibFeedResult.success) {
    res.status(200).send({
      success: true,
      message: glibFeedResult.message,
      glibs: glibFeedResult.glibFeed,
    });
    return;
  } else {
    res.status(400).send({
      success: false,
      message: glibFeedResult.message,
      glibs: null,
    });
    return;
  }
});

/**
 * GLIBS by a list of handles.
 *
 *  can be used for follows, followers or doubles or some other future list of handles.
 *  takes in a list of handles - generally this list comes directly from a user's mongo document
 *  the array for said type of handles (follows, followers, doubled follows)
 */
glibRouter.get(
    '/glibs/getglibsbyhandlelist/:handleList/:numGlibs/:pageNumber',
    checkApiKey,
    async (req, res) => {
    // Check for null handleList
      if (
        typeof req.params.handleList === 'undefined' ||
      req.params.handleList === undefined ||
      req.params.handleList === 'undefined' ||
      req.params.handleList === '"undefined"' ||
      req.params.handleList === '"empty"' ||
      req.params.handleList === 'empty' ||
      req.params.handleList === null ||
      req.params.handleList.length <= 0
      ) {
        res.status(400).send({
          success: false,
          message: 'empty' + GLOBALS.STRINGS.GET_GLIBFEED_MULTI_HANDLE_FEED_FAIL,
          glibs: null,
        });
        return;
      }

      try {
        console.log('before hlist convert: ' + req.params.handleList);
        // Careful - have to replace single ticks with double quotes to be able to JSON.parse (which is stupid)
        let hList = JSON.parse(req.params.handleList.replace(/'/g, '"'));
        console.log('hList: ' + hList);
        if (hList.indexOf(',') > -1) {
          hList = hList.split(',');
        }

        // and we have to JSON.parse to be able to pass in handleList to Mongoose and have it understand it.
        // otherwise it is the wrong type

        perPage = parseInt(req.params.numGlibs) || 50;
        pageNumber = parseInt(req.params.pageNumber) || 1;
        const glibSort = {glibrRanking: -1};

        const pagination = {
          limit: perPage,
          skip: perPage * (pageNumber - 1),
        };

        const glibQuery = {
          glibPrivate: 'false',
          groupId: null,
          userId: {
            $in: hList,
          },
        };

        const glibFeedResult = await getGlibFeed(
            glibQuery,
            glibSort,
            pagination.limit,
            pagination.skip,
        );
        //  const glibFeedResult = await getGlibFeed(glibQuery, glibSort, pagination.limit, pagination.skip);

        if (glibFeedResult.success) {
          res.status(200).send({
            success: true,
            message: glibFeedResult.message,
            glibs: glibFeedResult.glibFeed,
          });
          return;
        } else {
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.GET_GLIBFEED_MULTI_HANDLE_FEED_FAIL,
            glibs: null,
          });
          return;
        }
      } catch (err) {
        logger.error(
            `${GLOBALS.STRINGS.GET_GLIBFEED_MULTI_HANDLE_FEED_FAIL} - ${err}`,
        );
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GET_GLIBFEED_MULTI_HANDLE_FEED_FAIL,
          glibs: null,
        });
        return;
      }
    },
);

/**
 * getSingleGlib2
 *
 *  Return a single glib by its id
 */
glibRouter.get(
    '/glibs/getsingleglib/:glibid',
    checkApiKey,
    (req, res, next) => {
    //   http://api.glibr:5000/glibs/getSingleGligb/5b89728c13099e2eb00c1a3d/?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      console.log(req.params.glibid);
      // Make the glibid into an object first
      const mongoose = require('mongoose');
      const gID = mongoose.Types.ObjectId(req.params.glibid);

      Glib.findOne({
        _id: gID,
      })
          .exec()
          .then((result) => {
            res.status(200).send({
              success: true,
              glib: result,
            });
            return;
          })
          .catch((err) => {
            console.log(err);
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.GET_USER_GLIBS_ERROR,
            });
          });
      return;
    },
);

// -----------------------------------------------------
// GLIB LIKE AND UNLIKE METHODS

/**
 * glib Add Like
 *
 *  add a like to the glib.
 * The technique will be to add a user id to the array of likes.
 */
glibRouter.get(
    '/glibs/glibaddlike/:glibid/:handle',
    checkApiKey,
    (req, res, next) => {
    // api.glibr:5000/glibs/glibaddlike/5b89728c13099e2eb00c1a3d/glibrsmith?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      // Make the glibid into an object first
      const mongoose = require('mongoose');
      const gID = mongoose.Types.ObjectId(req.params.glibid);

      Glib.findOneAndUpdate(
          {
            _id: gID,
          },
          {
            $addToSet: {
              glibLikes: req.params.handle,
            },
          },
          {
            new: true,
            useFindAndModify: false,
          },
          function(err, doc) {
            if (err) {
              console.log('Something wrong when updating data!');
              logger.error(GLOBALS.STRINGS.GLIB_LIKE_ADD_FAILED + ' -' + err);
            }
            console.log(doc);
            res.status(200).send({
              success: true,
              glibLikes: doc.glibLikes,
            });
            return;
          },
      );
    },
);

/**
 * glibremovelike
 *
 *  remove a like from the glib.
 */
glibRouter.get(
    '/glibs/glibremovelike/:glibid/:handle',
    checkApiKey,
    (req, res, next) => {
    // api.glibr:5000/glibs/glibaddlike/5b89728c13099e2eb00c1a3d/glibrsmith?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      // Make the glibid into an object first
      const mongoose = require('mongoose');
      const gID = mongoose.Types.ObjectId(req.params.glibid);

      Glib.findOneAndUpdate(
          {
            _id: gID,
          },
          {
            $pull: {
              glibLikes: req.params.handle,
            },
          },
          {
            new: true,
            useFindAndModify: false,
          },
          function(err, doc) {
            if (err) {
              console.log('Something wrong when updating data!');
              logger.error(GLOBALS.STRINGS.GLIB_LIKE_REMOVE_FAILED + ' -' + err);
            }
            console.log(doc);
            res.status(200).send({
              success: true,
              glibLikes: doc.glibLikes,
            });
            return;
          },
      );
    },
);

/**
 * glib Add UN Like
 *
 *  add a UN like to the glib.
 * The technique will be to add a user id to the array of likes.
 */
glibRouter.get(
    '/glibs/glibaddunlike/:glibid/:handle',
    checkApiKey,
    (req, res, next) => {
    // api.glibr:5000/glibs/glibaddunlike/5b89728c13099e2eb00c1a3d/glibrsmith?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      // Make the glibid into an object first
      const mongoose = require('mongoose');
      const gID = mongoose.Types.ObjectId(req.params.glibid);

      Glib.findOneAndUpdate(
          {
            _id: gID,
          },
          {
            $addToSet: {
              glibUnlikes: req.params.handle,
            },
          },
          {
            new: true,
            useFindAndModify: false,
          },
          function(err, doc) {
            if (err) {
              console.log('Something wrong when updating data!');
              logger.error(GLOBALS.STRINGS.GLIB_UNLIKE_ADD_FAILED + ' -' + err);
            }
            console.log(doc);
            res.status(200).send({
              success: true,
              glibUnlikes: doc.glibUnlikes,
            });
            return;
          },
      );
    },
);

/**
 * glibremoveUNlike
 *
 *  remove a UN like from the glib.
 */
glibRouter.get(
    '/glibs/glibremoveunlike/:glibid/:handle',
    checkApiKey,
    (req, res, next) => {
    // api.glibr:5000/glibs/glibaddlike/5b89728c13099e2eb00c1a3d/glibrsmith?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      // Make the glibid into an object first
      const mongoose = require('mongoose');
      const gID = mongoose.Types.ObjectId(req.params.glibid);

      Glib.findOneAndUpdate(
          {
            _id: gID,
          },
          {
            $pull: {
              glibUnlikes: req.params.handle,
            },
          },
          {
            new: true,
            useFindAndModify: false,
          },
          function(err, doc) {
            if (err) {
              console.log('Something wrong when updating data!');
              logger.error(GLOBALS.STRINGS.GLIB_UNLIKE_REMOVE_FAILED + ' -' + err);
            }
            console.log(doc);
            res.status(200).send({
              success: true,
              glibUnlikes: doc.glibUnlikes,
            });
            return;
          },
      );
    },
);

module.exports = glibRouter;
