const express = require('express');
const Group = require('../models/groups');
const GroupUserLink = require('../models/groupuserlink');
const User = require('../models/user');
const logger = require('../common/logger');

const groupRouter = express.Router();

const GLOBALS = require('../common/globals.js');
const VAULT = require('../common/vault.js');
const Validation = require('../common/validation');

// Check for a valid Api key
function checkApiKey(req, res, next) {
  if (
    VAULT.API_KEYS.includes(req.query.apikey) ||
    VAULT.API_KEYS.includes(req.body.apikey)
  ) {
    next();
  } else {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.INVALID_API_KEY,
    });
  }
}

/**
 *  Ensure authenticated
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns {object}
 */
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    next();
    // res.status(400).send({
    //   success: false,
    //   message: GLOBALS.STRINGS.NOT_AUTHORIZED,
    // });
  }
}

// GET METHODS

/** getGroupList
 *  base method to return group list
 *  handle: the handle of the user
 *  userType: owner | admin | user
 *  @param {*} handle
 * @param {*} userType;
 * @returns {object}
 */
async function getGroupList(handle, userType) {
  const resUserObj = await getUserIdByHandle(handle);
  let ownerId = 0;
  let adminId = 0;
  let userId = 0;

  if (resUserObj.success) {
    if (userType === 'owner') {
      ownerId = resUserObj.userId;
    } else if (userType === 'admin') {
      adminId = resUserObj.userId;
    } else if (userType === 'all') {
      ownerId = resUserObj.userId;
      adminId = resUserObj.userId;
      userId = resUserObj.userId;
    } else {
      // user will also need to return admin
      userId = resUserObj.userId;
      adminId = resUserObj.userId;
    }

    let groupListObj = {
      success: false,
      message: 'no entry',
    };

    try {
      const groupList = await GroupUserLink.find({
        $or: [{ownerId: ownerId}, {adminId: adminId}, {userId: userId}],
      });

      groupListObj = {
        success: true,
        handle: handle,
        usersId: resUserObj.userId,
        grouplist: groupList,
      };
      return groupListObj;
    } catch (err) {
      logger.error(GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_LINKS_FAIL);
      logger.error(err);
      groupListObj = {
        success: false,
        errMessage: GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_LINKS_FAIL,
      };
      return groupListObj;
    }
  }
}

/**
 * Get All Groups
 *
 *  Return the list of Group in alphabetical order.
 */
groupRouter.get('/groups', checkApiKey, (req, res, next) => {
  Group.find({})
      .sort({name: 1})
      .exec()
      .then((result) => {
        res.status(200).send({success: true, Group: result});
      })
      .catch((err) => {
        logger.error(err);
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GET_CHARITIES_FAULT,
        });
      });
});

/**
 * Get single Group by group id
 */
groupRouter.get('/groups/:groupid', checkApiKey, async (req, res) => {
  try {
    const response = await Group.findOne({_id: req.params.groupid});

    if (response !== null) {
      res.status(200).send({
        success: true,
        groupid: req.params.groupid,
        groupName: response.name,
        description: response.description,
        category: response.category,
        ownerHandle: response.ownerHandle,
        groupPicture: response.groupPicture,
        isPrivate: response.isPrivate,
      });
    } else {
      logger.error(`${GLOBALS.STRINGS.GROUPS_CHANGE_USERTYPE_FAIL}`);
      res.status(400).send({
        success: false,
        message: GLOBALS.STRINGS.GROUPS_CHANGE_USERTYPE_FAIL,
      });
    }
  } catch (err) {
    logger.error(`${GLOBALS.STRINGS.GROUPS_CHANGE_USERTYPE_FAIL}  - ${err}`);
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_LINKS_FAIL,
    });
  }
});

/**
 * Get all users in a group
 *
 *  Return the list of users in alphabetical order.
 * Limit 50
 */
groupRouter.get('/groups/:groupId/users', checkApiKey, async (req, res) => {
  let groupUsers = null;

  groupUsers = await getUserList(req.params.groupId, 'all');

  if (groupUsers === null) {
    return res.status(400).send({success: false});
  }

  if (groupUsers.success) {
    res.status(200).send({groupUsers});
  } else {
    res.status(400).send({groupUsers});
  }
});

groupRouter.get(
    '/groups/:groupId/users/:userType',
    checkApiKey,
    async (req, res) => {
      let groupUsers = null;

      groupUsers = await getUserList(req.params.groupId, req.params.userType);

      if (groupUsers === null) {
        return res.status(400).send({success: false});
      }

      if (groupUsers.success) {
        res.status(200).send({groupUsers});
      } else {
        res.status(400).send({groupUsers});
      }
    }
);

// eslint-disable-next-line valid-jsdoc
/** List of users with handle and by type for a group
 *  Currently limited to 50
 */
async function getUserList(feedQuerygroupId, userType) {
  let userListObj = {
    success: false,
    message: 'no entry',
  };

  const feedQuery = {};
  // eslint-disable-next-line camelcase
  const andConditions = [];

  andConditions.push({groupId: feedQuerygroupId});

  if (userType !== 'all') {
    userType += 'Id';
    andConditions.push({[userType]: {$exists: true, $ne: null}});
  }

  feedQuery['$and'] = andConditions;

  let userCount = 0;

  try {
    userCount = await getUserCount(feedQuerygroupId);
    const userList = await GroupUserLink.find(feedQuery);

    userListObj = {
      success: true,
      groupId: feedQuerygroupId,
      count: userCount.count,
      userList: userList,
    };
    return userListObj;
  } catch (err) {
    logger.error(GLOBALS.STRINGS.GROUPS_UNABLE_TO_GET_USER_LIST_FOR_GROUP);
    logger.error(err);
    userListObj = {
      success: false,
      errMessage: GLOBALS.STRINGS.GROUPS_UNABLE_TO_GET_USER_LIST_FOR_GROUP,
    };
    return userListObj;
  }
}

/** Change User Type */
groupRouter.put(
    '/groups/:groupid/users/:userid/:usertype',
    ensureAuthenticated,
    async (req, res) => {
      try {
        const currentUserType = await getGroupUserType(
            req.params.userid,
            req.params.groupid
        );

        // check for valid user types first
        const userTypesList = [
          'user',
          'admin',
          'owner',
          'pendingJoin',
          'suspended',
          'banned',
        ];
        if (userTypesList.indexOf(req.params.usertype) < 0) {
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.GROUPS_CHANGE_USERTYPE_FAIL,
          });
          return;
        }

        if (currentUserType.success) {
        // check for valid update attempt
          if (currentUserType.userType === req.params.usertype) {
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.GROUPS_CHANGE_USERTYPE_FAIL,
            });
            return;
          }

          const mQuery = {};
          const andConditions = [];
          const fromUserType = currentUserType.userType + 'Id';
          const toUserType = req.params.usertype + 'Id';
          andConditions.push({groupId: req.params.groupid});
          andConditions.push({[fromUserType]: [req.params.userid]});
          mQuery['$and'] = andConditions;

          // set currentUserType to null and new usertype to the userid
          const response = await GroupUserLink.findOneAndUpdate(
              mQuery,
              {[fromUserType]: null, [toUserType]: req.params.userid},
              {new: true}
          );

          if (response !== null) {
            res.status(200).send({
              success: true,
              message: `${GLOBALS.STRINGS.GROUPS_CHANGE_USERTYPE_SUCCESS} ${req.params.usertype}`,
            });
          } else {
            logger.error(`${GLOBALS.STRINGS.GROUPS_CHANGE_USERTYPE_FAIL}`);
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.GROUPS_CHANGE_USERTYPE_FAIL,
            });
          }
        }
      } catch (err) {
        logger.error(`${GLOBALS.STRINGS.GROUPS_CHANGE_USERTYPE_FAIL}  - ${err}`);
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_LINKS_FAIL,
        });
      }
    }
);

// eslint-disable-next-line valid-jsdoc
/** Raw Count of all users in a group */
async function getUserCount(groupId) {
  let userListObj = {
    success: false,
    message: 'no users',
  };

  try {
    const userList = await GroupUserLink.find({groupId: groupId});

    userListObj = {
      success: true,
      groupId: groupId,
      count: userList.length,
    };
    return userListObj;
  } catch (err) {
    logger.error(GLOBALS.STRINGS.GROUPS_UNABLE_TO_GET_USER_LIST_FOR_GROUP);
    logger.error(err);
    userListObj = {
      success: false,
      errMessage: GLOBALS.STRINGS.GROUPS_UNABLE_TO_GET_USER_LIST_FOR_GROUP,
    };
    return userListObj;
  }
}
/** getgroupsbyhandle
 *  This method will get all groups under one handle by type of user (or all)
 */
groupRouter.get(
    '/groups/getgroupsbyhandle/:handle/:groupType/',
    checkApiKey,
    async (req, res) => {
      let groups = null;

      groups = await getGroupList(req.params.handle, req.params.groupType);

      if (groups === null) {
        return res.status(400).send({success: false});
      }

      if (groups.success) {
        res.status(200).send({groups});
      } else {
        res.status(400).send({groups});
      }
    }
);

/** getgroupsbyhandle overload for ALL - no passed in groupType
 *  This method will get all groups under one handle
 */
groupRouter.get(
    '/groups/getgroupsbyhandle/:handle',
    checkApiKey,
    async (req, res) => {
      const resGroupList = await getGroupList(req.params.handle, 'all');

      if (resGroupList.success) {
        res.status(200).send({resGroupList});
      } else {
        res.status(400).send({resGroupList});
      }
    }
);

/** getgroupsownedbyhandle
 *  Get all groups that this user OWNS
 */
groupRouter.get(
    '/groups/getgroupsownedbyhandle/:handle',
    checkApiKey,
    async (req, res) => {
    // get list of groups
      const groups = await getGroupList(req.params.handle, 'owner');

      if (groups.success) {
        res.status(200).send({
          groups,
        });
      } else {
        logger.error(
            `${GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_LINKS_FAIL}  - ${err}`
        );
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_LINKS_FAIL,
        });
      }
    }
);

/**  getgroupsasadminbyhandle
 *  Get all groups as an ADMIN
 */
groupRouter.get(
    '/groups/getgroupsasadminbyhandle/:handle',
    checkApiKey,
    async (req, res) => {
    // get list of groups
      const groups = await getGroupList(req.params.handle, 'admin');

      if (groups.success) {
        res.status(200).send({
          groups,
        });
      } else {
        logger.error(
            `${GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_LINKS_FAIL}  - ${err}`
        );
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_LINKS_FAIL,
        });
      }
    }
);

/**  getgroupsasuserhandle
 *  Get all groups that the handle is a regular USER member
 */
groupRouter.get(
    '/groups/getgroupsasuserhandle/:handle',
    checkApiKey,
    async (req, res) => {
    // get list of groups
      const groups = await getGroupList(req.params.handle, 'user');

      if (groups.success) {
        res.status(200).send({
          groups,
        });
      } else {
        logger.error(
            `${GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_LINKS_FAIL}  - ${err}`
        );
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_LINKS_FAIL,
        });
      }
    }
);

/** getUserType
 *  get user type of user for this group
 *
 *  RESTful signature:
 * /groups/:groupId/users/:handle/type
 *  */
groupRouter.get(
    '/groups/getgroupusertype/:handle/:groupId',
    checkApiKey,
    async (req, res) => {
      const groupUserType = await getGroupUserTypeByHandle(
          req.params.handle,
          req.params.groupId
      );

      if (groupUserType.success) {
        res.status(200).send({
          groupUserType,
        });
      } else {
        res.status(400).send({
          groupUserType,
        });
      }
    }
);

// eslint-disable-next-line valid-jsdoc
/** getGroupUserType
 *
 * Internal method to return the usertype for this user for this group
 */
async function getGroupUserTypeByHandle(handle, groupId) {
  const userResponse = await getUserIdByHandle(handle);
  if (userResponse.success) {
    return await getGroupUserType(userResponse.userId, groupId);
  } else {
    return {
      success: false,
      groupUserType: null,
      message: GLOBALS.STRINGS.GROUPS_NO_USER_FOUND_IN_GROUP,
    };
  }
}

async function getGroupUserType(userId, groupId) {
  let groupUserType = null;
  try {
    const findGroupEntry = await GroupUserLink.find({
      $and: [
        {groupId: groupId},
        {
          $or: [
            {adminId: userId},
            {userId: userId},
            {ownerId: userId},
            {pendingJoinId: userId},
            {suspendedId: userId},
            {bannedId: userId},
          ],
        },
      ],
    });

    if (findGroupEntry.length > 0) {
      // graduated now to the weakest/worst type so that any mistake in double entry (there shouldn't be)
      // the user will be returned in the least privaleged type.
      if (
        typeof findGroupEntry[0].ownerId !== 'undefined' &&
        findGroupEntry[0].ownerId !== null
      ) {
        groupUserType = 'owner';
      } else if (
        typeof findGroupEntry[0].adminId !== 'undefined' &&
        findGroupEntry[0].adminId !== null
      ) {
        groupUserType = 'admin';
      } else if (
        typeof findGroupEntry[0].userId !== 'undefined' &&
        findGroupEntry[0].userId !== null
      ) {
        groupUserType = 'user';
      } else if (
        typeof findGroupEntry[0].pendingJoinId !== 'undefined' &&
        findGroupEntry[0].pendingJoinId !== null
      ) {
        groupUserType = 'pendingJoin';
      } else if (
        typeof findGroupEntry[0].suspendedId !== 'undefined' &&
        findGroupEntry[0].suspendedId !== null
      ) {
        groupUserType = 'suspended';
      } else if (
        typeof findGroupEntry[0].bannedId !== 'undefined' &&
        findGroupEntry[0].bannedId !== null
      ) {
        groupUserType = 'banned';
      }

      return {
        success: true,
        message: GLOBALS.STRINGS.GROUPS_YES_USER_FOUND_IN_GROUP,
        group: findGroupEntry[0],
        userType: groupUserType,
      };
    } else {
      return {
        success: false,
        userType: null,
        message: GLOBALS.STRINGS.GROUPS_NO_USER_FOUND_IN_GROUP,
      };
    }
  } catch (err) {
    return {
      success: false,
      userType: null,
      message: `${GLOBALS.STRINGS.GROUPS_NO_USER_FOUND_IN_GROUP} - ${err}`,
    };
  }
}

/** is group member
 *  check by handle and group id if there is a membership.
 */
groupRouter.get(
    '/groups/isgroupmember/:handle/:groupId',
    checkApiKey,
    async (req, res) => {
      try {
        const userResponse = await getUserIdByHandle(req.params.handle);

        const isMember = await getUserGroupLinkItem(
            userResponse.userId,
            req.params.groupId,
            'all'
        );

        if (isMember.success) {
          res.status(200).send({
            success: true,
            userType: isMember.userType,
            message: GLOBALS.STRINGS.GROUPS_GET_USER_IN_GROUP_SUCCESS,
          });
        } else {
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.GROUPS_GET_USER_IN_GROUP_FAIL,
          });
        }
      } catch (err) {
        logger.error(`GLOBALS.STRINGS.GROUPS_GET_USER_IN_GROUP_FAIL ${err}`);
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUPS_GET_USER_IN_GROUP_FAIL,
        });
      }
    }
);

// JOIN GROUPS METHODS

async function getUserGroupLinkItem(userId, groupId, userType) {
  const mQuery = {};
  const andConditions = [];
  let currentUserType = userType;
  andConditions.push({groupId: groupId});

  if (userType !== 'all') {
    userType += 'Id';
    andConditions.push({[userType]: [userId]});
  } else {
    andConditions.push({
      $or: [
        {adminId: [userId]},
        {userId: [userId]},
        {ownerId: [userId]},
        {pendingJoinId: [userId]},
        {suspendedId: [userId]},
        {bannedId: [userId]},
      ],
    });

    const responseForType = await getGroupUserType(userId, groupId);
    if (responseForType.success) {
      currentUserType = responseForType.groupUserType;
    }
  }

  mQuery['$and'] = andConditions;

  let findGroup = {
    success: false,
    message: 'no entry',
  };

  try {
    const findGroupEntry = await GroupUserLink.find(mQuery);

    if (findGroupEntry.length > 0) {
      findGroup = {
        success: true,
        userId: userId,
        userType: currentUserType,
        groupsFound: findGroupEntry.length,
        message: GLOBALS.STRINGS.GROUPS_GET_USER_IN_GROUP_SUCCESS,
      };
      return findGroup;
    } else {
      findGroup = {
        success: false,
        userId: userId,
        userType: currentUserType,
        errMessage: GLOBALS.STRINGS.GROUPS_GET_USER_IN_GROUP_FAIL,
        groupsFound: findGroupEntry.length,
      };
      return findGroup;
    }
  } catch (err) {
    logger.error(GLOBALS.STRINGS.GROUPS_GET_USER_IN_GROUP_FAIL);
    logger.error(err);
    findGroup = {
      success: false,
      errMessage: GLOBALS.STRINGS.GROUPS_GET_USER_IN_GROUP_FAIL,
    };

    return findGroup;
  }
}

groupRouter.get(
    '/groups/deleteusergrouplink/:handle/:groupId/:userType',
    checkApiKey,
    async (req, res) => {
    // api.glibr:5000/groups/deleteusergrouplink/gregger/5c53e68d5b08ab2b9d1e7aff/user?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      const resUserObj = await getUserIdByHandle(req.params.handle);

      const resDeleteLInk = await deleteUserGroupLink(
          resUserObj.userId,
          req.params.groupId,
          req.params.userType
      );

      if (resDeleteLInk.success) {
        res.status(200).send({
          success: true,
          message: resDeleteLInk.message,
        });
      } else {
        res.status(400).send({
          success: false,
          message: resDeleteLInk.message,
        });
      }
    }
);

async function deleteUserGroupLink(userId, groupId, userType) {
  // check for user-group-usertype match with newUserType
  const resGroupFindMatch = await getUserGroupLinkItem(
      userId,
      groupId,
      userType
  );

  // looking for a match
  if (resGroupFindMatch.success) {
    let ownId = 0;
    let aId = 0;
    let uId = 0;

    if (userType === 'owner') {
      ownId = userId;
    } else if (userType === 'admin') {
      aId = userId;
    } else if (userType === 'all') {
      aId = ownId = uId = userId;
    } else {
      uId = userId;
    }

    // delete matching item
    try {
      await GroupUserLink.deleteMany({
        $and: [
          {groupId: groupId},
          {$or: [{adminId: aId}, {userId: uId}, {ownerId: ownId}]},
        ],
      });
      return {
        success: true,
        message: GLOBALS.STRINGS.GROUPS_USER_LINK_DELETE_SUCCESS,
      };
    } catch (err) {
      logger.error(GLOBALS.STRINGS.GROUPS_USER_LINK_DELETE_FAIL);
      logger.error(err);
      return {
        success: false,
        message: GLOBALS.STRINGS.GROUPS_USER_LINK_DELETE_FAIL,
      };
    }
  } else {
    logger.info(GLOBALS.STRINGS.GROUPS_USER_LINK_NO_GROUP_MATCH);
    return {
      success: false,
      message: GLOBALS.STRINGS.GROUPS_USER_LINK_NO_GROUP_MATCH,
    };
  }
}

/** joingroup
 *  add a regular user to a group
 * Add user to group as type ---
 *  /groups/:groupId/users/:handle/:userType
 */
groupRouter.post(
    '/groups/:groupId/users/:handle/:userType',
    ensureAuthenticated,
    async (req, res) => {
    // groupRouter.get('/groups/joingroup/:handle/:groupId', checkApiKey, async (req, res) => {
    // as user
    // check for valid user types first
      const userTypesList = [
        'user',
        'admin',
        'owner',
        'pendingJoin',
        'suspended',
        'banned',
      ];
      if (userTypesList.indexOf(req.params.userType) < 0) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUPS_CREATE_USER_GROUP_FAIL_WRONG_USERTYPE,
        });
      }

      const resUserObj = await getUserIdByHandle(req.params.handle);
      // check for membership to this group

      // is user already associated with this group as any type then don't proceed.
      const resGroupFindMatch = await getUserGroupLinkItem(
          resUserObj.userId,
          req.params.groupId,
          'all'
      );

      // false is the correct flag value to proceed  == no match yet.
      if (!resGroupFindMatch.success) {
      // we did not find a matching user entry
        const resGroup = await getGroupById(req.params.groupId);

        if (resGroup.success) {
          const resGroupAssign = await assignGroupToUser(
              req.params.groupId,
              resGroup.group.name,
              req.params.userType,
              req.params.handle
          );

          if (resGroupAssign.success) {
            res.status(200).send({
              success: true,
              message: GLOBALS.STRINGS.GROUPS_USER_LINK_SUCCESS,
              findMatch: resGroupFindMatch,
            });
          } else {
          // failed to assign group
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.GROUPS_CREATE_USER_GROUP_LINK_FAIL,
              findMatch: resGroupFindMatch,
            });
          }
        } else {
        // this is the success branch - we DID find a match and therfore did not insert the user.
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.GROUPS_CREATE_USER_GROUP_LINK_FAIL,
            findMatch: resGroupFindMatch,
          });
        }
      } else {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUPS_CREATE_USER_GROUP_LINK_FAIL,
          findMatch: resGroupFindMatch,
        });
      }
    }
);

/** Update Group
 * method to update a group
 */
groupRouter.put(
    '/groups/update/:groupid',
    ensureAuthenticated,
    async (req, res) => {
      logger.info('in group update');
      logger.info('groupid: ' + req.params.groupid);
      logger.info('name: ' + req.body.name);

      // validate stuff first
      // NAME: length (55), badwords, alphanumeric
      if (!Validation.isTextLessThan55Characters(req.body.name)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_NAME_TOO_LONG,
        });
        return;
      }
      // isAlphaNumericOnly
      if (!Validation.isAlphaNumericOnly(req.body.name)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_NAME_MUST_BE_ALPHANUMERIC,
        });
        return;
      }

      if (!Validation.noBadWords(req.body.name)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_NAME_NO_BAD_WORDS,
        });
        return;
      }

      if (!Validation.isTextLessThan55Characters(req.body.nameLower)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_NAME_TOO_LONG,
        });
        return;
      }
      // isAlphaNumericOnly
      if (!Validation.isAlphaNumericOnly(req.body.nameLower)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_NAME_MUST_BE_ALPHANUMERIC,
        });
        return;
      }

      if (!Validation.noBadWords(req.body.nameLower)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_NAME_NO_BAD_WORDS,
        });
        return;
      }

      // DESCRIPTION: length (1000), badwords
      if (!Validation.isTextLessThan1000Characters(req.body.description)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_DESCRIPTION_TOO_LONG,
        });
        return;
      }
      if (!Validation.noBadWords(req.body.description)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_DESCRIPTION_NO_BAD_WORDS,
        });
        return;
      }

      // CATEGORY: length (55), badwords, alphanumeric
      if (!Validation.isTextLessThan55Characters(req.body.category)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_CATEGORY_TOO_LONG,
        });
        return;
      }
      if (!Validation.noBadWords(req.body.category)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_CATEGORY_NO_BAD_WORDS,
        });
        return;
      }
      // isAlphaNumericOnly
      if (!Validation.isAlphaNumericOnly(req.body.category)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_CATEGORY_MUST_BE_ALPHANUMERIC,
        });
        return;
      }

      // get original group
      const groupResponse = await getGroupById(req.params.groupid);
      let groupCurrent = null;

      if (groupResponse.success) {
        groupCurrent = groupResponse.group;
      } else {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_BY_ID_FAIL,
        });
      }

      console.log('check is private: ' + req.body.isPrivate);
      // build new group
      const groupObj = {
        name: req.body.name.trim(),
        nameLower: req.body.nameLower.trim(),
        description: req.body.description,
        category: req.body.category.trim(),
        ownerHandle: groupCurrent.ownerHandle, // never update owner handle
        groupPicture: req.body.groupPicture,
        isPrivate: req.body.isPrivate, // convert to real boolean
        createTimeStamp: groupCurrent.createTimeStamp || new Date(),
        updateTimeStamp: new Date(),
      };

      // Update
      try {
        const response = await Group.findOneAndUpdate(
            {_id: req.params.groupid},
            {...groupObj}, // spread it
            {new: true}
        );

        if (response !== null) {
          await updateGroupName(req.params.groupid, groupObj.name);
          res.status(200).send({
            success: true,
            message: `${GLOBALS.STRINGS.GROUPS_UPDATE_SUCCESS}: ${groupObj.name}`,
          });
        } else {
          logger.error(`${GLOBALS.STRINGS.GROUPS_UPDATE_FAIL} ${groupObj.name}`);
          res.status(400).send({
            success: false,
            message: `${GLOBALS.STRINGS.GROUPS_UPDATE_FAIL} ${groupObj.name}`,
          });
        }
      } catch (err) {
        console.log(err);
        res.status(400).send({
          success: false,
          message: `${GLOBALS.STRINGS.GROUPS_UPDATE_FAIL} ${groupObj.name} error message: ${err}`,
        });
      }
    }
);

async function updateGroupName(groupId, newName) {
  try {
    console.log('updating group name in groupuserlinks: id - ' + groupId);
    console.log('new group name: ' + newName);

    const response = await GroupUserLink.updateMany(
        {groupId: groupId},
        {groupName: newName},
        {new: true}
    );

    if (response !== null) {
      console.log('it worked: ' + JSON.stringify(response));
      return true;
    } else {
      logger.error(
          'failed to update group name in the groupuserlinks collection: ' +
          groupId
      );
      return false;
    }
  } catch (err) {
    console.log(err);
    return false;
  }
}

/** Create Group
 * method to create a group.
 * It will also create a groupuserlink entry for the owner
 */
groupRouter.post(
    '/groups/creategroup',
    ensureAuthenticated,
    async (req, res) => {
      logger.info('name: ' + req.body.name);
      logger.info('ownere handle: ' + req.body.ownerHandle);

      const groupObj = {
        name: req.body.name.trim(),
        nameLower: req.body.nameLower.trim(),
        description: req.body.description,
        category: req.body.category.trim(),
        ownerHandle: req.body.ownerHandle,
        groupPicture: req.body.groupPicture,
        isPrivate: req.body.isPrivate,
        createTimeStamp: new Date(),
        updateTimeStamp: new Date(),
      };

      // server validation of fields

      // NAME: length (55), badwords, alphanumeric, dupe
      if (!Validation.isTextLessThan55Characters(groupObj.name)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_NAME_TOO_LONG,
        });
        return;
      }
      // isAlphaNumericOnly
      if (!Validation.isAlphaNumericOnly(groupObj.name)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_NAME_MUST_BE_ALPHANUMERIC,
        });
        return;
      }

      if (!Validation.noBadWords(groupObj.name)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_NAME_NO_BAD_WORDS,
        });
        return;
      }

      const responseObj = await checkForDupe(groupObj.name);
      if (responseObj.success) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_YES_DUPE_FOUND,
        });
        return;
      }

      // DESCRIPTION: length (1000), badwords
      if (!Validation.isTextLessThan1000Characters(groupObj.description)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_DESCRIPTION_TOO_LONG,
        });
        return;
      }
      if (!Validation.noBadWords(groupObj.description)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_DESCRIPTION_NO_BAD_WORDS,
        });
        return;
      }

      // CATEGORY: length (55), badwords, alphanumeric
      if (!Validation.isTextLessThan55Characters(groupObj.category)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_CATEGORY_TOO_LONG,
        });
        return;
      }
      if (!Validation.noBadWords(groupObj.category)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_CATEGORY_NO_BAD_WORDS,
        });
        return;
      }
      // isAlphaNumericOnly
      if (!Validation.isAlphaNumericOnly(groupObj.category)) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUP_CATEGORY_MUST_BE_ALPHANUMERIC,
        });
        return;
      }

      const resGroup = await createGroup(groupObj);

      if (resGroup.success) {
      // assign group as owner
        const resGroupAssign = await assignGroupToUser(
            resGroup.groupId,
            req.body.name,
            'owner',
            req.body.ownerHandle
        );

        if (resGroupAssign.success) {
          res.status(200).send({
            success: true,
            groupId: resGroup.groupId,
            message: GLOBALS.STRINGS.GROUPS_CREATE_SUCCESS,
          });
        } else {
        // failed to assign group
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.GROUPS_CREATE_USER_GROUP_LINK_FAIL,
          });
        }
      } else {
        logger.error(GLOBALS.STRINGS.GROUPS_CREATE_FAIL);
        logger.error(resGroup.errMessage);

        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUPS_CREATE_FAIL,
          err: resGroup.errMessage,
        });
      }
    }
);

async function createGroup(gObj) {
  const groupObj = Group({
    ...gObj,
  });

  logger.info(
      'name in inner create: ' + groupObj.name + ' - ' + groupObj.ownerHandle
  );

  try {
    await groupObj.save();

    return {
      success: true,
      groupId: groupObj._id,
    };
  } catch (err) {
    return {
      success: false,
      errMessage: err,
    };
  }
}

/* Assign Group to User
 *  put the user id in the correct entity in a new document.
 */
async function assignGroupToUser(groupId, groupName, userType, handle) {
  logger.info(`Assign Group to User: ${handle} - ${userType} - ${groupName}`);

  const resUserObj = await getUserIdByHandle(handle);

  const assignedUserType = userType + 'Id';

  if (resUserObj.success) {
    const userGroupLink = new GroupUserLink({
      groupId: groupId,
      groupName: groupName,
      handle: handle,
      createTimeStamp: new Date(),
    });

    userGroupLink[assignedUserType] = resUserObj.userId;

    try {
      const userGroupLinkResult = await userGroupLink.save();
      return {
        success: true,
        userGroupLinkId: userGroupLinkResult._id,
      };
    } catch (err) {
      logger.error(GLOBALS.STRINGS.GROUPS_CREATE_USER_GROUP_LINK_FAIL);
      logger.error(err);
      return {
        success: false,
        errMessage: err,
      };
    }
  } else {
    logger.error(GLOBALS.STRINGS.GROUPS_CREATE_USER_GROUP_LINK_FAIL);
    return {
      success: false,
      errMessage: err,
    };
  }
}

/* get user id by handle inner method
 *  we want to save the user id in the groupuserlinks collection.
 * get it
 */
async function getUserIdByHandle(userHandle) {
  let userObj = {
    success: false,
  };

  try {
    const user = await User.findOne({
      handle: userHandle,
    });

    userObj = {
      success: true,
      userId: user._id,
      handle: user.handle,
    };
    return userObj;
  } catch (err) {
    logger.error(GLOBALS.STRINGS.GROUPS_GET_USER_ID_FAIL);
    logger.error(err);
    userObj = {
      success: false,
      errMessage: GLOBALS.STRINGS.GROUPS_GET_USER_ID_FAIL,
    };
    return userObj;
  }
}

groupRouter.get(
    '/groups/getgroupbyid/:groupId',
    checkApiKey,
    async (req, res) => {
      const groupResponse = await getGroupById(req.params.groupId);

      if (groupResponse.success) {
        res.status(200).send({
          success: true,
          message: GLOBALS.STRINGS.GROUPS_RETRIEVE_BY_ID_SUCCESS,
          group: groupResponse.group,
        });
      } else {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_BY_ID_FAIL,
        });
      }
    }
);

async function getGroupById(groupId) {
  let groupObj = {
    success: false,
  };

  console.log('inner: ' + groupId);

  // Make the glibid into an object first
  const mongoose = require('mongoose');
  const gID = mongoose.Types.ObjectId(groupId);

  try {
    const group = await Group.findOne({
      _id: gID,
    });

    groupObj = {
      success: true,
      group: group,
    };
    return groupObj;
  } catch (err) {
    logger.error(GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_BY_ID_FAIL);
    logger.error(err);
    groupObj = {
      success: false,
      errMessage: GLOBALS.STRINGS.GROUPS_RETRIEVE_GROUP_BY_ID_FAIL,
    };
    return groupObj;
  }
}

groupRouter.get(
    '/groups/checkforgroupdupe/:stringtocheck',
    checkApiKey,
    async (req, res) => {
      logger.info('in check for existing group');

      const responseObj = await checkForDupe(req.params.stringtocheck);

      logger.info(
          'checking for duplicate group: ' + responseObj.success.toString()
      );

      if (responseObj.success) {
        res.status(200).send({
          success: true,
          user: responseObj.message,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: responseObj.message,
        });
        return;
      }
    }
);

async function checkForDupe(groupNameToCheck) {
  try {
    const stringLowerToCheck = groupNameToCheck.toLowerCase().trim();
    logger.info(stringLowerToCheck);

    const foundGroup = await Group.findOne({nameLower: stringLowerToCheck});

    if (foundGroup) {
      return {
        success: true,
        message: GLOBALS.STRINGS.GROUP_YES_DUPE_FOUND,
      };
    } else {
      return {
        success: false,
        message: GLOBALS.STRINGS.GROUP_NO_DUPE_FOUND,
      };
    }
  } catch (err) {
    logger.error(err);
    return {
      success: false,
      message: `Failed in group duplicate check: ${err}`,
    };
  }
}

module.exports = groupRouter;
