const express = require('express');

// models
const Payment = require('../models/payment');
const GlibPenny = require('../models/glibpennies');
const User = require('../models/user');
const Audit = require('../models/audit');

// glibr libraries
const GLOBALS = require('../common/globals.js');
const VAULT = require('../common/vault.js');

// used for idempotent id for charge to allow for re-submit on same charge with no dupe.
// const uuidv4 = require('uuid/v4');
// Not yet coded into the flow yet.  TODO

const stripe = require('stripe')(VAULT.STRIPE_PRIVATE_KEY);

const paymentRouter = express.Router();

paymentRouter.use(function(req, res, next) {
  res.locals.currentUser = req.user;
  next();
});

// Check for a valid Api key
function checkApiKey(req, res, next) {
  if (
    VAULT.API_KEYS.includes(req.query.apikey) ||
    VAULT.API_KEYS.includes(req.body.apikey)
  ) {
    next();
  } else {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.INVALID_API_KEY,
    });
  }
}

// Set up response headers, as well as generic error check
// function pre(res, params) {
//   res.header('Content-Type', 'application/json');
//   error(res, params.id, 'Invalid id provided');
// }

/**
 *  Charge customer post method
 *
 *  Takes in a handle and figures the rest out.
 */
paymentRouter.post('/payments/chargeuser', checkApiKey, async (req, res) => {
  const chargeResult = await chargeUser(req.body.handle);

  if (chargeResult.success) {
    res.status(200).send({
      success: chargeResult.success,
      message: chargeResult.message,
      stripeStatus: chargeResult.stripeStatus,
      stripePaid: chargeResult.stripePaid,
      amount: chargeResult.amountCharged,
    });
  } else {
    res.status(400).send({
      success: false,
      message: chargeResult.message,
      stripeStatus: 'fail',
      stripePaid: 'fail',
      amount: 0,
    });
  }
});

/**
 * getStripeToken
 *
 * @returns stripe public key
 */
paymentRouter.get('/payments/getStripePublicKey/:env', (req, res, next) => {
  try {
    res.status(200).send({
      success: true,
      stripeKey: VAULT.STRIPE_PUBLIC_KEY,
    });
  } catch (e) {
    // Unable to get stripe key

    res.status(400).send({
      success: false,
      stripeKey: '',
    });
  }
});

/**
 * GLIBS by a list of handles.
 *
 *  can be used for follows, followers or doubles or some other future list of handles.
 *  takes in a list of handles - generally this list comes directly from a user's mongo document
 *  the array for said type of handles (follows, followers, doubled follows)
 */
paymentRouter.get(
    '/payments/paymentslist/:handle',
    checkApiKey,
    (req, res, next) => {
      GlibPenny.find({
        handle: req.params.handle,
      })
          .exec()
          .then((result) => {
            res.status(200).send({
              success: true,
              glibChargeLines: result,
            });
          });
    }
);

/**
 *  penniesaccruedbyhandlepaymentstate
 *
 *  return accrued pennies by handle and payment state
 *
 */
paymentRouter.get(
    '/payments/penniesaccruedbyhandlepaymentstate/:handle/:paymentstate',
    checkApiKey,
    (req, res, next) => {
      getPenniesByHandlePaymentState(
          req.params.handle,
          req.params.paymentstate,
          function(response) {
            if (response.length === 0 || response === 0) {
              // no pennies found
              res.status(400).send({
                success: false,
                penniesAccrued: null,
              });
            } else {
              // good result
              res.status(200).send({
                success: true,
                penniesAccrued: response,
              });
            }
          }
      );
    }
);

/**
 *  userpennyaccruelist
 *
 *  return the full list of pennies accrued by user
 *
 */
paymentRouter.get(
    '/payments/userpennyaccruelist/:paymentstate',
    checkApiKey,
    (req, res, next) => {
      GlibPenny.aggregate([
        {
          $match: {
            paymentStatus: req.params.paymentstate,
          },
        },
        {
          $group: {
            _id: {
              handle: '$handle',
              userId: '$userId',
              charityName: '$charityName',
              charityId: '$charityId',
            },
            totalAccruedNewPennies: {
              $sum: '$pennies',
            },
            count: {
              $sum: 1,
            },
          },
        },
        {$sort: {totalAccruedNewPennies: -1}},
      ])
          .exec()
          .then((result) => {
            res.status(200).send({
              success: true,
              userPenniesAccrued: result,
            });
          });
    }
);

/**
 *  Check for unique card token via the fingerprint attribute
 *
 * https://blog.serverdensity.com/checking-if-a-document-exists-mongodb-slow-findone-vs-find/
 * https://stripe.com/docs/api/tokens/retrieve?lang=node
 * https://scotch.io/tutorials/how-to-use-the-javascript-fetch-api-to-get-data
 */
paymentRouter.get(
    '/payments/checkuniquestripecardtoken/:token',
    checkApiKey,
    (req, res, nex) => {
      stripe.tokens.retrieve(req.params.token, function(err, stripeToken) {
        if (err) {
          logger.error('fail to find token: ' + err);

          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.ACCOUNT_STRIPE_TOKEN_CHECK_FAILED,
            unique: false,
            stripeCardTokenFingerprint: null,
          });
        }

        // lookup fingerprint in the glibr database.
        //  don't use findOne its slow!!
        User.count({
          stripeCardTokenFingerprint: stripeToken.card.fingerprint,
        })
            .limit(1)
            .exec(function(err, count) {
              if (count <= 0 || err) {
                // No finding of the stripe fingerprint.  Then return true and the fingerprint

                res.status(200).send({
                  success: true,
                  message: GLOBALS.STRINGS.ACCOUNT_STRIPE_TOKEN_UNIQUE,
                  unique: true,
                  stripeCardTokenFingerprint: stripeToken.card.fingerprint,
                });
              } else {
                // Found a match - therefore send back a false and not unique

                res.status(400).send({
                  success: false,
                  message: GLOBALS.STRINGS.ACCOUNT_STRIPE_TOKEN_NOT_UNIQUE,
                  unique: false,
                  stripeCardTokenFingerprint: null,
                });
              }
            }); // end glibr db lookup for match on fingerprint
      }); // end Stripe lookup to get fingerprint
    }
);

// ----  Charge Customer  ----- //

/**
 *  chargeUser
 *  internal method to charge a user using stripe
 *
 *  This method does a lot
 * It gets the user, gets the most recent accurate penny amount,
 * charges the stripe api, tracks a new payment line, updates the glib penny lines
 * and in the case of error, sets the user state correctly.
 *
 * In theory glibr exposure will eventually be just $3.00 when we check our systems enough.
 *
 * TODO - put in curcuit breaker here to prevent huge fraudulent charges.
 * @param {*} userHandle
 * @returns {object}
 */
async function chargeUser(userHandle) {
  const chargeResult = {
    success: false,
    message: '',
    stripeStatus: null,
    stripePaid: null,
    amountCharged: 0,
  };

  // const chargeIdempotencyKey = uuidv4(); // key to use for re-sending the same charge.

  // get our user
  const userObj = await getUserPaymentDetails(userHandle);
  if (!userObj) {
    chargeResult.message =
      GLOBALS.STRINGS.CHARGE_FAILED_TO_FIND_USER + userHandle;
    return chargeResult;
  }

  // set pennies from new to pending
  let fPaymentLinesUpdated = await updateGlibPaymentLineStatus(
      userHandle,
      'new',
      'pending',
      null
  );
  if (!fPaymentLinesUpdated) {
    chargeResult.message = GLOBALS.STRINGS.CHARGE_UNABLE_TO_UPDATE_PENNY_LINES;
    return chargeResult;
  }

  // get most up to date penny count
  const penniesAccruedObj = await getPenniesByHandlePaymentState(
      userHandle,
      'pending'
  );

  if (
    penniesAccruedObj.length === 0 ||
    penniesAccruedObj[0].accruedPennies === 0
  ) {
    // no pennies found
    chargeResult.message =
      GLOBALS.STRINGS.CHARGE_NO_PENNIES_ACCRUED + userHandle;
    return chargeResult;
  }
  // check penny threshold and exit if it is too small
  if (
    penniesAccruedObj[0].accruedPennies <
    GLOBALS.CONSTANTS.GLIB_BILLING_THRESHOLD
  ) {
    chargeResult.message =
      GLOBALS.STRINGS.CHARGE_PENNIES_ACCRUED_BELOW_BILLING_THRESHOLD;
    // reset pending pennies back to 'new
    await updateGlibPaymentLineStatus(userHandle, 'pending', 'new', null);
    return chargeResult;
  }

  // build stripe object
  const stripeObj = {
    amount: penniesAccruedObj[0].accruedPennies,
    currency: 'usd',
    customer: userObj.stripeCustomerId,
    description:
      'Charge for user handle: ' +
      userObj.handle +
      ' - name: ' +
      userObj.firstName +
      ' ' +
      userObj.lastName +
      ' - email: ' +
      userObj.emailAddress,
    receipt_email: userObj.emailAddress,
    // we can add meta data to this call - for example maybe the full list of glibs
  };

  // call stripe and charge user
  const stripeChargeResponse = await createStripeCharge(stripeObj);
  if (!stripeChargeResponse.success) {
    chargeResult.message =
      GLOBALS.STRINGS.CHARGE_FAILED_TO_CHARGE_STRIPE +
      stripeChargeResponse.stripeResult; // will be err object
    // reset penny lines
    await updateGlibPaymentLineStatus(userHandle, 'pending', 'new', null);
    return chargeResult;
  }

  const chargePaymentObj = {
    userId: userObj.userId,
    stripeCustomerId: userObj.stripeCustomerId,
    stripeCardTokenFingerprint: userObj.stripeCardTokenFingerprint,
    // insert stripe invoice id into payment invoice document
    stripeInvoiceChargeId: stripeChargeResponse.stripeResult.id,
    stripePaymentStatus: stripeChargeResponse.stripeResult.status,
    stripePaymentPaid: stripeChargeResponse.stripeResult.paid,
    handle: userObj.handle,
    charityId: userObj.userCharityId,
    charityName: userObj.userCharity,
    amount: stripeChargeResponse.stripeResult.amount,
    chargetSubmit_Timestamp: stripeChargeResponse.stripeResult.created,
    createTimeStamp: new Date(),
    updateTimeStamp: new Date(),
  };

  if (
    chargePaymentObj.stripePaymentStatus ===
    GLOBALS.CONSTANTS.STRIPE_FAILURE_STATUS_CODE
  ) {
    chargePaymentObj.stripeFailureCode = stripeChargeResponse.failure_code;
    chargePaymentObj.stripeFailureMessage =
      stripeChargeResponse.failure_message;

    // failed so set User status to suspended
    // userStatusName == suspended-payment-failure
    await updateUserStatus(userHandle, 'suspended-payment-failure');
  }

  // create payment collection entry
  const paymentEntryResponse = await insertpaymentline(chargePaymentObj);

  if (paymentEntryResponse.success) {
    // successful payment line created
    chargeResult.success = true;
    chargeResult.message = GLOBALS.STRINGS.CHARGE_SUCCESS_FULL;
    chargeResult.stripeStatus = chargePaymentObj.stripePaymentStatus;
    chargeResult.stripePaid = chargePaymentObj.stripePaymentPaid;
    chargeResult.amountCharged = chargePaymentObj.amount;
  } else {
    chargeResult.success = false;
    chargeResult.message = GLOBALS.STRINGS.CHARGE_SUCCESS_PARTIAL;
    chargeResult.stripeStatus = chargePaymentObj.stripePaymentStatus;
    chargeResult.stripePaid = chargePaymentObj.stripePaymentPaid;
    chargeResult.amountCharged = chargePaymentObj.amount;
  }

  // set glibpennies payment line status
  if (chargePaymentObj.stripePaymentPaid) {
    // success
    // mark penny lines that are in 'pending' as 'paid'
    // add payment invoice id to the penny lines. - this will be used later to tie it all together.

    // set pennies from new to pending
    fPaymentLinesUpdated = await updateGlibPaymentLineStatus(
        userHandle,
        'pending',
        'paid',
        paymentEntryResponse.paymentLineId
    );
    if (!fPaymentLinesUpdated) {
      chargeResult.message =
        GLOBALS.STRINGS.CHARGE_SUCCESS_NO_PENNY_LINE_UPDATE;
    }
  } else {
    // failure
    fPaymentLinesUpdated = await updateGlibPaymentLineStatus(
        userHandle,
        'pending',
        'failed',
        paymentEntryResponse.paymentLineId
    );
    if (!fPaymentLinesUpdated) {
      chargeResult.message =
        GLOBALS.STRINGS.CHARGE_FAILURE_AND_FAILED_TO_UPDATE_PENNY_LINES;
    } else {
      chargeResult.message = GLOBALS.STRINGS.CHARGE_FAILED_TO_CHARGE_STRIPE;
    }
  }
  // add to the audit table
  let userActorId = 1;

  let userActorHandle = 'gonzo';

  // if we have a logged in user
  if (typeof res !== 'undefined' && res.locals.currentUser) {
    userActorId = res.locals.currentUser._id;
    userActorHandle = res.locals.currentUser.handle;
  }

  // add to the audit table
  const auditObj = {
    userIdActor: userActorId,
    handle: userActorHandle,
    action: 'chargeCustomer',
    stripeInvoiceChargeId: stripeChargeResponse.stripeResult.id,
    glibrPaymentInvoiceId: paymentEntryResponse.paymentLineId,
    userIdActedOn: userObj.userId,
    createTimeStamp: new Date(),
    updateTimeStamp: new Date(),
  };
  // insert audit line
  await insertAuditLine(auditObj);
  // return message payload
  return chargeResult;
}

/**
 * getUserPaymentDetails
 *
 * Get user information used during a charge transaction.
 *
 * @param {*} userHandle
 * @returns {object}
 */
async function getUserPaymentDetails(userHandle) {
  let userObj = null;

  try {
    const user = await User.findOne({
      handle: userHandle,
    });

    userObj = {
      // _id:user._id,
      userId: user._id,
      handle: user.handle,
      // Payment Information
      stripeCustomerId: user.stripeCustomerId,
      stripeCardTokenFingerprint: user.stripeCardTokenFingerprint,

      emailAddress: user.emailAddress,
      firstName: user.firstName,
      lastName: user.lastName,
      phone: user.phone,

      // User State
      userStatusID: user.userStatusID,
      userStatusName: user.userStatusName,
      userRoleID: user.userRoleID,
      userRoleName: user.userRoleName,
      // Password
      timezone: user.timezone,
      // charity
      userCharity: user.userCharity,
      userCharityId: user.userCharityId,
    };

    return userObj;
  } catch (err) {
    logger.error(err);
    return null;
  }
}

/**
 * UpdateUserStatus
 *
 * This method will set the user status to a new value
 * Used in the chargeUser method to change state to a suspended state
 * @param {*} userHandle
 * @param {*} newStatus
 * @returns {object}
 */
async function updateUserStatus(userHandle, newStatus) {
  let response = null;

  try {
    response = await User.findOneAndUpdate(
        {
          handle: userHandle,
        },
        {
          $set: {
            userStatusName: newStatus,
          },
        },
        {
          new: true,
          useFindAndModify: false,
        }
    );
  } catch (err) {
    response = err;
  }

  return response;
}

/**
 *  updateGlibPaymentLineStatus
 *
 *  updates the glibpennies lines per the new payment state
 * @param {*} userHandle
 * @param {*} currentState
 * @param {*} newState
 * @param {*} invoiceId
 * @returns {object}
 */
async function updateGlibPaymentLineStatus(
    userHandle,
    currentState,
    newState,
    invoiceId
) {
  let fPaymentLinesUpdated = false;

  try {
    await GlibPenny.updateMany(
        {
          handle: userHandle,
          paymentStatus: currentState,
        },
        {
          paymentStatus: newState,
          invoiceId: invoiceId,
        },
        {
          multi: true,
        }
    );

    fPaymentLinesUpdated = true;
  } catch (err) {
    fPaymentLinesUpdated = false;
  }

  return fPaymentLinesUpdated;
}

/**
 * getPenniesByHandlePaymentState
 *
 * This method will take a handle and a payment state and return all
 * the accrued pennies for that combination.
 * @param {*} userHandle
 * @param {*} paymentState
 * @returns {object}
 */
async function getPenniesByHandlePaymentState(userHandle, paymentState) {
  let resultPennies = 0;

  try {
    resultPennies = await GlibPenny.aggregate([
      {
        $match: {
          handle: userHandle,
          paymentStatus: paymentState,
        },
      },
      {
        $group: {
          _id: '$handle',
          accruedPennies: {
            $sum: '$pennies',
          },
        },
      },
    ]);
  } catch (err) {
    logger.error(err);
    resultPennies = err;
  }

  return resultPennies;
}

/**
 *  createStripeCharge - wrapper method for stripe create charge.
 * @param {*} stripeObj
 * @returns {object}
 */
async function createStripeCharge(stripeObj) {
  let stripeRes = null;

  let stripeChargeSuccess = false;

  try {
    stripeRes = await stripe.charges.create(stripeObj);
    stripeChargeSuccess = true;
  } catch (err) {
    stripeRes = err;
    stripeChargeSuccess = false;
  }

  return {
    success: stripeChargeSuccess,
    stripeResult: stripeRes,
  };
}

/**
 *  insertpaymentline - Internal method
 * @param {*} payObj
 * @returns {object}
 */
async function insertpaymentline(payObj) {
  const paymentObj = Payment({
    ...payObj,
  });

  try {
    const paymentResult = await paymentObj.save();
    return {
      success: true,
      paymentLineId: paymentResult._id,
    };
  } catch (err) {
    return {
      success: false,
      errMessage: err,
    };
  }
}

/**
 *  insertAuditLine - Internal method
 *
 *  May be used for all audit events.
 *
 *  Review the model for the correct set of IDs to pass to it.
 * @param {*} aObj
 * @returns {object}
 */
async function insertAuditLine(aObj) {
  const auditObj = Audit({
    ...aObj,
  });

  try {
    await auditObj.save();

    return {
      success: true,
      auditLineId: auditObj._id,
    };
  } catch (err) {
    return {
      success: false,
      errMessage: err,
    };
  }
}

// ------------------------------------------------------------------------------------
// -----------------------  Test Methods --------------------------------------

paymentRouter.get(
    '/payments/chargeusertest/:handle',
    checkApiKey,
    async (req, res) => {
      const chargeResult = await chargeUser(req.params.handle);

      if (chargeUser2.success) {
        res.status(200).send(chargeResult);
      } else {
        res.status(400).send(chargeResult);
      }
    }
);

paymentRouter.get(
    '/payments/createteststripecharge',
    checkApiKey,
    async (req, res) => {
      const stripeTestObj = {
        amount: 301,
        currency: 'usd',
        customer: 'cus_E2eLxPGaX2QZAH',
        description:
        'Charge for user handle: gregger - name: greg ob - email: greg.obyrne@gmail.com',
        receipt_email: 'greg.obyrne@gmail.com',
        metadata: {
          test: 'test',
        },
      };

      const response = await createStripeCharge(stripeTestObj);

      if (!response) {
        res.status(400).send({
          success: false,
          stripeChargeResult: 'failed to charge',
        });
      } else {
        res.status(200).send({
          success: true,
          stripeChargeResult: response,
        });
      }
    }
);

// test method to create a payment in the payments collection
paymentRouter.get(
    '/payments/insertpaymentline/',
    checkApiKey,
    async (req, res, next) => {
      const chargePayment = {
        userId: '5bfb4036aadc8b2a74c15c92',
        stripeCustomerId: 'AAA1abc23',
        stripeCardTokenFingerprint: 'UCBo6pxPgKd55EXL',
        // psuedo
        stripeInvoiceChargeId: '110011-UCBo6pxPgKd55EXL',
        stripePaymentStatus: 'success',
        stripePaymentPaid: 'paid',
        handle: 'gonzo',
        charityId: '5bcaaf6aec87ccda1bd212da',
        charityName: 'Sierra Club',
        amount: 200,
        chargetSubmit_Timestamp: new Date(),
        createTimeStamp: new Date(),
        updateTimeStamp: new Date(),
      };

      paymentResult = await insertpaymentline(chargePayment);

      res.status(200).send({
        paymentResult,
      });
    }
);

/**
 *  Audit insert TEST method
 */
paymentRouter.get(
    '/payments/createAuditEntry',
    checkApiKey,
    async (req, res, next) => {
      const auditObj = {
        userIdActor: 'AAA12345',
        handle: 'gonzo',
        action: 'chargeCustomer',
        stripeInvoiceChargeId: 'abcde12345',
        glibrPaymentInvoiceId: 'abcde12345',
        userIdActedOn: 'abcde12345',
        createTimeStamp: new Date(),
        updateTimeStamp: new Date(),
      };

      const response = await insertAuditLine(auditObj);

      if (!response) {
        res.status(400).send({
          success: false,
          auditResult: 'failed to insert audit line',
        });
      } else {
        res.status(200).send({
          success: true,
          auditResult: response,
        });
      }
    }
);

/**
 *  updatepaymentstate
 *
 *  test method to update the payment status lines in the glibpennies collection.
 */
paymentRouter.get(
    '/payments/updatepaymentstate/:handle/:currentpaymentstate/:newpaymentstate',
    checkApiKey,
    async (req, res, next) => {
    // api.glibr:5000/payments/updatepaymentstate/gonzo/new/pending/?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      const response = await updateGlibPaymentLineStatus(
          req.params.handle,
          req.params.currentpaymentstate,
          req.params.newpaymentstate,
          null
      );

      if (!response) {
        res.status(400).send({
          success: false,
          paymentStatusChange: GLOBALS.STRINGS.USER_WITH_GIVEN_HANDLE_NOT_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          paymentStatusChange: response,
        });
      }
    }
);

paymentRouter.get(
    '/payments/userpaymentdetail/:handle',
    checkApiKey,
    async (req, res) => {
    // api.glibr:5000/payments/updatepaymentstate/gonzo/new/pending/?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      const response = await getUserPaymentDetails(req.params.handle);

      if (!response) {
        res.status(400).send({
          success: false,
          userPaymentDetails: response,
        });
      } else {
        res.status(200).send({
          success: true,
          userPaymentDetails: response,
        });
      }
    }
);

// getPenniesByHandlePaymentState2
paymentRouter.get(
    '/payments/penniesbyhandlepaymentstate/:handle/:paymentstate',
    checkApiKey,
    async (req, res) => {
    // api.glibr:5000/payments/updatepaymentstate/gonzo/new/pending/?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      const response = await getPenniesByHandlePaymentState(
          req.params.handle,
          req.params.paymentstate
      );

      if (!response) {
        res.status(400).send({
          success: false,
          paymentState: response,
        });
      } else {
        res.status(200).send({
          success: true,
          paymentState: response,
        });
      }
    }
);

// export {paymentRouter};

module.exports = paymentRouter;
