const express = require('express');
const Charities = require('../models/charities');
const User = require('../models/user');


const charityRouter = express.Router();

const GLOBALS = require('../common/globals.js');
const VAULT = require('../common/vault.js');

// Check for a valid Api key
function checkApiKey(req, res, next) {
  if (VAULT.API_KEYS.includes(req.query.apikey) || VAULT.API_KEYS.includes(req.body.apikey)) {
    next();
  } else {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.INVALID_API_KEY,
    });
  }
}

/**
* getcharitieslist
*
*  Return the list of charities in alphabetical order.
*/
charityRouter.get('/charities/getcharitieslist', checkApiKey, (req, res, next) => {
  Charities.find({}).sort({'name': 1}).exec().then((result) => {
    res.status(200).send({success: true, charities: result});
  }).catch((err) => {
    logger.error(err);

    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.GET_CHARITIES_FAULT,
    });
  });
});


/**
* setcharityforuser
*
*  Set Chairty -
* 1. Update user with the new charity
* 2. increase the count of users chosen in the new charity
* 3. decrease the count of users chosen in the previous charity
*/
charityRouter.get('/charities/setcharityforuser/:charity/:handle', checkApiKey, (req, res, next) => {
  // Find the charity in the user and hold on to it to deprecate lower down
  let oldCharity = '';

  // 1. find the current charity that this user has - hold on to it so we can decrement it
  //     protect against nulls or undefined.
  User.findOne({handle: req.params.handle}).exec().then((result) => {
    if (result.userCharity !== undefined && result.userCharity !== null) {
      oldCharity = result.userCharity;
    }

    // 3. Increment the number of times this charity has been selected by users.
    Charities.findOneAndUpdate({name: req.params.charity}, {$inc: {usersChosen: 1}}, {new: true, useFindAndModify: false}, function(err, doc1) {
      if (err) {
        logger.error('Something wrong when trying to add the charity selection to the user: : ' + err);
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.SET_CHARITY_IN_USER_ERROR,
        });
      }

      // 2. Update the current users charity to the new selection.
      User.findOneAndUpdate({handle: req.params.handle}, {$set: {userCharity: req.params.charity, userCharityId: doc1._id, userCharityLogo: doc1.logo}}, {new: true, useFindAndModify: false}, function(err, doc2) {
        if (err) {
          logger.error('Something wrong when trying to add the charity selection to the user: : ' + err);
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.SET_CHARITY_IN_USER_ERROR,
          });
        }

        // 4. Decrement the previous charity selection
        Charities.findOneAndUpdate({name: oldCharity}, {$inc: {usersChosen: -1}}, {new: true, useFindAndModify: false}, function(err, doc3) {
          if (err) {
            // 4.a. but return true because everything else worked.
            logger.error('cound not find decrement charity: ' + err);
            res.status(200).send({success: true, charity: req.params.charity});
          } else {
            res.status(200).send({success: true, charity: req.params.charity});
          }
        });
      });
    });
  }).catch((err) => {
    logger.error('couldn\'t find the old charity: ' + err);
  });
});


module.exports = charityRouter;
