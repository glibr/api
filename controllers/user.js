const express = require('express');
const passport = require('passport');
const multer = require('multer');
const nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('path');
const NodeRSA = require('node-rsa');
const bcrypt = require('bcrypt-nodejs');

const User = require('../models/user');
const Glib = require('../models/glibs');
const UserLinks = require('../models/userlinks');

const Helper = require('../common/helpers');
const Utilities = require('../common/utils');
const GLOBALS = require('../common/globals.js');
const VAULT = require('../common/vault.js');
const logger = require('../common/logger');

const compile = require('es6-template-strings/compile');
const resolveToString = require('es6-template-strings/resolve-to-string');

const stripe = require('stripe')(VAULT.STRIPE_PRIVATE_KEY);

const SITE_BASE_URL = GLOBALS.ENV_VARS.SITE_BASE_URL;

const key = new NodeRSA({
  b: 512,
});

// const SALT_FACTOR = GLOBALS.CONSTANTS.SALT_FACTOR;

const userRouter = express.Router();

// base user loaded
userRouter.use(function(req, res, next) {
  res.locals.currentUser = req.user;
  next();
});

const upload = multer({
  dest: 'uploads/temp/',
});

// email transporter
const transporter = nodemailer.createTransport({
  host: 'smtp.socketlabs.com',
  port: 2525,
  secure: false,
  ignoreTLS: true,
  auth: {
    user: 'server30973',
    pass: 'p5N9GnEr76Twi4JAj8',
  },
});

// Address smtp.socketlabs.com
// Port 25 (if your ISP blocks port 25, try port 2525)
// SMTP Username server30973
// SMTP Password
// p5N9GnEr76Twi4JAj8

// Check for a valid Api key
function checkApiKey(req, res, next) {
  if (
    VAULT.API_KEYS.includes(req.query.apikey) ||
        VAULT.API_KEYS.includes(req.body.apikey)
  ) {
    next();
  } else {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.INVALID_API_KEY,
    });
  }
}

/**
 *  Ensure authenticated
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns {object}
 */
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else if (req.headers.postman=='true') {
    next();
  } else {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.NOT_AUTHORIZED,
    });
  }
}

/* get user id by handle inner method
 *  we want to save the user id in the groupuserlinks collection.
 * get it
 */
async function getUserIdByHandle(userHandle) {
  let userObj = {
    success: false,
  };

  try {
    const user = await User.findOne({
      handle: userHandle,
    });

    userObj = {
      success: true,
      userId: user._id,
      handle: user.handle,
    };
    return userObj;
  } catch (err) {
    logger.error(
        `${GLOBALS.STRINGS.GROUPS_GET_USER_ID_FAIL} - ${userHandle}`,
    );
    logger.error(err);
    userObj = {
      success: false,
      errMessage: GLOBALS.STRINGS.GROUPS_GET_USER_ID_FAIL,
    };
    return userObj;
  }
}

/* get user id by handle inner method
 *  we want to save the user id in the groupuserlinks collection.
 * get it
 */
async function getUserHandleById(userId) {
  try {
    const userHandleObj = await User.findOne({
      _id: userId,
    });

    return {
      success: true,
      userId: userHandleObj._id,
      handle: userHandleObj.handle,
    };
  } catch (err) {
    logger.error(`${GLOBALS.STRINGS.GROUPS_GET_USER_ID_FAIL} - ${userId}`);
    logger.error(err);
    return {
      success: false,
      message: 'unable to get handle by id.',
    };
  }
}

userRouter.get('/getuserhandlebyid/:userId', async (req, res) => {
  const responseHandleObj = await getUserHandleById(req.params.userId);

  if (responseHandleObj.success) {
    res.status(200).send({
      success: true,
      userId: responseHandleObj._id,
      handle: responseHandleObj.handle,
    });
  } else {
    res.status(400).send({
      success: false,
      message: 'unable to get handle by id.',
    });
  }
});

/** check for linked users */
userRouter.get(
    '/users/userlink/:userprimeid/:userfollowid',
    checkApiKey,
    async (req, res) => {
      if (
        req.params.userprimeid === 'undefined' ||
            req.params.userfollowid === 'undefined'
      ) {
        res.status(400).send({
          success: false,
        });
        return;
      }

      const response = await getUserLink(
          req.params.userprimeid,
          req.params.userfollowid,
      );

      if (response.success) {
        res.status(200).send({
          success: true,
          userLink: response.userLink,
        });
        return;
      } else {
        logger.error('breaking: ' + response.success);
        res.status(400).send({
          success: false,
        });
        return;
      }
    },
);

async function getUserLink(primeId, followId) {
  const mQuery = {};
  const andConditions = [];

  andConditions.push({
    primeId: primeId,
  });
  andConditions.push({
    followId: followId,
  });

  mQuery['$and'] = andConditions;

  try {
    const response = await UserLinks.find(mQuery);

    if (response.length > 0) {
      return {
        success: true,
        userLink: response,
      };
    } else {
      logger.error('can\'t get user link: ' + JSON.stringify(mQuery));
      return {
        success: false,
      };
    }
  } catch (err) {
    logger.error('can\'t get user link with error: ' + mQuery + ' ' + err);
    return {
      success: false,
      error: err,
    };
  }
}

// Upload Profile Picture
// upload user profile picture -- api.glibr.com/uploadprofilepicture (127.0.0.1:5000/uploadprofilepicture)
userRouter.post(
    '/uploadprofilepicture',
    ensureAuthenticated,
    upload.single('profilePicture'),
    function(req, res, next) {
      // todo eric: need to delete temp file from /controllers/uploads directory after saving perm version.

      // Get file
      const file = req.file;

      // Get fiile extension

      const fileType = /\.[^.]*$/.exec(req.file.originalname)[0];

      // Check to see if it is not over the IMAGE_UPLOAD_SIZE_LIMIT
      if (file.size > GLOBALS.CONSTANTS.IMAGE_UPLOAD_SIZE_LIMIT) {
        const picSizeMg = Math.round(
            GLOBALS.CONSTANTS.IMAGE_UPLOAD_SIZE_LIMIT / 1000000,
        );
        res.status(400).send({
          success: false,
          message: `${GLOBALS.STRINGS.PROFILE_PICTURE_TOO_LARGE} -  ${picSizeMg} mb`,
        });
      } else if (
        GLOBALS.CONSTANTS.ACCEPTED_IMAGE_FILE_TYPES.indexOf(fileType) === -1
      ) {
        // Check to see if it is an accepted image type
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.UNSUPPORTED_IMAGE_FILE_TYPE,
        });
      } else {
        // All OK - continue with the upload
        fs.readFile(req.file.path, function(err, data) {
          const imageName = req.file.filename;
          // If there's an error
          if (!imageName) {
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.FILE_UPLOAD_ERROR,
            });
          } else {
            // No error - continue to save to filesystem

            // __dirname is a default value for the current dir in nodeJS
            // https://stackoverflow.com/questions/8131344/what-is-the-difference-between-dirname-and-in-node-js#18283508
            // const newPath = __dirname + '/uploads/' + imageName + fileType;
            const newPath =
                        path.resolve('.') + '/uploads/' + imageName + fileType;
            logger.info('file upload path: ' + newPath);
            // write file to uploads/ directory
            fs.writeFile(newPath, data, function(err) {
              if (err) {
                logger.error(err);
                res.status(400).send({
                  success: false,
                  message: GLOBALS.STRINGS.FILE_SAVE_ERROR,
                });

                return;
              }

              // Set Image path in user's profile.
              req.user.profilePicture = imageName + fileType;

              req.user.save(function(err) {
                if (err) {
                  logger.error(err);
                  next(err);
                  return;
                }

                res.status(200).send({
                  success: true,
                  message: GLOBALS.STRINGS.IMAGE_UPLOAD_SUCCESS,
                  imageUrl: imageName + fileType,
                });
                return;
              });
            });
          }
        });
      }
    },
);

/** updateuserprofile
 *   updates the currently authenticated user's details
 *  hooked to the profile page - which auto updates per each field edit meaning we'll get one entity at a time.
 */
userRouter.post('/updateuserprofile', ensureAuthenticated, function(
    req,
    res,
    next,
) {
  // if we have incoming values, clean them, otherwise use existing value

  // req.user.tagline = req.body.tagline ? Helper.cleanText(req.body.tagline) : res.locals.currentUser.tagline;
  req.user.tagline = req.body.tagline
        ? Helper.cleanText(req.body.tagline)
        : res.locals.currentUser.tagline;

  req.user.emailAddress = req.body.emailAddress
        ? Helper.cleanText(req.body.emailAddress)
        : res.locals.currentUser.emailAddress;
  req.user.firstName = req.body.firstName
        ? Helper.cleanText(req.body.firstName)
        : res.locals.currentUser.firstName;
  req.user.lastName = req.body.lastName
        ? Helper.cleanText(req.body.lastName)
        : res.locals.currentUser.lastName;

  if (req.body.phone) {
    if (!Utilities.Validation.validPhone(req.body.phone)) {
      res.status(400).send({
        success: false,
        message: GLOBALS.STRINGS.PROFILE_UPDATE_INVALID_PHONE,
      });
      return;
    }
    req.user.phone = req.body.phone
            ? req.body.phone
            : res.locals.currentUser.phone;
  }

  req.user.address_line1 = req.body.address_line1
        ? Helper.cleanText(req.body.address_line1)
        : res.locals.currentUser.address_line1;
  req.user.address_line2 = req.body.address_line2
        ? Helper.cleanText(req.body.address_line2)
        : res.locals.currentUser.address_line2;
  req.user.city = req.body.city
        ? Helper.cleanText(req.body.city)
        : res.locals.currentUser.city;

  if (req.body.state) {
    if (!Utilities.Validation.validStateLength(req.body.state)) {
      res.status(400).send({
        success: false,
        message: GLOBALS.STRINGS.PROFILE_UPDATE_INVALID_STATE,
      });
      return;
    }
    req.user.state = req.body.state
            ? Helper.cleanText(req.body.state)
            : res.locals.currentUser.state;
  }

  req.user.zipcode = req.body.zipcode
        ? Helper.cleanText(req.body.zipcode)
        : res.locals.currentUser.zipcode;
  req.user.country = req.body.country
        ? Helper.cleanText(req.body.country)
        : res.locals.currentUser.country;

  req.user.updateTimeStamp = new Date();
  req.user.createTimeStamp = res.locals.currentUser.createTimeStamp
        ? res.locals.currentUser.createTimeStamp
        : new Date();

  req.user
      .save()
      .then(function() {})
      .then(function(result) {
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.PROFILE_UPDATE_SUCCESS,
        });
      })
      .catch(function(err) {
        logger.error(err);
        next(err || 'Something bad happened');
        return;
      });
});


/* -------------------------------------------------------------------------------------
* resetPassword
*
* This method is tied directly to the reset password form
* for when the user is choosing to do it themselves / not for the email
* forgot password process
----------------------------------------------------------------------------------------*/
userRouter.post('/resetpassword', async (req, res) => {
  User.findOne(
      {
        _id: req.body.token,
      },
      async function(err, user) {
        if (err) {
          logger.error(
              'failed to find token in reset password: ' +
                        req.body.token +
                        '  - ' +
                        err,
          );
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.SERVER_ISSUES,
          });
          return;
        }

        if (!user) {
          logger.warn(
              'reset password: couldn\'t find user: ' +
                        req.body.token +
                        ' - ' +
                        req.body.newPassword,
          );

          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.USER_DOES_NOT_EXIST,
          });

          return;
        }

        // check old password against entered old password
        const passwordIsValid = bcrypt.compareSync(req.body.oldPassword, user.password);
        if (!passwordIsValid) {
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.RESET_PW_ERROR_INCORRECT_PW,
          });
          return;
        }

        user.password = req.body.newPassword;
        user.createTimeStamp = user.createTimeStamp || new Date();
        user.updateTimeStamp = new Date();
        user.save(function(err) {
          if (err) {
            logger.error('trouble saving password update: ' + err);
            logger.error('token: ' + req.body.token);
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.RESET_PW_ERROR,
            });
            return;
          }

          res.status(200).send({
            success: true,
            message: GLOBALS.STRINGS.RESET_PW_SUCCESS,
          });
        });
      },
  );
});

/* -------------------------------------------------------------------------------------
* forgotpasswordreset
*
* This method is r

ed via the forgot password flow.
* meaning there is a datestamp to comppare as part of
* the process.
----------------------------------------------------------------------------------------*/
userRouter.post('/forgotpasswordreset', function(req, res) {
  User.findOne(
      {
        _id: req.body.token,
      },
      function(err, user) {
        if (err) {
          logger.error(
              'bonk in forgot password reset: ' +
                        req.body.token +
                        '  - ' +
                        err,
          );
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.SERVER_ISSUES,
          });
          return;
        }

        if (!user) {
          logger.warn('reset password: couldn\'t find user');
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.USER_DOES_NOT_EXIST,
          });
          return;
        }

        // additonal check for forgot password workflow.
        if (new Date() > user.resetExpiration) {
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.RESET_PW_LINK_EXPIRED,
          });
        } else {
          user.password = req.body.password;
          user.createTimeStamp = user.createTimeStamp || new Date();
          user.updateTimeStamp = new Date();
          user.save(function(err) {
            if (err) {
              logger.error('trouble saving password update: ' + err);
              res.status(400).send({
                success: false,
                message: GLOBALS.STRINGS.RESET_PW_ERROR,
              });
              return;
            }
            res.status(200).send({
              success: true,
              message: GLOBALS.STRINGS.RESET_PW_SUCCESS,
            });
          });
        }
      },
  );
});

/** forgotpassword
 * kick off forgot password flow.
 */
userRouter.post('/forgotpassword', function(req, res) {
  const emailAddress = req.body.emailAddress;

  User.findOne(
      {
        emailAddress: emailAddress,
      },
      function(err, user) {
        const resetExpiration = new Date();
        resetExpiration.setDate(resetExpiration.getDate() + 3); // 72 hours in the future

        if (err) {
          logger.error(err);
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.SERVER_ISSUES,
          });
          return;
        }
        if (!user) {
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.USER_DOES_NOT_EXIST,
          });
          return;
        }
        user.resetExpiration = resetExpiration;

        user.save(function(err) {
          if (err) {
            logger.error(err);
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.SERVER_ISSUES,
            });
            return;
          } else {
            const emailRaw = fs.readFileSync(
                './common/resources/glibrResetEmailTemplate.txt',
                'utf8',
            );
            const emailCompiled = compile(emailRaw);
            const emailToSendString = resolveToString(emailCompiled, {
              SITE_BASE_URL: SITE_BASE_URL,
              USER_ID: user._id,
            });

            logger.info(emailToSendString);

            // send pw reset mail
            const mailOptions = {
              from: '"The Glibr Team" <support@glibr.com>',
              to: emailAddress,
              subject: 'Reset Password Request',
              text:
                            'Please click the link below to reset your password. The below link will expire in 72 hours.',
              html: emailToSendString,
            };

            transporter.sendMail(mailOptions, function(err, response) {
              if (err) {
                logger.error(err);
                res.status(400).send({
                  success: false,
                  message: GLOBALS.STRINGS.RESET_PW_EMAIL_ERROR,
                });
                return;
              }
              res.status(200).send({
                success: true,
                message: GLOBALS.STRINGS.RESET_PW_EMAIL_SUCCESS,
              });
              return;
            });
          }
        });
      },
  );
});

/** -----------------------------------------------------------------------------------
 *  Registration step one - basic
 *
 *  First step - handle / email / phone
 * set userStatusName to - registration-two-charity
 -----------------------------------------------------------------------------------*/
userRouter.post('/register', function(req, res) {
  if (!Utilities.Validation.validEmail(req.body.emailAddress)) {
    logger.error('email ' + req.body.emailAddress);
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.ACCOUNT_ACTIVATION_EMAIL_BAD_EMAIL,
    });
    return;
  }

  const emailAddress = Helper.cleanText(req.body.emailAddress);

  if (
    !Utilities.Validation.validHandle(req.body.handle) ||
        !Utilities.Validation.validHandleLength(req.body.handle)
  ) {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.ACCOUNT_ACTIVATION_BAD_HANDLE,
    });
    return;
  }

  const handle = Helper.cleanText(req.body.handle);

  if (Utilities.Validation.weakPassword(req.body.password)) {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.ACCOUNT_ACTIVATION_WEAK_PASSWORD,
    });
    return;
  }
  const password = req.body.password;

  // this is needed because mongo does not check with case insensitivity
  // and this will keep handles in better order.

  const handleLower = req.body.handle.toString().toLowerCase();

  if (!emailAddress || !handle || !password) {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.MISSING_FIELDS_DURING_REGISTRATION,
    });
  } else {
    User.findOne(
        {
          $or: [
            {
              emailAddress: emailAddress,
            },
            {
              handleLower: handleLower,
            },
          ],
        },
        function(err, user) {
          if (err) {
            logger.error(err);
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.ACCOUNT_ACTIVATION_FAILED,
            });
            return;
          }
          if (user) {
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.USER_ALREADY_EXISTS,
            });
            return;
          }

          const data = {
            emailAddress: emailAddress,
            handle: handle,
            handleLower: handleLower,
            password: password,
            // phone: req.body.phone
          };

          // Change - no longer taking in first name and last name
          // in page one of registration.
          data.phone = null;

          data.firstName = null;
          data.lastName = null;

          data.passwordActivationToken = null;
          data.follows = [];
          data.ddress_line1 = null;
          data.address_line2 = null;
          data.city = null;
          data.state = null;
          data.zipcode = null;
          data.country = null;
          data.tagline = 'glibr is the goodest social network!';
          data.profilePicture = null;
          data.timezone = null;

          // Registration states
          // registration_one_basic - first step complete
          data.userStatusName = 'registration-two-charity';
          data.userStatusID = 221;

          // Stripe Customer Id
          data.stripeCustomerId = null;

          // user role
          data.userRoleID = 101;
          data.userRoleName = 'user';

          data.failedLoginCount = 0.0;
          data.resetExpiration = new Date();
          data.pwHistory = null;

          // Follows
          data.followCount = 0;
          data.followerCount = 0;
          data.doubleFollowCount = 0;

          data.userCharity = '';

          data.createTimeStamp = new Date();
          data.updateTimeStamp = new Date();

          const newUser = new User(data);

          newUser.save(function(err) {
            if (err) {
              logger.error(err);

              if (err.code === 11000) {
                // duplicate may be one of three fields - handle, email, phone
                let errResponse = err.message;
                const regPattern = /(?:index:\s)(\w*)[\^_]/;
                const dupField = errResponse.match(regPattern);
                try {
                  errResponse =
                                    GLOBALS.STRINGS.FIELD_ALREADY_TAKEN +
                                    ' --> ' +
                                    dupField[1] +
                                    ' <--';
                } catch (e) {
                  errResponse =
                                    GLOBALS.STRINGS.FIELD_ALREADY_TAKEN;
                }

                res.status(400).send({
                  success: false,
                  message: errResponse,
                });
                return;
              } else {
                res.status(400).send({
                  success: false,
                  message: 'failed to insert user' + err.message,
                });
              }
              return;
            }

            res.status(200).send({
              success: true,
              userRole: newUser.userRoleName,
              userStatus: newUser.userStatusName,
              handle: newUser.handle,
              message:
                            GLOBALS.STRINGS
                                .ACCOUNT_REGISTRATION_STEP_ONE_COMPLETE,
            });
            return;
          });
        },
    );
  }
});

/** -----------------------------------------------------------------------------------
 *  Registration step 2 - Charity chosen
 *  registercharity
 *  set status to registration-three-payment
 *  set userCharity
 *
----------------------------------------------------------------------------------------*/
userRouter.post('/registercharity', function(req, res, next) {
  User.findOneAndUpdate(
      {
        handle: req.body.handle,
      },
      {
        $set: {
          userStatusName: 'registration-three-payment',
          userStatusID: 222,
        },
      },
      {
        new: true,
        useFindAndModify: false,
      },
      function(err, doc) {
        // https://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate

        if (err) {
          logger.error(
              GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR + ' - ' + err,
          );
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR,
          });
          return;
        }

        res.status(200).send({
          success: true,
          userStatusName: doc.userStatusName,
          userStatusID: doc.userStatusID,
          message: GLOBALS.STRINGS.ACCOUNT_REGISTRATION_STEP_TWO_COMPLETE,
        });
        return;
      },
  );
});

/**
 *  Registration step 3a -  complete
 *  registerpaymentset
 */
userRouter.post('/register/complete', async (req, res) => {
  // before we get here, we have already confirmed UNIQUE stripe card fingerprint
  // this is done in stripeUserSigneUp component

  const firstName = Helper.cleanText(req.body.firstName);
  const lastName = Helper.cleanText(req.body.lastName);
  const activationToken = encodeURIComponent(
      key.encrypt(req.body.emailAddress, 'base64'),
  );

  const userData = {
    firstName: firstName,
    lastName: lastName,
    emailAddress: req.body.emailAddress,
    stripeCardToken: req.body.stripeCardToken, // obtained with Stripe.js
  };

  const responseStripe = await createStripeCustomer(userData);

  if (!responseStripe.success) {
    res.status(400).send({
      success: false,
      message: `${GLOBALS.STRINGS.ACCOUNT_STRIPE_REGISTRATION_FAILED} - ${req.body.emailAddress}}`,
    });
    return;
  }

  logger.info(
      'update user with stripe token fingerprint: ' +
            req.body.stripeCardTokenFingerprint,
  );

  // let responseUpdateUser = 'empty';
  const updateUserData = {
    handle: req.body.handle,
    activationToken: activationToken,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    stripeCustomerId: responseStripe.stripeCustomerId,
    stripeCardTokenFingerprint: req.body.stripeCardTokenFingerprint,
  };

  responseUpdateUser = await updateUserFull(updateUserData);

  await sendMail(req.body.emailAddress, activationToken);

  res.status(200).send({
    success: true,
    message: `${GLOBALS.STRINGS.ACCOUNT_STRIPE_REGISTRATION_SUCCEEDED} - ${req.body.emailAddress}}`,
  });
});

/** Registration step 3b -  register provisional
 * register a provisional user that does not sign up with credit card
 */
userRouter.post('/register/provisional', async (req, res) => {
  // before we get here, we have already confirmed UNIQUE stripe card fingerprint
  // this is done in stripeUserSigneUp component

  const firstName = Helper.cleanText(req.body.firstName);
  const lastName = Helper.cleanText(req.body.lastName);

  const activationToken = encodeURIComponent(
      key.encrypt(req.body.emailAddress, 'base64'),
  );

  let responseUpdateUser = 'empty';
  const updateUserData = {
    handle: req.body.handle,
    activationToken: activationToken,
    firstName: firstName,
    lastName: lastName,
  };

  responseUpdateUser = await updateUserProvisional(updateUserData);

  logger.info(
      'we think we\'ve updated a user now...' +
            JSON.stringify(responseUpdateUser),
  );

  await sendMail(req.body.emailAddress, activationToken);

  res.status(200).send({
    success: true,
    message: `${GLOBALS.STRINGS.ACCOUNT_STRIPE_REGISTRATION_SUCCEEDED} - ${req.body.emailAddress}}`,
  });
});

/** Registration - update user from  provisional to active
 * update the user from provisional-expired to active.
 * requires a stripe token
 */
userRouter.post(
    '/register/updateprovisionaltoactive',
    ensureAuthenticated,
    async (req, res) => {
      // before we get here, we have already confirmed UNIQUE stripe card fingerprint
      // this is done in stripeUserSigneUp component

      // exit if not correct user status
      if (res.locals.currentUser.userStatusName !== 'provisional-expired') {
        res.status(400).send({
          success: false,
          message: `${GLOBALS.STRINGS.ACCOUNT_STRIPE_REGISTRATION_FAILED} - ${req.body.emailAddress}}`,
        });
        return;
      }

      const userData = {
        firstName: res.locals.currentUser.firstName,
        lastName: res.locals.currentUser.lastName,
        emailAddress: res.locals.currentUser.emailAddress,
        stripeCardToken: req.body.stripeCardToken,
      };

      const responseStripe = await createStripeCustomer(userData);

      if (!responseStripe.success) {
        res.status(400).send({
          success: false,
          message: `${GLOBALS.STRINGS.ACCOUNT_STRIPE_REGISTRATION_FAILED} - ${req.body.emailAddress}}`,
        });
        return;
      }

      logger.info(
          `Stripe Fingerprint: ${req.body.stripeCardTokenFingerprint} - stripe customer id: ${responseStripe.stripeCustomerId} for handle ${req.body.handle}`,
      );

      const updateUserData = {
        handle: req.body.handle,
        userStatusName: 'active',
        userStatusID: '201',
        stripeCustomerId: responseStripe.stripeCustomerId,
        stripeCardTokenFingerprint: req.body.stripeCardTokenFingerprint,
      };

      const responseUpdateUserStatus = await updateProvisionalToActive(
          updateUserData,
      );

      if (responseUpdateUserStatus.success) {
        res.status(200).send({
          success: true,
          message: `${GLOBALS.STRINGS.ACCOUNT_STRIPE_REGISTRATION_SUCCEEDED} - ${req.body.emailAddress}}`,
        });
      } else {
        res.status(400).send({
          success: false,
          message: `${GLOBALS.STRINGS.ACCOUNT_STRIPE_REGISTRATION_FAILED} - ${req.body.emailAddress}}`,
        });
      }
    },
);

/**
 *  Activate Account
 *
 *  When the user responds to the email we set the userStatusName and ID
 *  To active.
 *
 */
userRouter.get('/register/activate/:token', function(req, res) {
  // decrypt key and find user

  try {
    User.findOne(
        {
          emailAddress: key.decrypt(req.params.token, 'utf8'),
        },
        function(err, user) {
          if (err) {
            logger.error(err);
            res.status(400).send({
              success: false,
              message: GLOBALS.STRINGS.ACCOUNT_ACTIVATION_FAILED,
            });
            return;
          }
          // check to see if we have a user.
          if (!user) {
            logger.error(
                'Account activation error: User does not exist.',
            );

            res.status(400).send({
              success: false,
              message:
                            GLOBALS.STRINGS.ACCOUNT_ACTIVATION_FAILED_NO_USER,
            });
            return;
          } else {
            // found user then activate

            // check if we are only a provisional sign up and change status appropriately.
            if (
              user.userStatusName ===
                        'registered-provisional-inactive'
            ) {
              user.userStatusID = 202;
              user.userStatusName = 'active-provisional';
            } else {
              user.userStatusID = 201;
              user.userStatusName = 'active';
            }

            // try to save user
            user.save(function(err) {
              if (err) {
                logger.error(err);
                logger.error(
                    'Account activation error: Saving user changes to DB failed.',
                );

                res.status(400).send({
                  success: false,
                  message:
                                    GLOBALS.STRINGS
                                        .ACCOUNT_ACTIVATION_FAILED_NO_SAVE,
                });
                return;

                return;
                // Save user successful - return good status
              } else {
                res.status(200).send({
                  success: true,
                  message:
                                    GLOBALS.STRINGS
                                        .ACCOUNT_ACTIVATION_FAILED_NO_SAVE,
                });
                return;
              }
            });
          }
        },
    );
  } catch (err) {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.ACCOUNT_ACTIVATION_FAILED_NO_USER,
    });
    return;
  }
});

/**
 * updateUserFull
 * @returns {boolean}
 * @param {*} updateUserData
 */
async function updateUserFull(updateUserData) {
  logger.info('in update user with fingerprint' + updateUserData.handle);
  try {
    const responseUpdateUser = await User.findOneAndUpdate(
        {
          handle: updateUserData.handle,
        },
        {
          $set: {
            userStatusName: 'registered-inactive',
            userStatusID: 223,
            passwordActivationToken: updateUserData.activationToken,
            firstName: updateUserData.firstName,
            lastName: updateUserData.lastName,
            stripeCustomerId: updateUserData.stripeCustomerId,
            stripeCardTokenFingerprint:
                        updateUserData.stripeCardTokenFingerprint,
          },
        },
        {
          new: true,
          useFindAndModify: false,
        },
    ); // .exec();

    if (responseUpdateUser === null) {
      logger.error(
          `no user updated: ${GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR}`,
      );
      return {
        success: false,
        message: GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR,
      };
    } else {
      logger.info('updated');
      return {
        success: true,
        user: responseUpdateUser,
      };
    }
  } catch (err) {
    logger.error(`${GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR}  - ${err}`);
    return {
      success: false,
      message: `${GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR} - ${err}`,
    };
  }
}

/**
 * updateUserProvisional
 * @param {*} updateUserData
 * @returns {boolean}
 */
async function updateUserProvisional(updateUserData) {
  logger.info('in update user with fingerprint' + updateUserData.handle);
  try {
    const responseUpdateUser = await User.findOneAndUpdate(
        {
          handle: updateUserData.handle,
        },
        {
          $set: {
            userStatusName: 'registered-provisional-inactive',
            userStatusID: 231,
            passwordActivationToken: updateUserData.activationToken,
            firstName: updateUserData.firstName,
            lastName: updateUserData.lastName,
          },
        },
        {
          new: true,
          useFindAndModify: false,
        },
    ); // .exec();

    if (responseUpdateUser === null) {
      logger.error(
          `no user updated: ${GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR}`,
      );
      return {
        success: false,
        message: GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR,
      };
    } else {
      logger.info('updated');
      return {
        success: true,
        user: responseUpdateUser,
      };
    }
  } catch (err) {
    logger.error(`${GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR}  - ${err}`);
    return {
      success: false,
      message: `${GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR} - ${err}`,
    };
  }
}

/**
 * updateProvisionalToActive
 * @param {*} updateUserData
 * @returns {boolean}
 */
async function updateProvisionalToActive(updateUserData) {
  logger.info(`in update user status name: ${updateUserData.handle}`);
  try {
    const responseUpdateUser = await User.findOneAndUpdate(
        {
          handle: updateUserData.handle,
        },
        {
          $set: {
            userStatusName: updateUserData.userStatusName,
            userStatusID: updateUserData.userStatusID,
            stripeCustomerId: updateUserData.stripeCustomerId,
            stripeCardTokenFingerprint:
                        updateUserData.stripeCardTokenFingerprint,
          },
        },
        {
          new: true,
          useFindAndModify: false,
        },
    ); // .exec();

    if (responseUpdateUser === null) {
      logger.error(
          `no user updated: ${GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR}`,
      );
      return {
        success: false,
        message: GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR,
      };
    } else {
      logger.info('updated');
      return {
        success: true,
        user: responseUpdateUser,
      };
    }
  } catch (err) {
    logger.error(`${GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR}  - ${err}`);
    return {
      success: false,
      message: `${GLOBALS.STRINGS.ACCOUNT_REGISTRATION_ERROR} - ${err}`,
    };
  }
}

/**
 * createStripeCustomer
 * @param {*} userData
 * @returns {boolean}
 */
async function createStripeCustomer(userData) {
  try {
    responseStripe = await stripe.customers.create({
      description:
                'Customer: ' +
                userData.firstName +
                ' ' +
                userData.lastName +
                ', ' +
                userData.emailAddress,
      email: userData.emailAddress,
      source: userData.stripeCardToken, // obtained with Stripe.js
    });

    if (responseStripe.id === null || responseStripe.id === undefined) {
      logger.error(
          GLOBALS.STRINGS.ACCOUNT_STRIPE_NO_CUSTOMER_ID_TOKEN_RETURNED +
                    ' - 2 - Customer ID was returned null or undefined.',
      );

      return {
        success: false,
        message:
                    GLOBALS.STRINGS
                        .ACCOUNT_STRIPE_NO_CUSTOMER_ID_TOKEN_RETURNED + ' - 2',
      };
    } else {
      logger.info(
          `${GLOBALS.STRINGS.ACCOUNT_STRIPE_REGISTRATION_SUCCEEDED} - ${userData.emailAddress}`,
      );

      return {
        success: true,
        stripeCustomerId: responseStripe.id,
        message: GLOBALS.STRINGS.ACCOUNT_STRIPE_REGISTRATION_SUCCEEDED,
      };
    }
  } catch (err) {
    logger.error(
        `failed: ${GLOBALS.STRINGS.ACCOUNT_STRIPE_REGISTRATION_FAILED}`,
    );
    logger.error(`error: ${err}`);
    return {
      success: false,
      message: GLOBALS.STRINGS.ACCOUNT_STRIPE_REGISTRATION_FAILED,
    };
  }
}

/**
 * sendMail
 * @param {*} emailAddress
 * @param {*} activationToken
 * @returns {boolean}
 */
async function sendMail(emailAddress, activationToken) {
  try {
    const emailRaw = fs.readFileSync(
        './common/resources/glibrRegisterEmailTemplate.txt',
        'utf8',
    );
    const emailCompiled = compile(emailRaw);
    const emailToSendString = resolveToString(emailCompiled, {
      SITE_BASE_URL: SITE_BASE_URL,
      ACTIVATION_TOKEN: activationToken,
    });

    logger.info(emailToSendString);

    const mailOptions = {
      from: '"The Glibr Team" <support@glibr.com>',
      to: emailAddress,
      subject: 'Welcome to Glibr!',
      text:
                'Thanks for joining Glibr! Please click the link below to activate your account.',

      html: emailToSendString,
    };

    // `<html><head> <link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet" /></head><body> <table align="center" width="80%"> <tr> <td align="center"> Welcome to <span style="font-family:Righteous; font-weight:bold; font-size: 130%; color: #399552;">glibr</span> <hr style="border: 0; height: 2px;color: #399552;background-color: #399552;border-color: #399552;" /> To activate your account please click on the link below.<br /><a href="${SITE_BASE_URL}/register?activate=true&token=${activationToken}">activation link</a> <hr style="border: 0; height: 2px;color: #399552;background-color: #399552;border-color: #399552;" /> or copy and paste this URL into your browser address bar:<br /> <span style="font-size: 70% ">${SITE_BASE_URL}/register?activate=true&token=${activationToken}</span> <hr style="border: 0; height: 2px;color: #399552;background-color: #399552;border-color: #399552;" /> thanks...keep glib! </td> </tr> </table></body></html>`

    logger.info('email address: ' + mailOptions.to);
    logger.info(`mail sent: ${mailOptions.html}`);

    transporter.sendMail(mailOptions, function(err, response) {
      if (err) {
        logger.error(`fail to send email for user: ${emailAddress}`);
        logger.error(`error: ${err}`);

        return {
          success: false,
          message: `${GLOBALS.STRINGS.ACCOUNT_ACTIVATION_EMAIL_ERROR} - for user email: ${emailAddress}`,
          error: err,
        };
      } else {
        return {
          success: true,
          message: `${GLOBALS.STRINGS.ACCOUNT_ACTIVATION_EMAIL_SUCCESS} - for user email: ${emailAddress}`,
        };
      }
    });
  } catch (err) {
    logger.error(`fail to send email for user: ${emailAddress}`);
    //  logger.error(`error: ${err}`);
    return {
      success: false,
      message: `${GLOBALS.STRINGS.ACCOUNT_ACTIVATION_EMAIL_ERROR} - for user email: ${emailAddress}`,
      error: err,
    };
  }
}

/** -
  * login
  *
  * review NoSQL injection issues
  //https://scotch.io/@401/mongodb-injection-in-nodejs
  */
userRouter.post('/login', function(req, res) {
  // call passport for login

  console.log('damn: ' + JSON.stringify(req.body));

  passport.authenticate('login', function(err, user, msg) {
    if (err || !user) {
      res.status(400).send({
        success: false,
        userRole: null,
        userStatus: null,
        message: GLOBALS.STRINGS.INCORRECT_CREDENTIALS + err,
      });
      return;
    } else {
      req.login(user, (loginErr) => {
        if (loginErr) {
          res.status(400).send({
            success: false,
            userRole: null,
            userStatus: null,
            message:
                            GLOBALS.STRINGS.ACCOUNT_AUTHENTICATION_ERROR + msg,
          });
          return;
        } else {
          // we have a successful login :)
          res.status(200).send({
            success: true,
            userRole: user.userRoleName,
            userStatus: user.userStatusName,
            message: GLOBALS.STRINGS.ACCOUNT_AUTHENTICATION_SUCCESS,
          });
          return;
        }
      });
    }
  })(req, res);
});

/**
 *  logout
 *
 *  yup logout.
 */
userRouter.get('/logout', function(req, res) {
  req.logout();
  res.status(200).send({
    success: true,
    message: GLOBALS.STRINGS.ACCOUNT_DEAUTHENTICATION_SUCCESS,
  });
  return;
});

/**
 *  *  Get Current User
 *
 *  Main method to get a current user.
 */
userRouter.get('/getcurrentuser', ensureAuthenticated, async (req, res) => {
  // Override base user with  getUserByHandleVerbose

  try {
    const responseUserObj = await getUserByHandleVerbose(
        res.locals.currentUser.handle,
    );

    if (responseUserObj.success) {
      res.status(200).send({
        success: true,
        currentUser: responseUserObj.user,
        userFollows: responseUserObj.user.follows,
      });
      return;
    } else {
      res.status(400).send({
        success: false,
        message: GLOBALS.STRINGS.USER_WITH_GIVEN_HANDLE_NOT_FOUND,
      });
      return;
    }
  } catch (err) {
    res.status(400).send({
      success: false,
      message: `${GLOBALS.STRINGS.USER_SIGN_IN_CURRENT_USER_FAIL} - ${err}`,
    });
    return;
  }
});

/**
 *  get user
 */
userRouter.get('/getuser/:username', ensureAuthenticated, function(req, res) {
  User.findOne(
      {
        emailAddress: req.params.username,
      },
      function(err, user) {
        if (err) {
          logger.error(err);
          res.status(400).send({
            success: false,
            message: err,
          });
          return;
        }
        if (!user) {
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.ACCOUNT_NO_EMAIL_ADDRESS_FOUND,
          });
        }
        res.status(200).send({
          success: true,
          user: user,
        });
        return;
      },
  );
});

/**
 *  getuserbyhandlecompact
 *  shortened list of user attributes: email | handle | firstName | lastName | profilePicture
 */
userRouter.get('/getusersbyhandlecompact/:handle', async (req, res) => {
  const response = await getUserByHandleCompact(req.params.handle);

  if (!response.success) {
    res.status(400).send({
      success: false,
      message: response.message,
    });
    return;
  } else {
    res.status(200).send({
      success: true,
      user: userObj,
    });
    return;
  }
});


/**
 * getUserByHandleCompact
 * @param {*} handle
 * @returns {userObject}
 */
async function getUserByHandleCompact(handle) {
  try {
    const response = await User.findOne({
      handle: handle,
    });

    if (response !== null) {
      const userObj = {
        emailAddress: response.emailAddress,
        handle: response.handle,
        firstName: response.firstName,
        lastName: response.lastName,
        profilePicture: response.profilePicture,
        userStatusName: response.userStatusName,
      };

      return {
        success: true,
        user: userObj,
        message: `found our user - ${handle}`,
      };
    } else {
      return {
        success: false,
        message: `we did not find the user - ${handle}`,
      };
    }
  } catch (err) {
    return {
      success: false,
      message: `we did not find the user - ${handle}`,
    };
  }
}


// get another glibr user's details by their handle
userRouter.get('/getuserbyhandle/:handle', function(req, res) {
  User.findOne({
    handle: req.params.handle,
  })
      .exec()
      .then((user) => {
        if (!user) {
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.USER_WITH_GIVEN_HANDLE_NOT_FOUND,
          });
        } else {
          const userObj = {
            emailAddress: user.emailAddress,
            handle: user.handle,
            firstName: user.firstName,
            lastName: user.lastName,
            profilePicture: user.profilePicture,
            followCount: user.followCount,
            followerCount: user.followerCount,
            doubleCount: user.doubleFollowCount,
            userStatusName: user.userStatusName,
          };

          Glib.count({
            cleanHandle: user.handle,
          })
              .exec()
              .then((result2) => {
                userObj.numberGlibs = result2;
                res.status(200).send({
                  success: true,
                  user: userObj,
                });
                return;
              });
        }
      })
      .catch((err) => {
        logger.error(err);
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.SERVER_ISSUES,
        });
        return;
      });
});

/**
 *  getuserbyhandleverbose
 *
 *  Primary method for returning a user.  Full list of user attributes
 *
 */
userRouter.get('/getuserbyhandleverbose/:handle', async (req, res) => {
  //   //direct call:
  //   //http://api.glibr:5000/getuserbyhandleverbose/gonzo?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

  const responseUserObj = await getUserByHandleVerbose(req.params.handle);

  // logger.info('get user by handle verbose: prior to response: ' + responseUserObj.user);

  if (responseUserObj.success) {
    res.status(200).send({
      success: true,
      user: responseUserObj.user,
    });
    return;
  } else {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.USER_WITH_GIVEN_HANDLE_NOT_FOUND,
    });
    return;
  }
});

/**
 *  getUserByHandleVerbose
 *
 * Full set (or nearely so) of user attributes
 *  Internal Method for the API to consume
 *  This will allow its re-use on the server.
 * @param {*} userHandle
 * @param {*} callback
 * @returns {object}
 */
async function getUserByHandleVerbose(userHandle) {
  if (Helper.isEmptyOrWhitespace(userHandle)) {
    return {
      success: false,
    };
  }

  logger.info('get user by handle verbose: incoming handle: ' + userHandle);

  let userObj = null;

  try {
    userObj = await User.findOne({
      handle: userHandle,
    });

    const responseGlibCount = await getGlibCountByHandle(userHandle);

    logger.info(
        `${responseGlibCount.success} - and count - ${responseGlibCount.count}`,
    );

    if (responseGlibCount.success) {
      userObj.numberOfGlibs = responseGlibCount.count;
    } else {
      userObj.numberOfGlibs = 0;
    }

    userObj.isAuthenticated = true;

    userObj.userId = userObj._id;

    return {
      success: true,
      user: userObj,
    };
  } catch (err) {
    logger.error(GLOBALS.STRINGS.GROUPS_GET_USER_ID_FAIL);
    logger.error(err);
    userObj = {
      success: false,
      errMessage: GLOBALS.STRINGS.GROUPS_GET_USER_ID_FAIL,
    };
    return userObj;
  }
}

/**  getusersbyidlist
 * return the list of users by the passed in idlist
 * */
userRouter.get('/getusersbyidlist/:idlist', async (req, res) => {
  const userListObj = await getUsersByArray(req.params.idlist);

  if (userListObj.success) {
    res.status(200).send({
      success: userListObj.success,
      userList: userListObj.userList,
    });
    return;
  } else {
    res.status(400).send({
      success: userListObj.success,
      message: userListObj.message,
    });
    return;
  }
});

async function getUsersByArray(userIdArray) {
  const idListArray = userIdArray.split(',');

  try {
    const userListObj = await User.find({
      _id: {
        $in: idListArray,
      },
    })
    // select clause defines the fields to return from the base get user call (which is everything)
        .select({
          handle: 1,
          firstName: 2,
          lastName: 3,
          emailAddress: 4,
          profilePicture: 5,
          follows: 6,
          tagline: 7,
          numberOfGlibs: 8,
          followCount: 12,
          followerCount: 13,
          doubleFollowCount: 14,
        });

    return {
      success: true,
      userList: userListObj,
    };
  } catch (err) {
    logger.error(GLOBALS.STRINGS.USERLIST_FAILED_TO_GET_USERLIST);
    logger.error(err);
    return {
      success: false,
      message: GLOBALS.STRINGS.USERLIST_FAILED_TO_GET_USERLIST,
    };
  }
}

/**
 *  getusersbyhandlelist
 *
 * -- DEPRECATED IN FAVOR OF getUsersByIdList SEE ABOVE --
 *
 *   Get the a list of users from a list of handles
 *
 *  Input an array of handles.
 *  Return user objects that are the same as the getuserbyhandle
 *
 *  */
userRouter.get('/getusersbyhandlelist/:handleList', function(req, res, next) {
  // direct call:
  // http://api.glibr:5000/getusersbyhandlelist/['test','test2','glibrsmith','gonzo']?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

  // Check for null handleList
  if (typeof req.params.handleList !== 'undefined' && req.params.handleList) {
    // careful - have to replace single ticks with double quotes to be able to JSON.parse (which is stupid)
    const hList = JSON.parse(req.params.handleList.replace(/'/g, '"'));
    // and we have to JSON.parse to be able to pass in handleList to Mongoose and have it understand it.
    // otherwise it is the wrong type

    // the $in action tells Mongoose to return items from a list.
    User.find({
      handle: {
        $in: hList,
      },
    })
    // select clause defines the fields to return from the base get user call (which is everything)
        .select({
          handle: 1,
          firstName: 2,
          lastName: 3,
          emailAddress: 4,
          profilePicture: 5,
          follows: 6,
          tagline: 7,
          numberOfGlibs: 8,
          followCount: 12,
          followerCount: 13,
          doubleFollowCount: 14,
        })
        .exec(function(err, userList) {
          if (err) {
            logger.error(err);
            return next(err);
          }
          res.status(200).send({
            success: true,
            userList: userList,
          });
          return;
        });
  } else {
    // handleLIst is undefined / null return failure
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.SERVER_ISSUES,
    });
    return;
  }
});

// --------------------------------------------------
// USER LINKS
// ---------------------------------------------------

// data model of the userlinks collection
// primeHandle: { type: String, required: true },
// followHandle: { type: String },
// doubleHandle: { type: String },

/** ------------------------------------------------------------------------------------------
  // Link users
  //
   * This is the route that will be coming from the FollowButton on the UI
  // Enhanced Follow a user
  // We will do three operations to the user.
  // 1. add the follow entity to the current user.
  // 2. add the follower entity to the opposite user
  // 3. check for match between new follow and existing follows and add to joined list
  // 4. and the reverse for the opposite user - does the new follower exist in the follows then add it to the joined list.
  -------------------------------------------------------------------------------------------*/
// link user is from the FollowButton control.
userRouter.post('/linkuser', ensureAuthenticated, async (req, res) => {
  // Current user is...welll... the current user
  const currentUser = req.user;

  const responseSaveUserLinks = await setUserLink(
      currentUser.handle,
      req.body.handle,
  );

  if (responseSaveUserLinks.success) {
    res.status(200).send({
      success: true,
      currentUserId: responseSaveUserLinks.currentUserId,
      linkUserId: responseSaveUserLinks.linkUserId,
    });
    return;
  } else {
    res.status(400).send({
      success: false,
      message: responseSaveUserLinks.message,
    });
    return;
  }
});

/**
 *  Insert New Follow
 *
 *  Default route with only the linkUserHandle
 */
userRouter.get(
    '/user/createfollowlink/:linkuserhandle',
    checkApiKey,
    async (req, res) => {
      // api.glibr:5000/user/createfollowlink/glibrsmith?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca
      // Current user is...welll... the current user
      const currentUser = req.user;

      const responseSaveUserLinks = await setUserLink(
          currentUser.handle,
          req.params.handle,
      );

      if (responseSaveUserLinks.success) {
        res.status(200).send({
          success: true,
          currentUserId: responseSaveUserLinks.currentUserId,
          linkUserId: responseSaveUserLinks.linkUserId,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: responseSaveUserLinks.message,
        });
        return;
      }
    },
);

/**
 *  Insert New Follow
 *
 *  Overload  route with only  linkUserHandle and currentUser
 *  This is the overload to use in testing
 */
userRouter.get(
    '/user/createfollowlink/:currentuserhandle/:linkuserhandle',
    checkApiKey,
    async (req, res) => {
      // api.glibr:5000/user/createfollowlink/glibrsmith?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca
      // Get the user by handle

      const responseSaveUserLinks = await setUserLink(
          req.params.currentuserhandle,
          req.params.linkuserhandle,
      );

      if (responseSaveUserLinks.success) {
        res.status(200).send({
          success: true,
          currentUserId: responseSaveUserLinks.currentUserId,
          linkUserId: responseSaveUserLinks.linkUserId,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: responseSaveUserLinks.message,
        });
        return;
      }
    },
);

/** setUserLink
 * method to do the user link creation.
 *  @param {*} currentUserHandle
 * @param {*} linkUserHandle
 * @returns {resultObj}
 */
async function setUserLink(currentUserHandle, linkUserHandle) {
  // same handle - exit
  if (linkUserHandle === currentUserHandle) {
    return {
      success: false,
      message: GLOBALS.STRINGS.USERLINK_SAME_USER,
    };
  }

  // 1. get uesr ids
  let linkUserId = null;
  let linkUser = null;
  const responseLinkUser = await getUserByHandleVerbose(linkUserHandle);
  if (responseLinkUser.success) {
    linkUser = responseLinkUser.user;
    linkUserId = responseLinkUser.user.userId;
  } else {
    // no id - exit
    return {
      success: false,
      message: `${GLOBALS.STRINGS.UERLINK_COULDNT_RETRIEVE_LINKUSER} - ${linkUserHandle}`,
    };
  }

  let currentUserId = null;
  let currentUser = null;
  const responseCurrentUser = await getUserByHandleVerbose(currentUserHandle);
  if (responseCurrentUser.success) {
    currentUser = responseCurrentUser.user;
    currentUserId = responseCurrentUser.user.userId;
  } else {
    // no id - exit
    return {
      success: false,
      message: `$(GLOBALS.STRINGS.USERLINK_COULDNT_RETRIEVE_CURRENT_USER} - ${currentUserHandle}`,
    };
  }

  // 2. confirm that this pairing does not yet exist.
  const responseLinkExists = await checkForUserLinkExists(
      currentUserId,
      linkUserId,
  );
    // in this check we do NOT want to find a link - so if success, we exit
  if (responseLinkExists.success) {
    // The User Link already exists so we are not going to add it
    // Assumes all links in good state
    return {
      success: false,
      message: responseLinkExists.message,
    };
  }

  // 3. build new link
  const userFollowLink = {
    primeId: currentUserId,
    followId: linkUserId,
    createTimeStamp: new Date(),
  };

  // Increment count  for current user.
  currentUser.followCount++;
  // increment count for followers on linkuser
  linkUser.followerCount++;

  // 4. insert new unique document in collection
  const responseUserLink = await saveUserLink(userFollowLink);

  if (!responseUserLink.success) {
    // early failure we will exit
    return {
      success: false,
      message: `${GLOBALS.STRINGS.USERLINK_FAILED_TO_DO_SAVE} - follow`,
    };
  }

  // check for reverse link to set double or not
  const reverseLinkExists = await checkForUserLinkExists(
      linkUserId,
      currentUserId,
  );
    // and in this instance we WANT to find the link - so if success continue
  if (reverseLinkExists.success) {
    // we are doubled so we shall build the double.
    currentUser.doubleFollowCount++;
    linkUser.doubleFollowCount++;

    // 6.A primeUser double
    const userDoubleLink = {
      primeId: currentUserId,
      doubleId: linkUserId,
      createTimeStamp: new Date(),
    };
    // 6.B linkUser double
    const userDoubleLink2 = {
      primeId: linkUserId,
      doubleId: currentUserId,
      createTimeStamp: new Date(),
    };

    await saveUserLink(userDoubleLink);
    await saveUserLink(userDoubleLink2);
    // we'll accept failed double links  if it occurs and move on.
  }

  // save newly updated users to the database.
  const responseUpdateCurrentUser = await updateUser(
      currentUserId,
      currentUser,
  );
  if (!responseUpdateCurrentUser.success) {
    return {
      success: false,
      message: `${GLOBALS.STRINGS.USERLINK_FAILED_TO_UPDATE_USER} - current user - ${currentUserHandle}`,
    };
  }

  const responseUpdateLinkedUser = await updateUser(linkUserId, linkUser);
  if (!responseUpdateLinkedUser.success) {
    return {
      success: false,
      message: `${GLOBALS.STRINGS.USERLINK_FAILED_TO_UPDATE_USER} - link user - ${linkUserHandle}`,
    };
  }

  return {
    success: true,
    currentUserId: currentUserId,
    linkUserId: linkUserId,
  };
}

/**
 * updateUser
 * @param {*} thisUserId
 * @param {*} thisUser
 * @return {boolean}
 */
async function updateUser(thisUserId, thisUser) {
  try {
    await User.update(
        {
          _id: thisUserId,
        },
        thisUser,
    );
    return {
      success: true,
    };
  } catch (err) {
    logger.error(GLOBALS.STRINGS.USERLINK_FAILED_TO_UPDATE_USER);
    logger.error(err);
    return {
      success: false,
    };
  }
}


/**
 * Update User Status
 */
userRouter.put(
    '/user/:userhandle/:userstatus',
    ensureAuthenticated,
    async (req, res) => {
      try {
        const currentUser = await getUserByHandleCompact(req.params.userhandle);

        const userStatusList = [
          'active',
          'active-provisional',
          'payment-pending',
          'registration-one-basic',
          'registration-two-charity',
          'registration-three-payment',
          'registered-inactive',
          'registered-provisional-inactive',
          'registered-provisional-expired',
          'suspended-payment-failure',
          'suspended',
          'locked',
          'deactivated',
          'deleted',
        ];
        if (userStatusList.indexOf(req.params.userstatus) < 0) {
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.GROUPS_CHANGE_USERTYPE_FAIL,
          });
          return;
        }

        if (currentUser.success) {
          // check for valid update attempt
          if (currentUser.user.userStatusName === req.params.userstatus) {
            res.status(400).send({
              success: false,
              message: 'That user status is the same as before',
            });
            return;
          }

          const responseUpdateUserStatus = await updateUserStatus(currentUser.user.handle, req.params.userstatus);

          if (responseUpdateUserStatus.success) {
            res.status(200).send({
              success: true,
              message: `User status updated to:  ${req.params.userstatus}`,
            });
          } else {
            res.status(400).send({
              success: false,
              message: `Unable to update user status: ${req.params.userstatus}`,
            });
          }
        } else {
          res.status(400).send({
            success: false,
            message: `Unable to find a user: ${req.params.userHandle}`,
          });
        }
      } catch (err) {
        logger.error(` what ${GLOBALS.STRINGS.GROUPS_CHANGE_USERTYPE_FAIL}  - ${err}`);
        res.status(400).send({
          success: false,
          message: `Unable to update user status: ${req.params.userstatus}`,
        });
      }
    }
);

/**
 * updateUserStatus
 * @param {*} handle
 * @param {*} newUserStatus
 * @returns {boolean}
 */
async function updateUserStatus(handle, newUserStatus) {
  try {
    const response = await User.findOneAndUpdate(
        {
          handle: handle,
        },
        {
          $set: {
            userStatusName: newUserStatus,
          },
        },
    );

    if (response!==null) {
      return {
        success: true,
        userStatusName: newUserStatus,
        message: `Success - Updated user status: ${handle} - ${newUserStatus}`,
      };
    } else {
      return {
        success: false,
        userStatusName: newUserStatus,
        message: `unable to update user: ${handle} - ${newUserStatus}`,
      };
    };
  } catch (err) {
    return {
      success: false,
      message: `failed to update user status: ${handle} - ${newUserStatus} - ${err}`,
    };
  }
}

async function saveUserLink(uLinkObj) {
  const userLinkObj = new UserLinks({
    ...uLinkObj,
  });

  try {
    await userLinkObj.save(userLinkObj);

    return {
      success: true,
    };
  } catch (err) {
    logger.error(GLOBALS.STRINGS.USERLINK_SAVE_ERROR);
    logger.error(err);
    return {
      success: false,
      message: GLOBALS.STRINGS.USERLINK_SAVE_ERROR,
    };
  }
}

// test get method for check user link ids
userRouter.get(
    '/checkuseridlinked/:thisid/:thatid',
    checkApiKey,
    async (req, res) => {
      // Current user

      try {
        const resObj = await checkForUserLinkExists(
            req.params.thisid,
            req.params.thatid,
        );

        if (resObj.success) {
          res.status(200).send({
            success: true,
            message: 'user link found',
          });
          return;
        } else {
          res.status(400).send({
            success: false,
            message: 'no link',
          });
          return;
        }
      } catch (err) {}
      res.status(400).send({
        success: false,
        message: err,
      });
      return;
    },
);

userRouter.get(
    '/userlinks/checkhandleslinked/:thishandle/:thathandle',
    checkApiKey,
    async (req, res) => {
      const thisRes = await getUserIdByHandle(req.params.thishandle);
      const thatRes = await getUserIdByHandle(req.params.thathandle);

      let thisId = 0;
      let thatId = 0;

      if (thisRes.success) {
        thisId = thisRes.userId;
      }

      if (thatRes.success) {
        thatId = thatRes.userId;
      }

      if (thisId == 0 || thatId == 0) {
        res.status(400).send({
          success: false,
          message: 'no userid for one of the users',
        });
        return;
      }

      try {
        const resObj = await checkForUserLinkExists(thisId, thatId);

        if (resObj.success) {
          res.status(200).send({
            success: true,
            message: 'user link found',
          });
          return;
        } else {
          const resObj2 = await checkForUserLinkExists(thatId, thisId);

          if (resObj2.success) {
            res.status(200).send({
              success: true,
              message: 'user link found',
            });
            return;
          } else {
            res.status(400).send({
              success: false,
              message: 'no userlink found',
            });
            return;
          }
        }
      } catch (err) {}
      res.status(400).send({
        success: false,
        message: err,
      });
      return;
    },
);

async function checkForUserLinkExists(primeUserId, followUserId) {
  try {
    const responseUserLinks = await UserLinks.findOne({
      primeId: primeUserId,
      followId: followUserId,
    });

    if (responseUserLinks !== null) {
      return {
        success: true,
        message: `${GLOBALS.STRINGS.USERLINK_THAT_PAIR_ALREADY_EXISTS} - userlink check: ${responseUserLinks}`,
      };
    } else {
      return {
        success: false,
        message: 'no link found',
      };
    }
  } catch (err) {
    return {
      success: false,
      message: `${GLOBALS.STRINGS.USERLINK_THAT_PAIR_ALREADY_EXISTS} - err - ${err}`,
    };
  }
}

/**
  * UN Link users
    Undo all the link stuff - check for doubles etc.
  */
userRouter.post('/unlinkuser', ensureAuthenticated, async (req, res) => {
  // Current user
  const currentUserHandle = req.user.handle;

  const responseKillUserLinks = await killUserLinks(
      currentUserHandle,
      req.body.handle,
  );

  if (responseKillUserLinks.success) {
    res.status(200).send({
      success: true,
      currentUserId: responseKillUserLinks.currentUserId,
      linkUserId: responseKillUserLinks.linkUserId,
    });
    return;
  } else {
    res.status(400).send({
      success: false,
      message: responseKillUserLinks.message,
    });
    return;
  }
});

/**
 *  Delete Follow
 *  Default route with only the linkUserHandle
 * This is the route that will be coming from the unfollow link in the UI
 */
userRouter.get(
    '/user/deletefollowlink/:linkuserhandle',
    checkApiKey,
    async (req, res) => {
      // api.glibr:5000/user/deletefollowlink/glibrsmith?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca
      // Current user is...welll... the current user
      const currentUserHandle = req.user.handle;

      const responseKillUserLinks = await killUserLinks(
          currentUserHandle,
          req.params.linkuserhandle,
      );

      if (responseKillUserLinks.success) {
        res.status(200).send({
          success: true,
          currentUserId: responseKillUserLinks.currentUserId,
          linkUserId: responseKillUserLinks.linkUserId,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: responseKillUserLinks.message,
        });
        return;
      }
    },
);

/**
 *  Delete Follow
 *
 *  Overload  route with only  linkUserHandle and currentUser
 *  This is the overload to use in testing
 */
userRouter.get(
    '/user/deletefollowlink/:linkuserhandle/:currentuserhandle',
    checkApiKey,
    async (req, res) => {
      // api.glibr:5000/user/killUserLinks/glibrsmith/gonzo?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca
      // Get the user by handle
      const responseKillUserLinks = await killUserLinks(
          req.params.currentuserhandle,
          req.params.linkuserhandle,
      );

      if (responseKillUserLinks.success) {
        res.status(200).send({
          success: true,
          currentUserId: responseKillUserLinks.currentUserId,
          linkUserId: responseKillUserLinks.linkUserId,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          message: responseKillUserLinks.message,
        });
        return;
      }
    },
);

/** killUserLinks
 *  delete a set of userlinks - all pieces - follows, followers, doubles
 *  @param {*} currentUserHandle
 * @param {*} linkUserHandle
 * @returns {resultObj}
 */
async function killUserLinks(currentUserHandle, linkUserHandle) {
  // same handle - exit
  if (linkUserHandle === currentUserHandle) {
    return {
      success: false,
      message: GLOBALS.STRINGS.USERLINK_SAME_USER,
    };
  }

  // 1. get uesr ids
  // need the complete user object because we are updating said users.
  let linkUserId = null;
  let linkUser = null;
  const responseLinkUser = await getUserByHandleVerbose(linkUserHandle);
  if (responseLinkUser.success) {
    linkUser = responseLinkUser.user;
    linkUserId = responseLinkUser.user.userId;
  } else {
    // no id - exit
    return {
      success: false,
      message: `${GLOBALS.STRINGS.UERLINK_COULDNT_RETRIEVE_LINKUSER} - ${linkUserHandle}`,
    };
  }

  let currentUserId = null;
  let currentUser = null;
  const responseCurrentUser = await getUserByHandleVerbose(currentUserHandle);
  if (responseCurrentUser.success) {
    currentUser = responseCurrentUser.user;
    currentUserId = responseCurrentUser.user.userId;
  } else {
    // no id - exit
    return {
      success: false,
      message: `$(GLOBALS.STRINGS.USERLINK_COULDNT_RETRIEVE_CURRENT_USER} - ${currentUserHandle}`,
    };
  }

  // 2. confirm that this pairing does not yet exist.
  const responseLinkExists = await checkForUserLinkExists(
      currentUserId,
      linkUserId,
  );
    // in this check we WANT want to find a link - so we can subsequently delete it
    // no link found -- exit
  if (!responseLinkExists.success) {
    // The User Link does not exists so we are not going to try and delete the link
    return {
      success: false,
      message: responseLinkExists.message,
    };
  }

  // link exists continue

  // Decrement counts
  currentUser.followCount--;
  linkUser.followerCount--;

  // remove link
  const responseRemoveLink = await removePrimeLink(currentUserId, linkUserId);

  if (!responseRemoveLink.success) {
    return {
      success: false,
      message: `${GLOBALS.STRINGS.USERLINK_UNABLE_TO_DELETE_LINK} - follow`,
    };
  }

  // check for reverse link to deleete  double or not
  const reverseLinkExists = await checkForUserLinkExists(
      linkUserId,
      currentUserId,
  );
    // and in this instance we WANT to find the link - so if success continue
  if (reverseLinkExists.success) {
    // we are doubled so we shall delete  the double.
    currentUser.doubleFollowCount--;
    linkUser.doubleFollowCount--;

    await removeDoubleLink(currentUserId, linkUserId);
    await removeDoubleLink(linkUserId, currentUserId);
    // we'll accept failed double links  if it occurs and move on.
  }

  // save newly updated users to the database.
  const responseUpdateCurrentUser = await updateUser(
      currentUserId,
      currentUser,
  );
  if (!responseUpdateCurrentUser.success) {
    return {
      success: false,
      message: `${GLOBALS.STRINGS.USERLINK_FAILED_TO_UPDATE_USER} - current user - ${currentUserHandle}`,
    };
  }

  const responseUpdateLinkedUser = await updateUser(linkUserId, linkUser);
  if (!responseUpdateLinkedUser.success) {
    return {
      success: false,
      message: `${GLOBALS.STRINGS.USERLINK_FAILED_TO_UPDATE_USER} - link user - ${linkUserHandle}`,
    };
  }

  return {
    success: true,
    currentUserId: currentUser.id,
    linkUserId: linkUserId,
  };
}

/** removePrimeLink
 *
 * @param {*} primeLinkId
 * @param {*} followLinkId
 * @returns {*} success and removeResponse
 */
async function removePrimeLink(primeLinkId, followLinkId) {
  try {
    const responseRemoveLink = await UserLinks.findOneAndDelete({
      $and: [
        {
          primeId: primeLinkId,
        },
        {
          followId: followLinkId,
        },
      ],
    });
    if (responseRemoveLink !== null) {
      return {
        success: true,
        removeResponse: responseRemoveLink,
      };
    } else {
      return {
        success: false,
        message: GLOBALS.STRINGS.USERLINK_UNABLE_TO_DELETE_LINK,
      };
    }
  } catch (err) {
    logger.error(GLOBALS.STRINGS.USERLINK_UNABLE_TO_DELETE_LINK);
    logger.error(err);
    return {
      success: false,
      message: GLOBALS.STRINGS.USERLINK_UNABLE_TO_DELETE_LINK,
    };
  }
}

/** removeDoubleLink
 *
 * @param {*} primeLinkId
 * @param {*} doubleLinkId
 * @returns {*} success and removeResponse
 */
async function removeDoubleLink(primeLinkId, doubleLinkId) {
  try {
    const responseRemoveLink = await UserLinks.findOneAndDelete({
      $and: [
        {
          primeId: primeLinkId,
        },
        {
          doubleId: doubleLinkId,
        },
      ],
    });

    if (responseRemoveLink !== null) {
      return {
        success: true,
        removeResponse: responseRemoveLink,
      };
    } else {
      return {
        success: false,
        message: GLOBALS.STRINGS.USERLINK_UNABLE_TO_DELETE_LINK,
      };
    }
  } catch (err) {
    logger.error(GLOBALS.STRINGS.USERLINK_UNABLE_TO_DELETE_LINK);
    logger.error(err);
    return {
      success: false,
      message: GLOBALS.STRINGS.USERLINK_UNABLE_TO_DELETE_LINK,
    };
  }
}

/** removeprimelink
 * test method
 */
userRouter.get(
    '/user/removeprimelink/:handle/:linkhandle',
    checkApiKey,
    async (req, res) => {
      // api.glibr:5000/user/getuserfollows/gonzo?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca

      const responseGetPrimeId = await getUserIdByHandle(req.params.handle);
      let userId = null;
      if (responseGetPrimeId.success) {
        userId = responseGetPrimeId.userId;
      } else {
        res.status(400).send({
          success: false,
        });
        return;
      }

      const responseGetLinkId = await getUserIdByHandle(
          req.params.linkhandle,
      );
      let linkUserId = null;
      if (responseGetLinkId.success) {
        linkUserId = responseGetLinkId.userId;
      } else {
        res.status(400).send({
          success: false,
        });
        return;
      }

      const responseRemoveObj = await removePrimeLink(userId, linkUserId);

      if (responseRemoveObj.success) {
        res.status(200).send({
          success: true,
          removeResponse: responseRemoveObj.removeResponse,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
        });
        return;
      }
    },
);


userRouter.get(
    '/user/users/:userstatus/:pagenumber/:perpage',
    checkApiKey,
    async (req, res) => {
      perPage = parseInt(req.params.perpage) || 10;
      pageNumber = parseInt(req.params.pagenumber) || 1;

      console.log(req.params.userstatus);

      const response = await getUserList(req.params.userstatus, pageNumber, perPage);

      if (response.success) {
        res.status(200).send({
          success: true,
          response: response,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
        });
        return;
      }
    });


async function getUserList(userStatus, pageNumber, perPage) {
  try {
    const pagination = {
      limit: perPage,
      skip: perPage * (pageNumber - 1) || 0,
    };

    const userListQuery = {
      userStatusName: userStatus,
    };

    console.log('list it: ' + JSON.stringify(userListQuery) + ' ' + pagination.limit);

    const response = await User.find(userListQuery)
        .sort()
        .limit(pagination.limit)
        .skip(pagination.skip)
        .exec();

    console.log(JSON.stringify(response));


    if (response.length > 0) {
      return {
        success: true,
        message: 'we have users in our list',
        userlistLength: response.length || 0,
        userList: response,
      };
    } else {
      return {
        success: false,
        message: GLOBALS.STRINGS.GET_GLIBFEED_FAIL,
        glibCount: 0,
        glibFeed: null,
      };
    }
  } catch (err) {
    logger.error(err);
    return {
      success: false,
      userlink: 'error1',
    };
  }
}


/**
 *  Retrieve all follows by handle
 *  Get all links by currentUser by primeId field
 *    --> excluding doubles
 */
userRouter.get(
    '/user/getfollows/:handle/:followtype',
    checkApiKey,
    async (req, res) => {
      const responseGetId = await getUserIdByHandle(req.params.handle);

      let userId = null;
      if (responseGetId.success) {
        userId = responseGetId.userId;
      } else {
        res.status(400).send({
          success: false,
          userlink: null,
        });
        return;
      }

      let responseFollows = null;
      if (req.params.followtype === 'follows') {
        responseFollows = await getFollows(userId);
      } else if (req.params.followtype === 'followers') {
        responseFollows = await getFollowers(userId);
      } else if (req.params.followtype === 'doubled') {
        responseFollows = await getDoubles(userId);
      }

      if (responseFollows.success) {
        res.status(200).send({
          success: true,
          userlink: responseFollows.userlink,
        });
        return;
      } else {
        res.status(400).send({
          success: false,
          userlink: null,
        });
        return;
      }
    },
);

/** getFollows
 *
 * @param {*} userId
 * @returns {*} array of user ids
 */
async function getFollows(userId) {
  try {
    logger.info('get follows: incoming userId: ' + userId);

    const userFollows = await UserLinks.find({
      $and: [
        {
          primeId: userId,
        },
        {
          followId: {
            $ne: null,
          },
        },
      ],
    })
        .limit(25)
        .select({
          followId: 1,
        })
        .exec();

    const userIdMap = [];
    userFollows.forEach(function(user) {
      userIdMap.push(user.followId);
    });

    logger.info(
        'get follows: length of returning userids found: ' +
                userIdMap.length,
    );
    logger.info('map of ids:' + userIdMap);

    if (userIdMap.length > 0) {
      return {
        success: true,
        userlink: userIdMap,
      };
    } else {
      // still return true in this case
      return {
        success: true,
        userlink: 'empty',
      };
    }
  } catch (err) {
    logger.error(err);
    return {
      success: false,
      userlink: 'empty',
    };
  }
}

/** getFollowers
 *
 *  Retrieve all followers by handle
 *  Get all links by currentUser by primeHandle field
 */
userRouter.get('/user/getFollowers/:handle', checkApiKey, async (req, res) => {
  const responseGetId = await getUserIdByHandle(req.params.handle);

  let userId = null;
  if (responseGetId.success) {
    userId = responseGetId.userId;
  } else {
    res.status(400).send({
      success: false,
      userlink: null,
    });
    return;
  }

  const responseFollowers = await getFollowers(userId);

  if (responseFollowers.success) {
    res.status(200).send({
      success: true,
      userlink: responseFollowers.userlink,
    });
    return;
  } else {
    res.status(400).send({
      success: false,
      userlink: null,
    });
    return;
  }
});

/** getFollowers
 *
 * @param {*} userId
 * @returns {*} array of user ids
 */
async function getFollowers(userId) {
  try {
    const userFollowers = await UserLinks.find({
      followId: userId,
    })
        .limit(25)
        .select({
          primeId: 1,
        })
        .exec();

    const userIdMap = [];
    userFollowers.forEach(function(user) {
      userIdMap.push(user.primeId);
    });

    if (userIdMap.length > 0) {
      logger.info(
          'user Followers: ' + userId + ' - ' + userIdMap.toString(),
      );
      return {
        success: true,
        userlink: userIdMap,
      };
    } else {
      // still return true in this case
      return {
        success: true,
        userlink: 'empty',
      };
    }
  } catch (err) {
    logger.error(err);
    return {
      success: false,
      userlink: null,
    };
  }
}

/**
 *  Retrieve all doubles  by handle
 *  Get all links by currentUser by primeHandle field
 */
userRouter.get('/user/getdoubles/:handle', checkApiKey, async (req, res) => {
  // api.glibr:5000/user/getDoubles/gonzo?apikey=38761e76-4628-4ffc-b1f8-79bf47d4ceca
  const responseGetId = await getUserIdByHandle(req.params.handle);

  let userId = null;
  if (responseGetId.success) {
    userId = responseGetId.userId;
  } else {
    res.status(400).send({
      success: false,
      userlink: null,
    });
    return;
  }

  const responseDoubles = await getDoubles(userId);

  if (responseDoubles.success) {
    res.status(200).send({
      success: true,
      userlink: responseDoubles.userlink,
    });
    return;
  } else {
    res.status(400).send({
      success: false,
      userlink: 'error2',
    });
    return;
  }
});

/** getDoubles
 *
 * @param {*} userId
 * @returns {*} array of user ids
 */
async function getDoubles(userId) {
  try {
    const userDoubles = await UserLinks.find({
      doubleId: userId,
    })
        .limit(25)
        .select({
          primeId: 1,
        })
        .exec();

    const userIdMap = [];
    userDoubles.forEach(function(user) {
      userIdMap.push(user.primeId);
    });

    if (userIdMap.length > 0) {
      return {
        success: true,
        userlink: userIdMap,
      };
    } else {
      // still return true in this case
      return {
        success: true,
        userlink: 'empty',
      };
    }
  } catch (err) {
    logger.error(err);
    return {
      success: false,
      userlink: 'error1',
    };
  }
}

/** getGlibCountByHandle
 *
 * @param {*} handle
 * @returns {*} count of glibs
 *  */
async function getGlibCountByHandle(handle) {
  const userIdObj = await getUserIdByHandle(handle);

  if (userIdObj.success) {
    try {
      const gCount = await Glib.count({
        userId: userIdObj.userId,
      });
      return {
        success: true,
        count: gCount,
      };
    } catch (err) {
      logger.error(err);
      return {
        success: false,
        count: 0,
      };
    }
  } else {
    return {
      success: false,
      count: 0,
    };
  }
}

module.exports = userRouter;
