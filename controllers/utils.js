const express = require('express');
const utilsRouter = express.Router();
const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const logger = require('../common/logger');
const crypto = require('crypto');
const mime = require('mime');
const fs = require('fs');
const request = require('request-promise');

// glibr modules
const Helper = require('../common/helpers');
const GLOBALS = require('../common/globals.js');
const VAULT = require('../common/vault.js');
const UserModel = require('../models/user');

// https://stackoverflow.com/questions/40494050/uploading-image-to-amazon-s3-using-multer-s3-nodejs
// https://github.com/zishon89us/node-cheat/blob/master/aws/express_multer_s3/app.js
// https://www.npmjs.com/package/multer-s3
aws.config.update({
  secretAccessKey: GLOBALS.ENV_VARS.AWS_S3_SECRETACCESSKEY,
  accessKeyId: GLOBALS.ENV_VARS.AWS_S3_ACCESSKEYID,
  region: 'us-west-2',
});

const s3 = new aws.S3();

const glibrBucket = 'glibr-public/uploads';
const formDataKey = 'picToUpload';

// Check for a valid Api key - Remember to add this to the relevant (probably all) route methods.
function checkApiKey(req, res, next) {
  if (VAULT.API_KEYS.includes(req.query.apikey) || VAULT.API_KEYS.includes(req.body.apikey)) {
    next();
  } else {
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.INVALID_API_KEY,
    });
  }
}


// set image filter for MULTER image uploading
// https://stackoverflow.com/questions/47673533/how-to-catch-the-error-when-i-am-using-file-filter-in-multer
const imageFilter = function(req, file, cb) {
  // accept image only
  const ext = mime.getExtension(file.mimetype).toLowerCase();
  if (! GLOBALS.CONSTANTS.ACCEPTED_IMAGE_FILE_TYPES.includes(`.${ext}`)) {
    const err = new multer.MulterError(); // Error();
    err.code = 'filetype';
    err.message = GLOBALS.STRINGS.UNSUPPORTED_IMAGE_FILE_TYPE;
    return cb(err);
  }
  cb(null, true);
};

// set storage location and filename for MULTER image uploading.
const storage = multerS3({
  s3: s3,
  bucket: glibrBucket,
  acl: 'public-read',
  key: function(req, file, cb) {
    // Image filter above would have already run by this point.

    // Crypto.RandomBytes function will generate a random name.
    // More random than Math.Random as to avoid name collisions.
    const customFileName = crypto.randomBytes(18).toString('hex');
    // get file extension from original file name
    const fileExtension = mime.getExtension(file.mimetype); // path.extname(file.originalname).split('.')[1];
    const fullFileName = `${customFileName}.${fileExtension}`;
    req.fileName = fullFileName;
    cb(null, fullFileName);
  },
});

// Instantiate MULTER
const multerUploader = multer({
  storage: storage,
  fileFilter: imageFilter,
  limits: {
    fileSize: GLOBALS.CONSTANTS.IMAGE_UPLOAD_SIZE_LIMIT,
  },
}).single(formDataKey);


/* ---------------------------------------------------------------------
 * Upload  Picture *
 *----------------------------------------------------------------------*/
utilsRouter.post('/utils/imageupload', async (req, res) => {
  multerUploader(req, res, async (err) => {
    if (req.fileValidationError) {
      logger.error(`${GLOBALS.STRINGS.UNSUPPORTED_IMAGE_FILE_TYPE} -  ${err.message}`);
      res.status(400).send({
        success: false,
        message: 'Wrong file type',
      });
    } else if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
      logger.error(`${GLOBALS.STRINGS. FILE_UPLOAD_ERROR} - ${err.message}`);
      res.status(400).send({
        success: false,
        message: `${GLOBALS.STRINGS. FILE_UPLOAD_ERROR} - ${err.message}`,
      });
    } else if (err) {
      // An unknown error occurred when uploading.
      logger.error('in err: ' + err.message);
      res.status(400).send({
        success: false,
        message: `${GLOBALS.STRINGS. FILE_UPLOAD_ERROR} - ${err.message}`,
      });
    } else if (! req.file) {
      logger.error(GLOBALS.STRINGS.FILE_NOT_DEFINED);
      res.status(400).send({
        success: false,
      });
    } else {
      logger.info(GLOBALS.STRINGS.FILE_UPLOAD_SUCCESS);
      // return success and file name

      const imagePath = `${GLOBALS.CONSTANTS.IMAGE_UPLOAD_DESTINATION}${req.file.key}`;

      const responseCleanImage = await CheckForNSFWImage(imagePath);

      // reject naughty images.
      if (!responseCleanImage.success) {
        fs.unlink(imagePath, function(err) {
          if (err) {
            logger.error(`${GLOBALS.STRINGS.FILE_DELETE_FAILURE} - ${err}`);
          } else {
            // if no error, file has been deleted successfully
            logger.info(GLOBALS.STRINGS.FILE_DELETE_SUCCESS);
          }
          res.status(400).send({
            success: false,
            message: GLOBALS.STRINGS.FILE_REJECTED_NSFW,
            fileName: req.file.filename,
            originalName: req.file.originalname,
          });
        });
        return;
      }

      res.status(200).send({
        success: true,
        message: GLOBALS.STRINGS.FILE_UPLOAD_SUCCESS,
        fileName: req.file.key,
        originalName: req.file.originalname,
      });
    }
  });
});

async function CheckForNSFWImage(imagePath) {
  try {
    const picpurifyUrl = VAULT.PICPURIFY_URL;


    const data = {
      url_image: imagePath,
      // file_image: await fs.createReadStream(imagePath),
      API_KEY: VAULT.PICPURIFY_KEY,
      task: 'porn_detection',
    };

    const responseNSFWCheck = await request.post({url: picpurifyUrl, formData: data});

    responseJSON =JSON.parse(responseNSFWCheck);

    if (responseJSON.status === 'success') {
      if (responseJSON.final_decision === 'OK') {
        return {
          success: true,
          message: responseJSON.final_decision,
        };
      } else if (responseJSON.final_decision === 'KO') {
        return {
          success: false, // only fail to upload return, otherwise it should not block operation of the site.
          message: responseJSON.final_decision,
        };
      }
    } else if (jsonBody.success === 'failure') {
      logger.error(`1. ${GLOBALS.STRINGS.FAILED_NSFW_CHECK} - ${responseJSON.error.errorCode} - ${responseJSON.erorr.errorMsg}`);
      return {
        success: true,
        message: (`2. ${GLOBALS.STRINGS.FAILED_NSFW_CHECK} -${responseJSON.error.errorCode} - ${responseJSON.erorr.errorMsg}`),
      };
    }
  } catch (error) {
    logger.error(`3. ${GLOBALS.STRINGS.FAILED_NSFW_CHECK} - ${error}`);
    return {
      success: true,
      message: (`4. ${GLOBALS.STRINGS.FAILED_NSFW_CHECK} - ${error}`),
    };
  }
}

// Serve image files in the /controllers/uploads directory
utilsRouter.get('/public/uploads/:filename', function(req, res, next) {
  const params = {Bucket: glibrBucket, Key: req.params.filename};

  try {
    s3.getObject(params, function(err, data) {
      if (err) {
        return next();
      } else {
        // Convert file to base65 image
        const img = new Buffer(data.Body, 'base64');
        res.contentType(data.ContentType);
        res.status(200).send(img);
      }
    });
  } catch (err) {
    // very noisy in the console.
    logger.error(`${GLOBALS.STRINGS.FILE_DOES_NOT_EXIST} - ${err}`);

    res.status(400).send({
      success: false,
      message: `${GLOBALS.STRINGS.FILE_DOES_NOT_EXIST} - ${err}`,
    });
  }
});


utilsRouter.get('/public/uploads/uri/:filename', function(req, res, next) {
  const params = {Bucket: glibrBucket, Key: req.params.filename};

  try {
    s3.getObject(params, function(err, data) {
      s3.getSignedUrl('getObject', params, function(err, url) {
        if (err) {
          logger.error(`${GLOBALS.STRINGS.FILE_DOES_NOT_EXIST} - ${err}`);
        }
        res.send(url);
      });
    });
  } catch (err) {
    // very noisy in the console.
    logger.error(`${GLOBALS.STRINGS.FILE_DOES_NOT_EXIST} - ${err}`);

    res.status(400).send({
      success: false,
      message: `${GLOBALS.STRINGS.FILE_DOES_NOT_EXIST} - ${err}`,
    });
  }
});


// /**
//  * savefilenametodb
//  *
//  *  take the filename and save it to the db.
//  */
utilsRouter.post('/utils/savefilenametodb', async (req, res) => {
  try {
    let query = null;
    let upsertStatement = null;

    if (req.body.collectionType === 'profile') {
      query = {
        handle: req.body.userHandle,
      };
      upsertStatement = {
        profilePicture: req.body.fileName,
      };
    }

    const options = {
      new: true,
      useFindAndModify: false,
    };

    UserModel.findOneAndUpdate(query, upsertStatement, options, function(err, user) {
      if (err) {
        logger.error(GLOBALS.STRINGS.PROFILE_FAILED_TO_UPLOAD_PICTURE + ' - ' + err);
        res.status(400).send({
          success: false,
          message: GLOBALS.STRINGS.PROFILE_FAILED_TO_UPLOAD_PICTURE,
        });
        return;
      }

      logger.info('yup, it thinks it worked: ' + req.body.fileName);

      res.status(200).send({
        success: true,
        message: GLOBALS.STRINGS.PROFILE_UPDATE_SUCCESS,
      });
      return;
    });
  } catch (err) {
    logger.error(`${GLOBALS.STRINGS.FAILED_WRITING_FILENAME_TO_DB} - ${err}`);
    return;
  }
});


utilsRouter.get('/utils/checkforbadwords/:stringtocheck', checkApiKey, async (req, res) => {
  try {
    logger.info('in clean string check');
    const cleanedString = Helper.cleanText(req.params.stringtocheck);

    let foundBadText = false;
    logger.info('cleaned string: ' + cleanedString);

    if (req.params.stringtocheck !== cleanedString) {
      foundBadText = true;
    }

    res.status(200).send({
      success: true,
      cleanedString: cleanedString,
      foundBadText: foundBadText, // <--bit to check
      message: 'all good',
    });

    return;
  } catch (err) {
    logger.error('bad word check error: ' + err);
    res.status(400).send({
      success: false,
      message: GLOBALS.STRINGS.TEXT_CLEAN_FAILURE,

    });
  }
});

// API test method
utilsRouter.get('/timestamplist/:numberoftimestamps', function(req, res) {
  const numberOfTimeStamps = req.params.numberoftimestamps;
  let listOfTimeStamps = '';

  for (let i = 0; i < numberOfTimeStamps; i++) {
    // add comma at the end as needed
    if (i > 0) listOfTimeStamps += ',';
    // append the raw dateTimeStamp
    listOfTimeStamps += new Date().getTime();
    // pause a second
    sleep(999);
  }

  res.status(200).send({
    success: true,
    message: listOfTimeStamps,
  });
});

/**
 * @param {number} miliseconds
 * @returns {object}
 */
function sleep(miliseconds) {
  const currentTime = new Date().getTime();
  while (currentTime + miliseconds >= new Date().getTime()) {}
}


module.exports = utilsRouter;
