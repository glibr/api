const express = require('express');
const helmet = require('helmet');
const session = require('express-session');
const passport = require('passport');
const setUpPassport = require('./setuppassport');
const mongoose = require('mongoose');
const MongoDBStore = require('connect-mongodb-session')(session);
const bodyParser = require('body-parser');
const logger = require('./common/logger');
const assert = require('assert');

// glibr libraries
const GLOBALS = require('./common/globals.js');
const VAULT = require('./common/vault.js');

// Controllers references so they are detected by webpack.
// TODO: Come up with better way.  Now we have to update routes.json and below.
/* eslint-disable no-unused-vars */
const glibRouter = require('./controllers/glibs');
const charityRouter = require('./controllers/charities');
const userRouter = require('./controllers/user');
const paymentRouter = require('./controllers/payment');
const testRouter = require('./controllers/test');
const groupRouter = require('./controllers/groups');
const utilsRouter = require('./controllers/utils');
const searchRouter = require('./controllers/search');
/* eslint-disable no-unused-vars */


// >>  CREATE THE APP <<
const app = express();
// >>  CREATE THE APP <<

app.use(function(req, res, next) {
  // res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Origin', GLOBALS.ENV_VARS.SITE_BASE_URL);
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  // res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers,Origin,Accept, X-Requested-With,Content-Type, Access-Control-Request-Method,Access-Control-Request-Headers');
  res.setHeader('Cache-Control', 'no-cache');
  next();
});

app.use(helmet());

// Set up mongo session backing store
const glibrMongoSessionStore = new MongoDBStore({
  uri: GLOBALS.ENV_VARS.MONGO_CONN_STRING,
  collection: 'glibrsessions',

});

glibrMongoSessionStore.on('error', function(error) {
  assert.ifError(error);
  assert.ok(false);
});

mongoose.connect(GLOBALS.ENV_VARS.MONGO_CONN_STRING, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

mongoose.connect;
mongoose.Promise = global.Promise;

setUpPassport();

// Make sure our responses set readable json output
app.set('json spaces', 5);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false,
}));


// Set up express sessions with mongodb session backing store
app.use(require('express-session')({
  secret: VAULT.EXPRESS_SESSION_SECRET,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 7, // 1 week
  },
  store: glibrMongoSessionStore,
  resave: false,
  saveUninitialized: false,
}));

logger.info(`environment: :${process.env.NODE_ENV}:  :`);
logger.info(`allow origin url: ${GLOBALS.ENV_VARS.SITE_BASE_URL}`);

app.use(passport.initialize());
app.use(passport.session());


app.get('/', function(req, res) {
  logger.info('Hello ' + JSON.stringify(req.session));
});

// Setup all the routes
app.use(glibRouter);
app.use(charityRouter);
app.use(userRouter);
app.use(paymentRouter);
app.use(testRouter);
app.use(groupRouter);
app.use(utilsRouter);
app.use(searchRouter);


app.listen(5000, function() {
  logger.info('Server started on port 5000');
});
